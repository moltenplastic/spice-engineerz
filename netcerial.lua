local nc = {}
local bit = bit and bit or require("bit")
local START_TABLE = "\1"
local END_TABLE = "\0"
local STRING = "\2"
local NUMBER = "\3"
local INTEGER = "\4"
local TRUE = "\5"
local FALSE = "\6"
local MAXSTRINGSIZE = 1024 --so we cannot be crashed wit faking bullshit strings

local function grab_byte(v)
	return math.floor(v / 256), string.char(math.floor(v) % 256)
end
local function readDouble(x)
	--x = string.reverse(x)
	local sign = 1
	local mantissa = string.byte(x, 7) % 16
	for i = 6, 1, -1 do mantissa = mantissa * 256 + string.byte(x, i) end
	if string.byte(x, 8) > 127 then sign = -1 end
	local exponent = (string.byte(x, 8) % 128) * 16 +math.floor(string.byte(x, 7) / 16)
	if exponent == 0 then return 0 end
	mantissa = (math.ldexp(mantissa, -52) + 1) * sign
	return math.ldexp(mantissa, exponent - 1023)
end
local function writeDouble(x)
	local sign = 0
	if x < 0 then sign = 1; x = -x end
	local mantissa, exponent = math.frexp(x)
	if x == 0 then -- zero
		mantissa, exponent = 0, 0
	else
		mantissa = (mantissa * 2 - 1) * math.ldexp(0.5, 53)
		exponent = exponent + 1022
	end
	local v, byte = "" -- convert to bytes
	x = mantissa
	for i = 1,6 do
		x, byte = grab_byte(x); v = v..byte -- 47:0
	end
	x, byte = grab_byte(exponent * 16 + x); v = v..byte -- 55:48
	x, byte = grab_byte(sign * 128 + x); v = v..byte -- 63:56
	return v
end
local function readInt(v)
	return bit.lshift(v:byte(1),24)+bit.lshift(v:byte(2),16)+bit.lshift(v:byte(3),8)+v:byte(4)
end
local function writeInt(n)
	return string.char(bit.rshift(n,24),bit.band(bit.rshift(n,16),0xFF),bit.band(bit.rshift(n,8),0xFF),bit.band(n,0xFF))
end
local function readShort(v)
	return bit.lshift(v:byte(1),8)+v:byte(2)
end
local function writeShort(n)
	return string.char(bit.band(bit.rshift(n,8),0xFF),bit.band(n,0xFF))
end
function nc.s(v)
	local t = type(v)
	--print(t)
	if t == "table" then
		local s = START_TABLE
		for i, v in pairs(v) do
			s = s..nc.serialize(i)
			s = s..nc.serialize(v)
		end
		return s..END_TABLE
	elseif t == "string" then
		local len = #v
		return STRING..writeShort(len)..v
	elseif t == "number" then
		local d, f = math.modf(v)
		if f ~= 0 then
			return NUMBER..writeDouble(v)
		else
			return INTEGER..writeInt(v)
		end
	elseif t == "boolean" then
		return (v and TRUE or FALSE)
	end
	error("Could not convert "..t,2)
end
nc.serialize = nc.s
function nc.us(v)
	--print(#v)
	local t = v:sub(1,1)
	if t == START_TABLE then
		--print("table")
		local ni = 2
		local tab = {}
		while v:sub(ni,ni) ~= END_TABLE do
			local ti,n = nc.unserialize(v:sub(ni))
			ni = ni+n
			local tv,n = nc.unserialize(v:sub(ni))
			ni = ni+n
			tab[ti] = tv
		end
		return tab,ni
	elseif t == STRING then
		--print("string")
		local len = readShort(v:sub(2,3))
		if len < 0 or len > MAXSTRINGSIZE then print("Invalid string given") return "", 5 end
		return v:sub(4,len+3),len+3
	elseif t == NUMBER then
		--print("number")
		return readDouble(v:sub(2,9)),9
	elseif t == INTEGER then
		--print("integer")
		return readInt(v:sub(2,5)),5
	elseif t == TRUE then
		--print("boolean")
		return true, 1
	elseif t == FALSE then
		return false, 1
	end
	error("Could not reconvert "..v:byte(1),2)
end
nc.unserialize = nc.us

return nc
