assert(xpcall(function(...)
DEBUGGING = true

class = require "class"
resource = require "resource"
nc = require "netcerial"
network = require "network"
enet = require "enet"

CLIENT = false
SERVER = true

local arguments = {...}

local argparse = require "argparse"
local parser = argparse()
	:description
[[Server for Spice Engineerz (NNF)

Requirements:

	LuaSocket
	ENET
	Headless Love2D]]
parser:flag "-b" "--broadcast"
	:description "Broadcast the game on the local network"
parser:flag "--inside-thread"
	:description "This should only be passed when the server is running inside a Love2D thread."
parser:option "-p" "--port"
	:description "Select the port to run the game on"
	:default "0"
parser:option "-r" "--rules"
	:description "Set a game rule."
	:count "*"
parser:option "-s" "--sps"
	:description "Sets the network syncs per second"
	:default "15"

arguments = parser:parse(arguments)

arguments.sps = 1/tonumber(arguments.sps)

local address = "*"

if arguments.inside_thread then
	local eventchannel = love.thread.getChannel "server_events"
	local logchannel = love.thread.getChannel "server_log"
	local quitchannel = love.thread.getChannel "server_quit"
	local backdoor = love.thread.getChannel "server_backdoor"
	local pause = love.thread.getChannel "server_pause"
	
	function log(...)
		local t = {...}
		for i=1, #t do t[i] = tostring(t[i]) end
		logchannel:push(t)
	end
	
	function postEvent(event)
		eventchannel:push(event)
	end
	
	function canQuit()
		return not not quitchannel:peek()
	end
	
	function executeBackDoor()
		local bd = backdoor:peek()
		
		if bd then
			backdoor:pop()
			local f, e = loadstring(bd.src, "=server_back_door")
			
			if not f then
				postEvent({type = "callback", id = bd.id, f, e})
			else
				local ret = {pcall(f, unpack(bd))}
				ret.type = "callback"
				ret.id = bd.id
				postEvent(ret)
			end
		end
		
		if pause:pop() then
			pause:demand() --halt thread until unpause
		end
	end
	
	love.filesystem = require "love.filesystem"
	love.timer = require "love.timer"
	love.physics = require "love.physics"
	love.math = require "love.math"
	love.string = require "love.string"
	require "nersis"
	require "nersis.lvx"
	
	address = "localhost"
else
	function log(location, ...)
		print("["..location.."]", ...)
	end
	
	function postEvent(event)
		
	end
	
	function canQuit()
		love.event.pump()
		for e in love.event.poll() do
			if e == "quit" then
				return true
			end
		end
		return false
	end
	
	function executeBackDoor()
	
	end
	
	resource.init()
end

uuid = require "uuid"

host = enet.host_create(address..":"..arguments.port,64,2)
context = class.import "common.ServerContext":new(host)
--context:load(love.filesystem.read("testsave.sesf"))
context:load("testsave")

log("INIT", "Server starting")
if love then
	log("INIT", "Running under Love2D")
else
	log("INIT", "Not running under Love2D! This may not work!")
end

log("INIT", "Server running at "..host:get_socket_address())
postEvent({type="address", address=host:get_socket_address()})

local broadcastThread

if arguments.broadcast then
	log("INIT", "Broadcasting server presence")
	broadcastThread = class.import "thread.LocalBroadcast":new()
	broadcastThread:startBroadcasting(host:get_socket_address():match(":(.+)$"))
end

peers = {}
players = {}

local function processEvent(event)
	if event.type == "connect" then
		log("NET", "Client "..tostring(event.peer).." connected!")
		
		peers[event.peer] = {
			state = network.PEERSTATE.LOGIN,
		}
		
	elseif event.type == "disconnect" then
		local peerdata = peers[event.peer]
		if peerdata.state == network.PEERSTATE.PLAY then
			log("NET/PLAY", "Player "..peerdata.player.username.." disconnected without quit packet!")
			processEvent({type="receive",data=string.char(network.SERVERBOUND.QUIT).."\1\0",peer=event.peer}) --Craft QUIT packet
		end
		peers[event.peer] = nil
	elseif event.type == "receive" then
		local peerdata = peers[event.peer]
		local buffer = love.string.newReadBuffer(event.data)
		local packetType = buffer:readByte()
		
		if peerdata.state == network.PEERSTATE.LOGIN then
			if packetType == network.SERVERBOUND.SERVER_INFO then
				--Ok. We use NetCerial for server information sending.
				--However, NetCerial is feature frozen.
				--After the game is out, do not fuck around with netcerial.
				local info = {
					version = { --if major or minor is different, the client should jump ship.
						major = 0,
						minor = 0,
						build = 0
					},
					players = #players,
					name = "SE_TEST_SERVER",
					description = "Hey. This is a test server."
				}
				event.peer:send(string.char(network.CLIENTBOUND.SERVER_INFO)..network.constantSerialize(info))
			elseif packetType == network.SERVERBOUND.LOGIN then
				local username = buffer:readString()
				
				if type(username) ~= "string" then
					log("NET/LOGIN", tostring(event.peer).." gave no user name!")
					event.peer:send(string.char(network.CLIENTBOUND.ERROR)..string.char(network.CLIENTBOUND_ERROR.BAD_LOGIN))
					return
				end
				
				if #username > 25 then
					log("NET/LOGIN", tostring(event.peer).." gave name longer than 25 characters!")
					event.peer:send(string.char(network.CLIENTBOUND.ERROR)..string.char(network.CLIENTBOUND_ERROR.NAME_TOO_LONG))
					return
				end
				
				if players[username] then
					log("NET/LOGIN", tostring(event.peer).." gave name that is already in use!")
					event.peer:send(string.char(network.CLIENTBOUND.ERROR)..string.char(network.CLIENTBOUND_ERROR.NAME_IN_USE))
					return
				end
				
				event.peer:send(string.char(network.CLIENTBOUND.LOGGEDIN))
				
				log("NET/LOGIN", "Successful login by "..tostring(event.peer).." ("..username..")")
				peerdata.username = username
				
				local player = {
					peer = event.peer,
					peerdata = peerdata,
					username = username,
					inventory = {},
				}
				
				peerdata.player = player
				
				player.entity = class.import "common.entity.Player":new(context, username, player)
				context:spawn(player.entity)
				
				players[username] = player
				
				peerdata.state = network.PEERSTATE.PLAY
			end
		elseif peerdata.state == network.PEERSTATE.PLAY then
			local player = peerdata.player
			
			if packetType == network.SERVERBOUND.ENTITY then
				--find entity with ID--
				local id = buffer:readString()
				local ent = context:findById(id)
			
				if ent then
					ent:processPacket(player, buffer)
				end
			elseif packetType == network.SERVERBOUND.QUIT then
				log("NET/LOGIN", player.username.." quit")
				context:despawn(player.entity)
				players[player.username] = nil
				peerdata.player = nil
				peerdata.state = network.PEERSTATE.LOGIN
				event.peer:disconnect()
			elseif DEBUGGING then
				if packetType == network.SERVERBOUND.DEBUG_SPAWN_ENTITY then
					local classname, x, y, angle = buffer:readString(), buffer:readDouble(), buffer:readDouble(), buffer:readDouble()
					local s, cl = pcall(class.import, classname)
			
					if s then
						local o = cl:new(context)
				
						context:spawn(o)
				
						o.body:setX(x)
						o.body:setY(y)
						o.body:setAngle(angle)
					end
				end
			end
		end
	end
end

local sync = 0

local gettime = require "socket".gettime

while true do
	if canQuit() then break end
	
	love.timer.sleep(1/60)
	
	local a = love.timer.getTime()
	
	executeBackDoor()
	context:update(1/60)
	
	while true do
		local event = host:service(0)
		if not event then break end
		processEvent(event)
	end
	
	local b = love.timer.getTime()-a
	
	sync = sync+(1/60)+b
	
	if sync > arguments.sps then
		sync = sync-arguments.sps
		--do physics sync--
		local buffer = love.string.newWriteBuffer()
		buffer:writeByte(network.CLIENTBOUND.SYNC)
		buffer:writeDouble(gettime())
		buffer:writeInt(#context.entities+#context.removed)
		local syncpkt = {}
		for i, v in pairs(context.entities) do
			buffer:writeString(v.id)
			buffer:writeByte(0)
			buffer:writeString(v.classname)
			v:createSyncPacket(buffer)
		end
		for i, v in pairs(context.removed) do
			buffer:writeString(v.id)
			buffer:writeByte(1)
		end
		context.removed = {}
		context:broadcast(buffer:getString(),1,"unreliable")
		host:flush()
	end
end

for i=1, host:peer_count() do
	host:get_peer(i):disconnect_now(network.DISCON_REASON.SERVER_EXIT)
end
end,debug.traceback,...))
