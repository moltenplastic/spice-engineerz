local function autoMutable(t)
	local nt = setmetatable({},{__index=function(_,i)
		if t[i] and type(t[i][1]) == "number" then
			--copy--
			return {t[i][1],t[i][2],t[i][3],t[i][4]}
		else
			return t[i]
		end
	end})
	return nt
end

Colors = autoMutable {
	text = autoMutable {
		default = {255,255,255},
		light = {0,0,0},
	},
	primary = {0x3f, 0x51, 0xb5},
	primaryLight = {0x79, 0x86, 0xcb},
	primaryLightest = {0x1a, 0x23, 0x7e},
	background = {0x21, 0x21, 0x21},
	backgroundLight = {0xfa,0xfa,0xfa},
	accent = {0xf5, 0x00, 0x57},
}
