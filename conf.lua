function love.conf(t)
	t.modules.version = "0.10.0"
	
	local server = arg[2] == "--server"
	t.modules.keyboard = not server
	t.modules.graphics = not server
	t.modules.mouse = not server
	t.modules.window = not server
	t.modules.joystick = not server
	t.modules.audio = not server
	t.modules.sound = not server
	
	t.window.vsync = true
end
