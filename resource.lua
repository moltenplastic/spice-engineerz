--Resources.
--Resources are much fun. Like classes, we have a list of directories that are base resource directories.
--However, those directories can also have prefixes (Example: Mods)
--The global directory for resources (game resources) has the prefix "global"
--global:fonts/ComicSansMS.ttf for example
--Mods will be <cleansed_mod_name>:<whatever>

--Resource Packs:
--Resource packs can change a lot about the game. For instance, Resource packs can modify the UI layout.
--Resource packs are loaded over the defaults, and they have their own global list.
--Resource requests can request to not grab from a resource pack using resource.get(name, true)
--Resource pack paths are prepended to final resource paths

local resource = {}

function resource.init()
	resource.thread = love.thread.newThread("threads/resource_loader.lua")
	resource.thread:start()
end

--resource.preload("resource.domain:path/to/file", overrideResourcePack)
--resource.get("resource.domain:path/to/file", overrideResourcePack)
--resource.getAsync("resource.domain:path/to/file", callback, overrideResourcePack)
--resource.unload("resource.domain:path/to/file")
--resource.addType("pattern_one;pattern_two;pattern_three", "print('loader code')")

--Resource Domain Management--

local resourceDomains = {}
resourceDomains.global = "resources"

function resource.addDomain(name, path)
	resourceDomains[name] = path
end

function resource.removeDomain(name)
	resourceDomains[name] = nil
end

--Resource Pack Management--

local resourcePacks = {}

function resource.addPack(path)
	resourcePacks[#resourcePacks+1] = path
end

function resource.removePack(path)
	for i=1, #resourcePacks do
		if resourcePacks[i] == path then
			table.remove(resourcePacks, i)
			return
		end
	end
end

function resource.find(path)
	local domain, rpath = path:match("^(.+):(.+)$")
	
	if not domain or not rpath then
		domain = "global"
		rpath = path
		print("Domain not specified! This is potentially unsupported behavior! Defaulting to global domain.")
		print(debug.traceback())
	end
	
	local domainPath = assert(resourceDomains[domain], "Domain "..domain.." not found")
	
	local finalPath = domainPath.."/"..rpath
	
	for i=1, #resourcePacks do
		local npath = resourcePacks[i].."/"..finalPath
		if love.filesystem.isFile(npath) then
			return npath
		end
	end
	
	if not love.filesystem.isFile(finalPath) then
		error("Path "..rpath.." in domain "..domain.." not found!")
	end
	
	return finalPath
end

--Resource decoding and loading--

local IMAGE = {
	constructor = "return love.image.newImageData",
	post = function(_, id) return love.graphics.newImage(id) end
}

local SOURCE = {
	constructor = "return love.audio.newSource",
}

local DEFAULT = {
	constructor = "return love.filesystem.read",
}

local MODEL = {
	constructor = "return love.filesystem.read",
	post = function(path, f)
		local dir = resource.find(path):match("^(.+)/.-$").."/"
		local o = obj.loadOBJ(f, dir)
		local meshes = {}
		obj.toMesh(o, meshes)
		return meshes
	end
}

local MDF = {
	constructor = "return love.filesystem.read",
	post = function(path, f)
		return mdf.parse(f)
	end
}

local decoders = {
	["%.png$"] = IMAGE,
	["%.ogg$"] = SOURCE,
	["%.bin$"] = DEFAULT,
	["%.obj$"] = MODEL,
	["%.mdf$"] = MDF,
}

local cache = {}
local waiting = {}
local callbacks = {}

local requestChannel = love.thread.getChannel("resource_request_channel")
local responseChannel = love.thread.newChannel()

function resource.get(path, ...)
	if cache[path] then return cache[path] end
	
	local dir = resource.find(path)
	
	local decoder, decoderId
	for i, v in pairs(decoders) do
		if dir:find(i) then
			decoder = v
			decoderId = i
			break
		end
	end
	
	if not decoder then 
		decoder = DEFAULT
		decoderId = "%.bin$"
	end
	
	if not waiting[path] then
		waiting[#waiting+1] = path
		waiting[path] = true
		requestChannel:push({path, dir, decoder.constructor, decoderId, responseChannel, ...})
	end
	
	local a = love.timer.getTime()
	while waiting[path] do
		resource.update(true)
	end
	
	local ms = (love.timer.getTime()-a)*1000
	
	if ms > 1 then
		log("RES", "Unblocked load took "..ms.."ms!")
	end
	
	return cache[path]
end

function resource.getAsync(path, callback, ...)
	if cache[path] then callback(cache[path], path) return end
	
	local dir = resource.find(path)
	
	local decoder, decoderId
	for i, v in pairs(decoders) do
		if dir:find(i) then
			decoder = v
			decoderId = i
			break
		end
	end
	
	if not decoder then 
		decoder = DEFAULT
		decoderId = "%.bin$"
	end
	
	if not waiting[path] then
		waiting[#waiting+1] = path
		waiting[path] = true
		
		callbacks[path] = {callback}
		requestChannel:push({path, dir, decoder.constructor, decoderId, responseChannel, ...})
	else
		callbacks[path][#callbacks[path]+1] = callback
	end
end

function resource.preload(path, ...) resource.getAsync(path, function() end, ...) end

function resource.unload(path)
	if waiting[path] then
		if callbacks[path] then
			callbacks[path][#callbacks[path]+1] = function()
				cache[path] = nil
			end
		else
			callbacks[path] = {function()
				cache[path] = nil
			end}
		end
	else
		cache[path] = nil
	end
end

function resource.update(block)
	if resource.thread and not resource.thread:isRunning() then
		error(resource.thread:getError() or "Resource thread stopped")
	end

	local response
	
	if block then
		response = responseChannel:demand()
	else
		response = responseChannel:pop()
	end
	
	if not response then return false end
	
	if not waiting[response[1]] then
		--we don't own this, push it back in the responseChannel and leave
		responseChannel:push(response)
		return false
	end
	
	local path, decoderId = table.remove(response, 1), table.remove(response, 1)
	waiting[path] = nil
	for i=1, #waiting do
		if waiting[i] == path then table.remove(waiting, i) break end
	end
	
	local decoder = decoders[decoderId]
	if decoder then
		--if the decoder still isFile at this point--
		if decoder.post then
			cache[path] = decoder.post(path, unpack(response))
		else
			cache[path] = response[1]
		end
		
		if callbacks[path] then
			local cbs = callbacks[path]
			callbacks[path] = nil
			for i=1, #cbs do
				cbs[i](cache[path], path)
			end
		end
		
		log("RES", path.." loaded")
	end
	
	return true
end

resource.waiting = waiting
resource.cache = cache

return resource
