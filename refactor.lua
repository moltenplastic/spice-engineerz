#!/usr/bin/lua

--Refactor.lua: Class refactoring (Moving and Renaming)

local insult = {"Smartass", "Dipshit", "Dumbass", "Motherfucker", "Bitch"}

local lfs = require "lfs"

local args = {...}

local command = table.remove(args,1)

local function formatFile(file)
	file = file:gsub("^%./","")
	
	if file:sub(1,6) == "class/" then
		return file:sub(7,-5):gsub("/",".")
	end
	
	return file
end

local function findUsagesOfPackageIn(package, dir, ret)
	dir = dir or "."
	ret = ret or {}
	for file in lfs.dir(dir) do
		if file:sub(1,1) ~= "." then
			file = dir.."/"..file
			if lfs.attributes(file).mode == "directory" then
				findUsagesOfPackageIn(package, file, ret)
			elseif file:find("%.lua") then
				local f = io.open(file,"r")
				local count = 1
				for line in f:lines() do
					local s, e = line:find('"'..package..'.',1,true)
					
					if not s then
						s, e = line:find("'"..package..'.',1,true)
					end
					
					if s then
						ret[#ret+1] = {file, count, s, line:match("(.-)\"", e)}
						break
					end
					count = count+1
				end
				f:close()
			end
		end
	end
	return ret
end

local function findUsagesOfClassIn(class, dir, ret)
	dir = dir or "."
	ret = ret or {}
	for file in lfs.dir(dir) do
		if file:sub(1,1) ~= "." then
			file = dir.."/"..file
			if lfs.attributes(file).mode == "directory" then
				findUsagesOfClassIn(class, file, ret)
			elseif file:find("%.lua") then
				local f = io.open(file,"r")
				local count = 1
				for line in f:lines() do
					local s, e = line:find('"'..class..'"',1,true)
					
					if not s then
						s, e = line:find("'"..class.."'",1,true)
					end
					
					if s then
						ret[#ret+1] = {file, count, s}
						break
					end
					count = count+1
				end
				f:close()
			end
		end
	end
	return ret
end

if command == "move" then
	local what = table.remove(args, 1)
	if what == "class" then
		if not (args[1] and args[2]) then
			print("Usage: refactor move class [source.Class] [destination.Class]")
			return
		end
		
		local class = args[1]
		local dest = args[2]
		
		local usages = findUsagesOfClassIn(class)
		
		print((#usages).." usages found")
		for i, v in ipairs(usages) do
			print("In "..formatFile(v[1]).." on line "..v[2].." column "..v[3])
		end
		
		print()
		local answer
		while not answer do
			print("Do you want to refactor? [Y/N]")
			answer = io.read("*l"):gsub(1,1):lower()
			
			if answer ~= "y" and answer ~= "n" then
				answer = nil
				io.write(insult[math.random(1,#insult)]..", ")
			end
		end
		
		if answer == "y" then
			for i, v in ipairs(usages) do
				print("Refactoring "..formatFile(v[1]))
				--make a backup
				os.execute("cp "..v[1].." "..v[1]..".bak")
				--load entire file into memory--
				local f = io.open(v[1], "r")
				local src = f:read("*a")
				f:close()
				src = src:gsub('"'..class:gsub("%.","%%.")..'"', '"'..dest..'"')
					:gsub("'"..class:gsub("%.","%%.").."'", '"'..dest..'"')
				f = io.open(v[1], "w")
				f:write(src)
				f:close()
			end
			
			print("Moving class")
			os.execute("mkdir -p class/"..dest:match("^(.+)%..-$"):gsub("%.", "/"))
			os.execute("mv class/"..class:gsub("%.","/")..".lua class/"..dest:gsub("%.", "/")..".lua")
		end
	elseif what == "package" then
		if not (args[1] and args[2]) then
			print("Usage: refactor move package [source.package] [destination.package]")
			return
		end
		
		local package = args[1]
		local dest = args[2]
		
		local usages = findUsagesOfPackageIn(package)
		
		print((#usages).." usages found")
		for i, v in ipairs(usages) do
			print("In "..formatFile(v[1]).." on line "..v[2].." column "..v[3].." (class "..package..v[4]..")")
		end
		
		print()
		local answer
		while not answer do
			print("Do you want to refactor? [Y/N]")
			answer = io.read("*l"):gsub(1,1):lower()
			
			if answer ~= "y" and answer ~= "n" then
				answer = nil
				io.write(insult[math.random(1,#insult)]..", ")
			end
		end
		
		if answer == "y" then
			for i, v in ipairs(usages) do
				print("Refactoring "..formatFile(v[1]))
				--make a backup
				os.execute("cp "..v[1].." "..v[1]..".bak")
				--load entire file into memory--
				local f = io.open(v[1], "r")
				local src = f:read("*a")
				f:close()
				src = src:gsub('"'..package:gsub("%.","%%."), '"'..dest)
					:gsub("'"..package:gsub("%.","%%."), "'"..dest)
				f = io.open(v[1], "w")
				f:write(src)
				f:close()
			end
			
			print("Moving package")
			os.execute("mkdir -p class/"..dest:match("^(.+)%..-$"):gsub("%.", "/"))
			os.execute("mv class/"..package:gsub("%.","/").." class/"..dest:gsub("%.", "/"))
		end
	else
		print("Usage: refactor move [class|package]")
		return
	end
elseif command == "delete" then
	print("Not yet implemented")
	return
elseif command == "usages" then
	local what = table.remove(args, 1)
	if what == "class" then
		local usages = findUsagesOfClassIn(args[1])
		
		print((#usages).." usages found")
		for i, v in ipairs(usages) do
			print("In "..formatFile(v[1]).." on line "..v[2].." column "..v[3])
		end
	elseif what == "package" then
		local usages = findUsagesOfPackageIn(args[1])
		
		print((#usages).." usages found")
		for i, v in ipairs(usages) do
			print("In "..formatFile(v[1]).." on line "..v[2].." column "..v[3].." (class "..args[1]..v[4]..")")
		end
	else
		print("Usage: refactor usages [class|package]")
		return
	end
elseif command == "restore" then
	--restores all .lua.bak files
	local function restoreDirectory(dir)
		dir = dir or "."
		for file in lfs.dir(dir) do
			if file:sub(1,1) ~= "." then
				file = dir.."/"..file
				if lfs.attributes(file).mode == "directory" then
					restoreDirectory(file)
				elseif file:find("%.lua%.bak") then
					print("Restoring file "..file:sub(1,-5))
					os.execute("mv "..file.." "..file:sub(1,-5))
				end
			end
		end
	end
	
	restoreDirectory()
elseif command == "removebackups" then
	--removes all .lua.bak files
	local function removeDirectory(dir)
		dir = dir or "."
		for file in lfs.dir(dir) do
			if file:sub(1,1) ~= "." then
				file = dir.."/"..file
				if lfs.attributes(file).mode == "directory" then
					removeDirectory(file)
				elseif file:find("%.lua%.bak") then
					print("Removing file "..file)
					os.execute("rm "..file)
				end
			end
		end
	end
	
	removeDirectory()
else
	print("Usage: refactor [move|delete|usages|restore|removebackups]")
	return
end
