local sandbox = {}

function sandbox.init(nocopy)
	--nocopy: a table with keys that shouldn't be copied to the new environment. the value will be the placeholder for the value
	--You cannot have nil values. Duh.
	
	--first, deepcopy the current environment (_G)
	local _DG = {}
	deepcopy(_G,_DG,nocopy)
	
	--then, return our deep copied environment
	return _DG
end

return sandbox
