DEBUGGING = true

if arg[#arg] == "-debug" then require("mobdebug").start() end

local server = arg[2] == "--server"
log = print --early log calls will not be in the client console!
if server then
	--server bootstrap--
	love.filesystem.load("server.lua")(unpack(arg, 3))
	function love.run() end
	return
end

CLIENT = true
SERVER = false

--So... How the hell do you build in space?
	--Easy! When clicking on an empty space or when snap is off, the system creates a structure object
	--Structures are basically build optimized triangulated geometry. How?
		--[[
			Say we have a structure like this:
			##
			##
			####
			####
			Normally, an unoptimized game engine would make Polygon objects for each block.
			But an optimized engine computes the union between the three objects, and then creates a polygon object for it
			The first method yields, 3 polygon objects, 4 verticies each.
			The second method yields, 1 polygon object, 6 verticies each.
			
			So, how do we do this in real time? Threads.
			Abusing threads will let us compute these in real time.
			The game will initially append the object by itself to the object list, then after a timeout of no modificaations to the
			shape, it submits the entire unoptimized model to a thread for unioning.
			
			Unions: http://stackoverflow.com/questions/2667748
		]]

class = require "class"
resource = require "resource"
nc = require "netcerial"
network = require "network"
uuid = require "uuid"
ffi = require "ffi"
require "util"
sandbox = require "sandbox"
obj = require "obj"
mdf = require "mdf"
matrix = require "matrix"

gamestate = require "gamestate"
require "fonts"
require "colors"

function love.load()
	io.output():setvbuf("no")
	nersis.hello()
	--love.window.setMode(love.graphics.getWidth(), love.graphics.getHeight(), {msaa=4})
	--[[do
		--love.string testing
		print(love.string)
		for i, v in pairs(love.string) do
			print(i,v)
		end
		
		local write = love.string.newWriteBuffer()
		write:writeByte(64)
		write:writeShort(284)
		write:writeInt(133700)
		write:writeFloat(12.7)
		write:writeDouble(math.pi)
		write:writeString("hello!")
		write:writeBytes("FADE")
		
		print(#write:getString())
		local read = love.string.newReadBuffer(write:getString())
		print(read:readByte())
		print(read:readShort())
		print(read:readInt())
		print(read:readFloat())
		print(read:readDouble())
		print(read:readString())
		print(read:readBytes(4))
		
		os.exit()
	end]]
	
	--[[do
		local objmodel = obj.loadOBJ(love.filesystem.read("resources/model/thruster.obj"),"resources/model/")
		local lodmodel = {}
		obj.toMesh(objmodel, lodmodel)
		print(#lodmodel)
		os.exit()
	end]]

	resource.init()
	
	gamestate.switch(class.import "client.state.MainMenu":new())
	
	gamestate.registerEvents()
end

local consoleui
local consoleshow = false
local consolelist
local console = {}

function log(tag, str)
	console[#console+1] = {tag, str}
	
	print(tag, str)
	
	if consolelist and consoleshow then
		consolelist.scroll = 1
		consolelist:updateAdapter()
		consolelist:scrollChanged()
		consolelist:markDirty()
	end
end

function setConsoleShown(show)
	consoleshow = show
	if show then
		consolelist.scroll = 1
		consolelist:updateAdapter()
		consolelist:scrollChanged()
		consolelist:markDirty()
	else
		FOCUS = nil
	end
end

resource.getAsync("global:ui/console", function(src)
	consoleui = class.import "client.ui.Inflater".inflate(src)
	consoleui:setPreferredWidth(love.graphics.getWidth())
	consoleui:setPreferredHeight(love.graphics.getHeight()/2)
	
	consolelist = consoleui:findById("list")
	consolelist.adapter = class.anonymous(
	{
		getItemCount = function(self)
			return #console
		end,

		createView = function(self)
			return class.import "client.ui.component.Label":new("Placeholder")
		end,

		bindView = function(self, view, position)
			local c = console[position]
			view.text = "["..c[1].."] "..console[position][2]
		end
	}, class.import "client.ui.util.Adapter")
	consolelist.scroll = 1
	consolelist:scrollChanged()
	
	consoleui:findById("close").click = function()
		setConsoleShown(false)
	end
	
	consoleui:findById("input").submit = function(edit)
		local current = gamestate.current()
		
		if current and current.command then
			current:command(edit.text, consoleui)
		else
			local f, e = loadstring(edit.text)
		
			if not f then
				log("CONSOLE", e)
			else
				local s, e = pcall(f)
				if not s then
					log("CONSOLE", e)
				end
			end
		end
		
		edit.text = ""
		edit:setCursor(0)
	end
end)

function love.update(dt)
	while resource.update() do end
	
	if consoleui and consoleshow then
		if consoleui.dirty then
			consoleui:clean()
		end
		consoleui:update(dt)
	end
end

function love.postdraw()
	if consoleui and consoleshow then
		consoleui:draw()
	end
	
	if DEBUGGING and FOCUS then
		local f = FOCUS
		while f do
			love.graphics.setColor(255,0,255)
			love.graphics.rectangle("line", f.absoluteX, f.absoluteY, f.width, f.height)
			f = f.parent
		end
	end
	
	love.graphics.setColor(255, 255, 255)
	love.graphics.setFont(Fonts.Regular)
	love.graphics.printf("FPS: "..love.timer.getFPS(), 0, 600-16, 800, "right")
end

function love.mousepressed(x, y, b)
	if consoleui and consoleshow then
		return consoleui:mousePressed(x, y, b)
	end
end

function love.mousereleased(x, y, b)
	if consoleui and consoleshow then
		return consoleui:mouseReleased(x, y, b)
	end
end

function love.keypressed(key, ...)
	if key == "`" then
		setConsoleShown(not consoleshow)
		return true
	end
	
	if FOCUS and FOCUS:keyPressed(key, ...) then
		return true
	end
end

function love.keyreleased(...)
	if FOCUS and FOCUS:keyReleased(key, ...) then
		return true
	end
end

function love.textinput(input)
	if FOCUS and FOCUS:textInput(input) then
		return true
	end
end
