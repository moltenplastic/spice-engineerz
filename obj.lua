local obj = {}

local function split(self, delimiter) --Not by me
	local result = {}
	local from  = 1
	local delim_from, delim_to = string.find( self, delimiter, from  )
	while delim_from do
		table.insert( result, string.sub( self, from , delim_from-1 ) )
		from = delim_to + 1
		delim_from, delim_to = string.find( self, delimiter, from  )
	end
	table.insert( result, string.sub( self, from  ) )
	return result
end

local objCommands = {}

function objCommands:mtllib(mtl)
	self.mtl = obj.loadMTL(love.filesystem.read(self.dir..mtl),self.dir)
end

function objCommands:v(x,y,z)
	self.v[#self.v+1] = {tonumber(x),tonumber(y),tonumber(z)}
	if tonumber(x) == nil or tonumber(y) == nil or tonumber(z) == nil then
		print(x,y,z)
	end
end

function objCommands:vn(x,y,z)
	self.vn[#self.vn+1] = {tonumber(x),tonumber(y),tonumber(z)}
end
function fixuv(v)
	while v > 1 do
		v = v-1
	end
	while v < 0 do
		v = v+1
	end
	return v
end
function objCommands:vt(x,y)
	self.vt[#self.vt+1] = {fixuv(tonumber(x)),fixuv(tonumber(y))*2-fixuv(tonumber(y))}
end

function objCommands:o(name)
	local oa
	if self.currentObj then self.polygons[#self.polygons+1] = self.currentObj oa = self.currentObj end
	self.currentObj = {name=name,faces={},uvs={},mtl=oa and oa.mtl or nil}
end

function objCommands:s(n)
	self.currentObj.s = n
end

function objCommands:usemtl(mtl)
	if self.mtl then
		self.currentObj.mtl = self.mtl[mtl]
	else
		self.currentObj.mtl = {map_Kd=love.graphics.newImage(self.dir..mtl..".png")}
	end
end

local function parseVertex(v)
	local t = split(v,"/")
	for i, v in ipairs(t) do
		t[i] = tonumber(v)
	end
	return t
end

function objCommands:f(v1,v2,v3,v4)
	v1 = parseVertex(v1)
	v2 = parseVertex(v2)
	v3 = parseVertex(v3)
	local faces = {v1[1],v2[1],v3[1],uv={v1[2],v2[2],v3[2]},i=1,mtl=self.currentObj.mtl}
	self.currentObj.faces[#self.currentObj.faces+1] = faces
end

objCommands["#"] = function() end
objCommands.g = function() end

function obj.loadOBJ(str,dir)
	local n = 0
	local obj = {}
	obj.v = {}
	obj.vn = {}
	obj.vt = {}
	obj.polygons = {}
	obj.dir = dir
	for line in str:gmatch("([^\n]+)") do
		if #line > 0 and line:match("(%s+)") ~= line then
			line = line:match("%s*(.+)%s*")
			
			local cmd = split(line, "(%s+)")
			if not objCommands[cmd[1]] then print(n) error(cmd[1]) end
			objCommands[cmd[1]](obj,unpack(cmd,2))
			
			n = n+1
		end
	end
	obj.polygons[#obj.polygons+1] = obj.currentObj
	return obj
end

function obj.toMesh(obj, meshes)
	table.sort(obj.polygons, function(a,b)
		return obj.v[a.faces[1][1]][3] < obj.v[b.faces[1][1]][3]
	end)
	
	local NONE = {}
	
	for i, v in ipairs(obj.polygons) do
		local mesh = {}
		mesh.image = v.mtl.map_Kd
		for _, face in pairs(v.faces) do
			local tri = {}
			
			local p,uv,c
			
			p = obj.v[face[1]]
			uv = obj.vt[face[1]] or NONE
			tri[1] = {p[1],p[2],uv[1],uv[2],v.mtl.Kd[1],v.mtl.Kd[2],v.mtl.Kd[3],255}
			
			p = obj.v[face[2]]
			uv = obj.vt[face[2]] or NONE
			tri[2] = {p[1],p[2],uv[1],uv[2],v.mtl.Kd[1],v.mtl.Kd[2],v.mtl.Kd[3],255}
			
			p = obj.v[face[3]]
			uv = obj.vt[face[3]] or NONE
			tri[3] = {p[1],p[2],uv[1],uv[2],v.mtl.Kd[1],v.mtl.Kd[2],v.mtl.Kd[3],255}
			
			mesh[#mesh+1] = tri
		end
		meshes[#meshes+1] = mesh
	end
end

local function setMTL(_,i)
	return function(self,...)
		if #({...}) == 1 then
			self[self.current][i] = ...
		else
			self[self.current][i] = {...}
		end
	end
end

local mtlCommands = setmetatable({},{__index=setMTL})
function mtlCommands:newmtl(name)
	self[name] = {}
	self.current = name
end
function mtlCommands:map_Kd(file)
	print("load map_Kd ",file)
	self[self.current].map_Kd = love.graphics.newImage(self.dir..file)
	self[self.current].map_Kd:setFilter("linear")
end
function mtlCommands:Kd(r,g,b)
	self[self.current].Kd = {r*255,g*255,b*255}
end
mtlCommands["#"] = function() end

function obj.loadMTL(str,dir)
	local n = 0
	local mtl = {}
	mtl.dir = dir
	for line in str:gmatch("([^\n]+)") do
		if #line > 0 and line:match("(%s+)") ~= line then
			line = line:match("%s*(.+)%s*")
			
			local cmd = split(line, "(%s+)")
			mtlCommands[cmd[1]](mtl,unpack(cmd,2))
			
			n = n+1
		end
	end
	return mtl
end

return obj
