local makeVertex, makeVertexFromIntersection, makeIntersection, makePolygon

local vertexMethods = {}

function makeVertex(x, y)
	local vertex = {}
	vertex.x = x
	vertex.y = y
	vertex.next = nil
	vertex.prev = nil
	vertex.neighbour = nil
	vertex.alpha = 0
	vertex.entry = true
	vertex.intersect = false
	vertex.checked = false
	
	return setmetatable(vertex, {__index=vertexMethods})
end

function makeVertexFromIntersection(x, y, alpha)
	local v = makeVertex(x, y)
	v.alpha = alpha
	v.intersect = true
	v.entry = false
	return v
end

function vertexMethods:check()
	self.checked = true
	if self.neighbor and not self.neighbor.checked then
		self.neighbor:check()
	end
end

function vertexMethods:inside(poly)
	local winding = 0
	local infinity = makeVertex(1000000, self.y)
	local q = poly.first
	local i
	repeat
		i = makeIntersection(self, infinity, q, poly:getNext(q.next))
		if (not q.intersect) and i:test() then
			winding = winding+1
		end
		q = q.next
	until q == poly.first
	
	return (winding % 2) ~= 0
end

local intersectionMethods = {}

function makeIntersection(s1, s2, c1, c2)
	local i = setmetatable({}, {__index=intersectionMethods})
	
	local den = (c2.y - c1.y) * (s2.x - s1.x) - (c2.x - c1.x) * (s2.y - s1.y)
	
	i.x = 0
	i.y = 0
	i.us = 0
	i.uc = 0
	
	--if den == 0 then return i end
	
	i.us = ((c2.x - c1.x) * (s1.y - c1.y) - (c2.y - c1.y) * (s1.x - c1.x)) / den
	i.uc = ((s2.x - s1.x) * (s1.y - c1.y) - (s2.y - s1.y) * (s1.x - c1.x)) / den
	
	if i:test() then
		i.x = s1.x + i.us * (s2.x - s1.x)
		i.y = s1.y + i.us * (s2.y - s1.y)
	end
	
	return i
end

function intersectionMethods:test()
	return (0 < self.us and self.us < 1) and (0 < self.uc and self.uc < 1)
end

local polygonMethods = {}

function makePolygon(p)
	local polygon = setmetatable({vertices=0}, {__index=polygonMethods})
	
	if p then
		for i=1, #p, 2 do
			polygon:add(makeVertex(p[i], p[i+1]))
		end
	end
	
	return polygon
end

function polygonMethods:add(v)
	if not self.first then
		self.first = v
		self.first.next = v
		self.first.prev = v
	else
		local next, prev
		next = self.first
		prev = next.prev
		next.prev = v
		v.next = next
		v.prev = prev
		prev.next = v
	end
	self.vertices = self.vertices+1
end

function polygonMethods:insert(vertex, start, en)
	local prev, curr = nil, start
	while curr ~= en and curr.alpha < vertex.alpha do
		curr = curr.next
	end
	vertex.next = curr
	prev = curr.prev
	vertex.prev = prev
	prev.next = vertex
	curr.prev = vertex
	self.vertices = self.vertices+1
end

function polygonMethods:getNext(v)
	while v.intersect do v = v.next end
	return v
end

function polygonMethods:getFirstIntersect()
	local v = self.first
	repeat
		if v.intersect and not v.checked then
			break
		end
		v = v.next
	until v == self.first
	return v
end

function polygonMethods:hasUnprocessed()
	local v = self.first
	repeat
		if v.intersect and not v.checked then
			return true
		end
		print(v.intersect, v.checked)
		v = v.next
	until v == self.first
	return false
end

function polygonMethods:points()
	local i = 1
	local v = self.first
	local p = {}
	repeat
		p[i], p[i+1] = v.x, v.y
		i = i+2
		v = v.next
	until v == self.first
	return p
end

function polygonMethods:union(poly)
	return self:clip(poly, false, false)
end

function polygonMethods:intersection(poly)
	return self:clip(poly, true, true)
end

function polygonMethods:difference(poly)
	return self:clip(poly, false, true)
end

local function xor(a,b) return a ~= b end

function polygonMethods:clip(clip, s_entry, c_entry)
	-- Phase one - find intersections
	local s = self.first
	local c = clip.first

	repeat -- for each vertex Si of subject polygon do
		if not s.intersect then
			repeat -- for each vertex Cj of clip polygon do
				if not c.intersect then
					local i = makeIntersection(s, self:getNext(s.next), c, clip:getNext(c.next))
					if i:test() then
						local iS = makeVertexFromIntersection(i.x, i.y, i.us)
						local iC = makeVertexFromIntersection(i.x, i.y, i.uc)

						iS.neighbour = iC
						iC.neighbour = iS

						self:insert(iS, s, self:getNext(s.next))
						clip:insert(iC, c, clip:getNext(c.next))
					end
				end
				c = c.next
			until c == clip.first
		end
		s = s.next
	until s == self.first

	-- phase two - identify entry/exit points
	s = self.first
	c = clip.first

	s_entry = xor(s_entry, s:inside(clip))
	c_entry = xor(c_entry, c:inside(self))

	repeat
		if s.intersect then
			s.entry = s_entry
			s_entry = not s_entry
		end
		s = s.next
	until s == self.first

	repeat
		if c.intersect then
			c.entry = c_entry
			c_entry = not c_entry
		end
		c = c.next
	until c == clip.first

	-- phase three - construct a list of clipped polygons
	local list = {}

	while self:hasUnprocessed() do
		local current = self:getFirstIntersect()
		print(current)
		local clipped = makePolygon()
		clipped:add(makeVertex(current.x, current.y))
		
		repeat
			current:check()
			if current.entry then
				repeat
					current = current.next
					clipped:add(makeVertex(current.x, current.y))
				until current.intersect
			else
				repeat
					current = current.prev
					clipped:add(makeVertex(current.x, current.y))
				until current.intersect
			end
			current = current.neighbour
		until current.checked

		list[#list+1] = clipped
	end
	
	print(#list)

	if #list == 0 then
		list[1] = self
	end

	return list
end

return {polygon=makePolygon, vertex=makeVertex}
