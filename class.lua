local class = {}

local function cleanClassName(classname)
	return classname:gsub("/","")
end

local function isInnerClass(classname)
	if select(2, classname:gsub("%.[A-Z]","")) > 1 then
		return true, classname:match("^(.-%.[A-Z][^.]+)%.(.+)$")
	else
		return false
	end
end

local registry = {}

class.classloaders = {
	default = function(class, path)
		local newclass = class:gsub("%.","/")
		local newpath = path:gsub("%?",newclass)
		return love.filesystem.load(newpath)
	end
}

class.classloaderOrder = {"default"}

class.classpaths = {
	default = {"class/?.lua"}
}

function class.import(classname)
	classname = cleanClassName(classname)
	if registry[classname] then
		return registry[classname]
	end
	
	local hasInner, realclass, innername = isInnerClass(classname)
	
	local loadclassname = classname
	
	if hasInner then
		loadclassname = realclass
	end
	
	log("CLASS", "Loading class "..loadclassname)
	
	local errs = {"Class \""..loadclassname.."\" failed to load!"}
	for _, classloaderName in ipairs(class.classloaderOrder) do
		local classloader = class.classloaders[classloaderName]
		errs[#errs+1] = "Using Classloader \""..classloaderName.."\":"
		for _, classpath in ipairs(class.classpaths[classloaderName]) do
			local s, e = classloader(loadclassname, classpath)
			if s then
				setfenv(s, _G)
				s()
				assert(registry[classname], "Class failed to register!")
				return registry[classname]
			else
				errs[#errs+1] = "\tIn Classpath \""..classpath.."\": "..e
			end
		end
	end
	
	error(table.concat(errs,"\n"),2)
end

--importSafe attempts to import classname. if classname is not found or classname is not a subclass of superclass, it will load
--superclass
--if superclass is not given it returns nil plus the error that occurred

--this is great for loading classes by name over network
function class.importSafe(classname, superclass)
	
	local s, c = pcall(class.import, classname)
	
	if s then
		if superclass then
			if c:isA(superclass) then
				return c
			else
				return class.import(superclass)
			end
		else
			return c
		end
	else
		print(c)
		if superclass then
			return class.import(superclass)
		else
			return nil, c
		end
	end
end

function class.register(classname, clazz)
	if registry[classname] then return registry[classname] end
	
	registry[classname] = clazz
	clazz.classname = classname
	
	local hasInner, outerclass, innerclass = isInnerClass(classname)
	
	if hasInner then
		registry[outerclass][innerclass] = clazz
	end
	
	return clazz
end

function class.new(name, super)
	if type(super) == "string" then
		super = class.import(super)
	elseif not super then
		super = class.import("Object")
	end
	
	return class.register(name, super:extend()), super
end

function class.anonymous(fc, super)
	if type(super) == "string" then
		super = class.import(super)
	elseif not super then
		super = class.import("Object")
	end
	
	local c = super:extend()
	c.classname = "anonymous "..super.classname
	for i, v in pairs(fc) do c[i] = v end
	return c, super
end

return class
