Fonts = {}

Fonts.Display4 = love.graphics.newFont("font/Roboto-Light.ttf", love.window.toPixels(112))
Fonts.Display3 = love.graphics.newFont("font/Roboto-Regular.ttf", love.window.toPixels(56))
Fonts.Display2 = love.graphics.newFont("font/Roboto-Regular.ttf", love.window.toPixels(45))
Fonts.Display1 = love.graphics.newFont("font/Roboto-Regular.ttf", love.window.toPixels(34))

Fonts.Headline = love.graphics.newFont("font/Roboto-Regular.ttf", love.window.toPixels(24))

Fonts.Title = love.graphics.newFont("font/Roboto-Medium.ttf", love.window.toPixels(20))
Fonts.Subhead = love.graphics.newFont("font/Roboto-Regular.ttf", love.window.toPixels(16))
Fonts.SubheadTabs = love.graphics.newFont("font/Roboto-Medium.ttf", love.window.toPixels(16))
Fonts.Regular = love.graphics.newFont("font/Roboto-Regular.ttf", love.window.toPixels(14))
