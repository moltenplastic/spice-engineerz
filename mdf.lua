local mdf = {}

--[=[
New MDF Format:
Example:
lod low
	object name
		prop layer default
		color 1 1 1
		vert 0 0
		vert 1 0
		vert 1 1
		vert 0 1
		tri 0 1 2
		tri 2 3 0

Whats new?
	Less files to be both loaded by the game and exported by the converter.
	LOD is now in the hands of the model instead of in the hands of the programmer.
]=]

function mdf.parse(str)
	local lods = {}
	local lod
	
	local objects = {}
	
	local object
	
	local color = {255,255,255,255}
	local verts
	
	for line in str:gmatch("[^\n]+") do
		line = line:match("^%s*(.+)%s*$")
		
		local args = {}
		
		for part in line:gmatch("[^%s]+") do
			args[#args+1] = part
		end
		
		local command = table.remove(args,1)
		
		if command == "lod" then
			if object then
				objects[#objects+1] = object
				objects[object.name] = object
				object = nil
			end
			
			if lod then
				lods[lod.name] = lod
				lod.objects = objects
			end
			
			objects = {}
			lod = {}
			lod.name = args[1]
		elseif command == "object" then
			if object then
				objects[#objects+1] = object
				objects[object.name] = object
			end
			
			object = {}
			object.name = args[1]
			verts = {}
			object.tris = {}
			object.props = {}
		elseif command == "color" then
			color[1] = math.floor((tonumber(args[1]) or 1) * 255)
			color[2] = math.floor((tonumber(args[2]) or 1) * 255)
			color[3] = math.floor((tonumber(args[3]) or 1) * 255)
			color[4] = math.floor((tonumber(args[4]) or 1) * 255)
		elseif command == "vert" then
			verts[#verts+1] = {tonumber(args[1]) or 0, tonumber(args[2]) or 0}
		elseif command == "tri" then
			local v1, v2, v3 = verts[tonumber(args[1])+1], verts[tonumber(args[2])+1], verts[tonumber(args[3])+1]
			object.tris[#object.tris+1] = {
				{v1[1],v1[2],0,0,color[1],color[2],color[3],color[4]},
				{v2[1],v2[2],0,0,color[1],color[2],color[3],color[4]},
				{v3[1],v3[2],0,0,color[1],color[2],color[3],color[4]},
			}
		elseif command == "prop" then
			object.props[args[1]] = {unpack(args, 2)}
		end
	end
	
	if object then
		objects[#objects+1] = object
		objects[object.name] = object
	end
	
	if lod then
		lods[lod.name] = lod
		lod.objects = objects
	end
	
	for i, v in pairs(lods) do print(i) end
	
	return lods
end

mdf.LODS = {
	nintendo_64 = 0,
	lowest = 1,
	low = 2,
	normal = 3,
	high = 4,
	highest = 5,
	ultra_super_duper_hd = 6
}

function mdf.toBlockMesh(lods, meshes)
	for lln, lod in pairs(lods) do
		local ll = mdf.LODS[lln]
		local layers = {}
		
		for i, v in pairs(meshes) do
			if v[ll] then
				layers[ll] = v[ll]
			end
		end
	
		for _, obj in ipairs(lod.objects) do
			local layer = obj.props.layer or "default"
			
			if not layers[layer] then
				layers[layer] = {}
				meshes[layer] = meshes[layer] or {}
				meshes[layer][ll] = layers[layer]
			end
			
			local mesh = {}
			for _, tri in ipairs(obj.tris) do
				mesh[#mesh+1] = {tri[1],tri[2],tri[3]}
			end
			layers[layer][#layers[layer]+1] = mesh
		end
	end
end

function mdf.computeObjectBounds(object)
	local x1,y1,x2,y2 = math.huge,math.huge,-math.huge,-math.huge
	for _, tri in ipairs(object.tris) do
		x1,y1,x2,y2 = math.min(x1,tri[1][1]),math.min(y1,tri[1][2]),math.max(x2,tri[1][1]),math.max(y2,tri[1][2])
		x1,y1,x2,y2 = math.min(x1,tri[2][1]),math.min(y1,tri[2][2]),math.max(x2,tri[2][1]),math.max(y2,tri[2][2])
		x1,y1,x2,y2 = math.min(x1,tri[3][1]),math.min(y1,tri[3][2]),math.max(x2,tri[3][1]),math.max(y2,tri[3][2])
	end
	return x1,y1,x2,y2
end

function mdf.computeBounds(objects)
	local x1,y1,x2,y2 = math.huge,math.huge,-math.huge,-math.huge
	for _, object in ipairs(objects) do
		local nx1,ny1,nx2,ny2 = mdf.computeObjectBounds(object)
		x1,y1,x2,y2 = math.min(x1,nx1),math.min(y1,ny1),math.max(x2,nx2),math.max(y2,ny2)
	end
	return x1,y1,x2,y2
end

function mdf.toVertexBuffers(lods, vbinit)
	local vertexBuffers = {}
	
	for lln, lod in pairs(lods) do
		print(lln)
		local ll = mdf.LODS[lln]
		local vb = class.import "client.render.VertexBuffer":new()
		vertexBuffers[ll] = vb
		
		vb:start()
		
		if vbinit then
			vbinit(lod, vb)
		end
		
		for _, obj in ipairs(lod.objects) do
			for _, tri in ipairs(obj.tris) do
				vb:add(tri[1], tri[2], tri[3])
			end
		end
		vb:finish()
	end
	
	return vertexBuffers
end

mdf.MIN_LOD_LEVEL = 0
mdf.MAX_LOD_LEVEL = 6 --Default value: 8
function mdf.selectLOD(lods, lod)
	lod = math.floor(math.max(math.min(lod, mdf.MAX_LOD_LEVEL), mdf.MIN_LOD_LEVEL))
	
	while lod >= 0 and not lods[lod] do
		lod = lod-1
	end
	
	if not lods[lod] then
		while lod <= 8 and not lods[lod] do
			lod = lod+1
		end
	end
	
	return lods[lod]
end

--TODO: mdf.toLayeredVertexBuffers

return mdf
