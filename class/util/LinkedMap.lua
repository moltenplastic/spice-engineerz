local LinkedMap, super = class.new "util.LinkedMap"

--[[
Linked Map: an 2 dimensional map that is relative to a base.
Objects are contained in a list for easy iteration
Objects are also contained in a relatively indexed map. This map is automatically shifted. This is how you lookup by coordinate.
]]

--[[
Linked Map Objects:
Every linked map object should be a table. In these tables, left, right, up, and down are set to whatever
]]

local offsetToDirection = {
	[-1] = {[0]="left"},
	[0] = {[1]="down",[-1]="up"},
	[1] = {[0]="right"},
}

local directionToOffset = {
	left = {-1,0},
	right = {1,0},
	up = {0,-1},
	down = {0,1},
}

local inverseDirections = {
	left = "right",
	right = "left",
	up = "down",
	down = "up",
}

function LinkedMap:init(base)
	assert(base, "LinkedMap requires a base object!")
	self.base = base
	self.list = {base}
	self.relmap = {[0]={[0]=base}}
	self.pos = {[base]={0,0}}
	base.x = 0
	base.y = 0
end

--Returns ALL neighbors of a position, even if they don't exist
--Returns in the format of {<object>, <direction>}
function LinkedMap:neighbors(x, y)
	local list = {}
	list[#list+1] = {self:get(x-1, y), "left"}
	list[#list+1] = {self:get(x+1, y), "right"}
	list[#list+1] = {self:get(x, y-1), "up"}
	list[#list+1] = {self:get(x, y+1), "down"}
	return list
end

--Returns a non nil value if the position at x, y has neighbors
function LinkedMap:hasNeighbors(x, y)
	return self:get(x-1, y) or self:get(x+1, y) or self:get(x, y-1) or self:get(x, y+1)
end

--Returns nil or the input object if the object can be added to the LinkedMap
function LinkedMap:put(x, y, obj, force)
	local old = self:get(x,y)
	if old then
		for i=0, #self.list do
			if self.list[i] == old then
				table.remove(self.list, i)
				break
			end
		end
		
		self.pos[old] = nil
		self.relmap[x][y] = nil
	end
	
	--try to find a link target--
	local targets = self:neighbors(x, y)
	
	local hasTargets = false
	for i=1, #targets do if targets[i][1] then hasTargets = true break end end
	if (not hasTargets) and not (x==0 and y==0) and not force then return end
	
	for i=1, #targets do
		local target = targets[i]
		if target[1] then
			target[1][inverseDirections[target[2]]] = obj
			
			if obj then
				obj[target[2]] = target[1]
			end
		end
	end
	
	if obj then
		self.list[#self.list+1] = obj
		self.pos[obj] = {x,y}
		self.relmap[x] = self.relmap[x] or {}
		self.relmap[x][y] = obj
		obj.x = x
		obj.y = y
	end
	
	if old == self.base then
		--we have to translate this stuff to a neighboring x and y address else a :removeIslands call will remove everything
		local dir
		for i, v in pairs(directionToOffset) do
			local o = self:get(x+v[1],y+v[2])
			if o then
				self.base = o
				break
			end
		end
	end
	
	return obj or true
end

--Gets an object inside the LinkedMap
function LinkedMap:get(x, y)
	return self.relmap[x] and self.relmap[x][y] or nil
end

--Finds the position of the passed object inside the LinkedMap
function LinkedMap:find(obj)
	return self.pos[obj]
end

--Counts the number of objects inside th LinkedMap
function LinkedMap:length()
	return #self.list
end

function LinkedMap:translate(tx, ty)
	local newrelmap = {}
	for x, row in pairs(self.relmap) do
		local nrow = {}
		
		for y, obj in pairs(row) do
			nrow[y+ty] = obj
		end
		
		newrelmap[x+tx] = nrow
	end
	
	self.relmap = newrelmap
	
	for obj, pos in pairs(self.pos) do
		pos[1] = pos[1]+tx
		pos[2] = pos[2]+ty
		obj.x = pos[1]
		obj.y = pos[2]
	end
end

function LinkedMap:removeIslands()
	--will return a list of removed objects because they were islands
	local marked = {}
	
	local current = self.base
	local stack = {}
	
	while current do
		if not marked[current] then
			marked[current] = true
			
			stack[#stack+1] = current.left
			stack[#stack+1] = current.right
			stack[#stack+1] = current.up
			stack[#stack+1] = current.down
		end
		
		current = table.remove(stack, 1)
	end
	
	local list = {}
	for _, obj in ipairs(self.list) do
		if not marked[obj] then
			list[#list+1] = obj
		end
	end
	for _, obj in ipairs(list) do
		self:put(obj.x, obj.y, nil)
	end
	return list
end

--Metamethod for iterating the LinkedMap
--Iterator format: for pos (as table), obj
function LinkedMap.meta:__call()
	local idx, obj = next(self.list)
	return function()
		local nobj = obj
		idx, obj = next(self.list, idx)
		return self.pos[nobj], nobj
	end
end
