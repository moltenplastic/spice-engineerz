Classes that may have lots of sub classes go in their own package, and they must be in the package root with the name "Base".
Example: common.entity.Base, common.block.Base, util.thread.Base (TODO: Refactor thread into util.thread)

class.import must be used everywhere. You can cache class imports as upvalues, but not as globals.
For refactoring's sake, make sure that all calls to class.import use a string. Nothing dynamic like concatenations.
Allowed: class.import "my.Ass", class.import "my.Ass"
Diallowed: class.import("my"..".".."Ass")

Dynamic imports from unsafe sources should use class.importSafe.
For example: class.importSafe(blockClassNameFromNetwork, "common.block.Base")
This will load blockClassNameFromNetwork if it exists, else it will load common.block.Base.
If an object is not given a base class.importSafe will return nil and the error.

Classes should not exist in the default package except for Object.

Classes can have static functions. Since Lua doesn't have reflection stuff, have fun.
For instance: client.ui.component.Base having a static method named "builder" that helps build components from layout files.
