local Base, super = class.new "thread.Base"

--Base thread super class, should not instance directly.
--Use this to add "functionality" to a thread (aka an API)

function Base:init(source)
	self.thread = love.thread.newThread(source)
end

--Starts the thread with arguments
function Base:start(...)
	self.thread:start(...)
end
