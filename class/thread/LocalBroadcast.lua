local LocalBroadcast, super = class.new("thread.LocalBroadcast", "thread.Base")

function LocalBroadcast:init()
	super.init(self, "threads/local_broadcast.lua")
end

function LocalBroadcast:startBroadcasting(port)
	self:start(tostring(port))
end
