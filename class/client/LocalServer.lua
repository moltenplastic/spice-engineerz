local LocalServer = class.new "client.LocalServer"

--Constructs a server thread, passing the arguments to the server thread on startup
function LocalServer:init(...)
	self.args = {"--inside-thread", ...}
	
	self.thread = love.thread.newThread("server.lua")
	self.thread:start(unpack(self.args))
	
	self.eventchannel = love.thread.getChannel "server_events"
	self.logchannel = love.thread.getChannel "server_log"
	self.quitchannel = love.thread.getChannel "server_quit"
	self.backdoor = love.thread.getChannel "server_backdoor"
	self.pausechannel = love.thread.getChannel "server_pause"
	self.paused = false
	self.callbacks = {}
end

--Updates the local server
--This method will throw an error if the server thread threw an error
function LocalServer:update()
	if self.thread:getError() then
		error(self.thread:getError(), 0)
	end

	while self.logchannel:peek() do
		local logentry = self.logchannel:pop()
		log("LOCALSERVER/"..tostring(logentry[1]), table.concat(logentry, "\t", 2))
	end
	
	while self.eventchannel:peek() do
		local event = self.eventchannel:pop()
		
		if event.type == "address" then
			self.address, self.port = event.address:match("^(.+):(.+)$")
			self.port = tonumber(self.port)
			
			log("LOCALSERVER/CLIENT", "Server Initialized on port "..self.port)
			
			if self.onInit then self:onInit() end
		elseif event.type == "callback" then
			print("Callback", event.id)
			local id = event.id
			local callback = self.callbacks[id]
			local s, e = pcall(callback, unpack(event))
			
			if not s then print(e) end
			
			if event.remove then
				self.callbacks[id] = nil
			end
		end
	end
end

--Pushes Lua source code through the server backdoor channel for execution
function LocalServer:execute(src, callback, ...)
	local id = uuid.generate()
	local t = {...}
	t.src = src
	t.id = id
	
	self.backdoor:push(t)
	
	self.callbacks[id] = callback
end

function LocalServer:pause()
	self.pausechannel:push(true)
	self.paused = true
end

function LocalServer:unpause()
	self.pausechannel:push(false)
	self.paused = false
end

function LocalServer:save(callback)
	self:execute([[
	local saveDirectory = ...
	return context:save(saveDirectory, "zlib")
	]], function(s, e)
		if not s then
			print(e)
			callback(false, e)
		else
			callback()
		end
	end, "testsave")
end
