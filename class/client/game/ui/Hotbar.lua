local Hotbar, super = class.new("client.game.ui.Hotbar")

local resources = {"side", "section"}

function Hotbar:init(context)
	self.res = {}
	self.context = context
	
	for i = 1, #resources do
		local k = resources[i]
		resource.getAsync("global:ui/toolbelt/"..k..".png", function(image)
			self.res[k] = image
		end)
	end
end

function Hotbar:getItem(i)
	if self.context.localPlayer then
		local player = self.context.localPlayer
		return player.inventory[i]
	end
end

function Hotbar:select(n)
	if self.context.localPlayer then
		self.context.localPlayer.selected = ((n - 1) % #self.context.localPlayer.inventory) + 1
	end
end

function Hotbar:getSelection()
	if self.context.localPlayer then
		return self.context.localPlayer.selected
	end
	return 0
end

function Hotbar:update()
	
end

function Hotbar:draw()
	if self.res.side and self.res.section then
		local width = self.res.side:getWidth()*2+self.res.section:getWidth()*9
		self.x = love.graphics.getWidth()/2-width/2
		self.width = width
		
		love.graphics.push()
	
		love.graphics.translate(self.x,0)
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(self.res.side)
		love.graphics.translate(self.res.side:getWidth(), 0)
	
		if self.context.localPlayer then
			local items = self.context.localPlayer.inventory
		
			for i=1, math.max(#items, 9) do
				local selected = self:getSelection() == i
			
				love.graphics.setColor(255,255,255)
				love.graphics.draw(self.res.section)
				
				if selected then
					love.graphics.setColor(223,85,255,81)
				else
					love.graphics.setColor(0,0,0,81)
				end
				
				love.graphics.push()
				love.graphics.translate(self.res.section:getWidth()/2-16, self.res.section:getHeight()/2-16)
				love.graphics.rectangle("fill", 0, 0, 32, 32, 2)
				love.graphics.rectangle("line", 0, 0, 32, 32, 2)
				love.graphics.pop()
		
				local item = items[i]
				if item then
					love.graphics.push()
					love.graphics.translate(self.res.section:getWidth()/2-8, self.res.section:getHeight()/2-8)
					item:draw()
					item:drawOverlay()
					love.graphics.pop()
				end
		
				love.graphics.translate(self.res.section:getWidth(), 0)
			end
		end
	
		love.graphics.setColor(255,255,255)
		love.graphics.draw(self.res.side,self.res.side:getWidth(),0,0,-1,1)
		love.graphics.pop()
	end
end

function Hotbar:click(x, y)
	if self.res.side and self.res.section then
		if x >= self.x and x <= self.x+self.width and y <= self.res.side:getHeight() then
			x = x-self.x-self.res.side:getWidth()
			
			if self.context.localPlayer then
				local items = self.context.localPlayer.inventory
		
				for i=1, 9 do
					local selected = self:getSelection() == i
					local sx, sy = self.res.section:getWidth()/2-16, self.res.section:getHeight()/2-16
					
					if x >= sx and x <= sx+32 and y >= sy and y <= sy+32 then
						self.context.localPlayer:selectItem(i)
						return true
					end
		
					x = x-self.res.section:getWidth()
				end
			end
		end
	end
end
