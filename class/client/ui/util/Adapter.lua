local Adapter, super = class.new "client.ui.util.Adapter"

function Adapter:getItemCount()
	return 0
end

function Adapter:getItemType()
	return 1
end

function Adapter:getTypeCount()
	return 1
end

function Adapter:createView()
	error(self.classname.." forgot to override createView!")
end

function Adapter:bindView(view, position)
	error(self.classname.." forgot to override bindView!")
end
