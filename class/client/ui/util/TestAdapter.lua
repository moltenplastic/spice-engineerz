local TestAdapter, super = class.new("client.ui.util.TestAdapter", "client.ui.util.Adapter")

function TestAdapter:getItemCount()
	return 100
end

function TestAdapter:createView()
	return class.import "client.ui.component.Label":new("Test")
end

function TestAdapter:bindView(view, position)
	view.text = "Item "..position
end
