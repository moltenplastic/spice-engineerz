local TabLayout, super = class.new("client.ui.component.TabLayout", "client.ui.component.Layout")

local tween = require "tween"

function TabLayout:init(attrs)
	super.init(self, attrs)
	
	
	self.textColor = attrs.textColor or {255, 255, 255}
	self.accentColor = attrs.accentColor or Colors.accent
	self.tabColor = attrs.tabColor or Colors.primary
	
	self.tabWidthMin = 160 --pixels
	self.tabHeight = 48 --pixels
	self.tabs = {}
	self.tabScroll = 0 --pixels
	self.selectedTab = 1
	self.tabFont = Fonts.SubheadTabs
	self.totalTabWidth = 0
	
	self.indicatorHeight = 2
	self.indicator = {
		x = 0,
		x2 = 0
	}
	self.tabHover = {255,255,255,64}
	self.tween = nil
	
	self.tabPosition = 0 --percents
end

function TabLayout:update(dt)
	if self.tween then
		self.tween:update(dt)
	end
end

function TabLayout:calculateTabMetrics(tab)
	local text = tab.name:upper()
	local textwidth = self.tabFont:getWidth(text)
	local tabwidth = math.max(self.tabWidthMin, textwidth+24)
	local textheight = self.tabFont:getHeight()
	
	tab.width = tabwidth
	tab.textwidth = textwidth
	tab.textheight = textheight
end

function TabLayout:updateSize()
	local width, height
	
	if self.preferredWidth then
		width = self.preferredWidth-(self.paddingLeft+self.paddingRight)
	else
		width = 0
	end
	
	if self.preferredHeight then
		height = self.preferredHeight-(self.paddingTop+self.paddingBottom)
	else
		height = 0
	end
	
	local contentWidth, contentHeight = width, height-self.tabHeight
	
	self.tabs = {}
	local tabX = 0
	
	for i, v in pairs(self.children) do
		v.attrs.tabName = v.attrs.tabName or "Unset tabName attribute"
		
		v:setPreferredSize(contentWidth, contentHeight)
		v.y = self.tabHeight
		
		v:updateSize()
		
		local tab = {name = v.attrs.tabName:upper()}
		
		self:calculateTabMetrics(tab)
		tab.x = tabX
		tabX = tabX+tab.width
		
		if i == self.selectedTab then
			self.indicator.x = tab.x
			self.indicator.x2 = tab.x+tab.width
		end
		
		self.tabs[i] = tab
	end
	
	self.totalTabWidth = tabX
	
	self.contentWidth, self.contentHeight = contentWidth, contentHeight
	
	self.width, self.height = width+self.paddingLeft+self.paddingRight, height+self.paddingBottom+self.paddingTop
end

function TabLayout:draw()
	self:drawBackground()
	
	love.graphics.push()
	love.graphics.translate(self.x+self.paddingLeft, self.y+self.paddingRight)
	
	love.graphics.setColor(self.tabColor)
	love.graphics.rectangle("fill", 0, 0, self.contentWidth, self.tabHeight)
	
	--first, draw the tabs--
	love.graphics.push()
	
	local tabX
	if self.totalTabWidth < self.contentWidth then
		love.graphics.translate((self.contentWidth/2)-(self.totalTabWidth/2), 0)
		tabX = (self.contentWidth/2)-(self.totalTabWidth/2)
	else
		love.graphics.translate(-self.tabScroll, 0)
		tabX = -self.tabScroll
	end
	
	love.graphics.setColor(self.accentColor)
	love.graphics.rectangle("fill", self.indicator.x, self.tabHeight-self.indicatorHeight, self.indicator.x2-self.indicator.x, self.indicatorHeight)
	
	local oldFont = love.graphics.getFont()
	love.graphics.setFont(self.tabFont)
	
	local mouseX, mouseY = love.mouse.getPosition()
	mouseX = mouseX-self.absoluteX
	mouseY = mouseY-self.absoluteY
	
	for i=1, #self.tabs do
		local tab = self.tabs[i]
		local text = tab.name
		local textwidth = tab.textwidth
		local tabwidth = tab.width
		local textheight = tab.textheight
		local tabheight = self.tabHeight
		
		if mouseX > tabX and mouseX < tabX+tabwidth and mouseY < tabheight then
			love.graphics.setColor(self.tabHover)
			love.graphics.rectangle("fill", 0, 0, tabwidth, tabheight)
		end
		
		love.graphics.setColor(self.textColor)
		love.graphics.print(text, (tabwidth/2)-(textwidth/2), (tabheight/2)-(textheight/2))
		
		love.graphics.translate(tabwidth, 0)
		tabX = tabX+tabwidth
	end
	
	love.graphics.setFont(oldFont)
	love.graphics.pop()
	
	--TODO: Optimize? lelsauce.
	love.graphics.translate((-self.tabPosition)*self.contentWidth, 0)
	for i=1, #self.children do
		if i >= self.tabPosition-0.5 and i <= self.tabPosition+3.5 then
			self.children[i]:draw()
		end
		love.graphics.translate(self.contentWidth, 0)
	end
	
	love.graphics.pop()
end

function TabLayout:mousePressed(x, y, b)
	self.tween = tween.new(0.3, self, {tabHover={self.tabHover[1],self.tabHover[2],self.tabHover[3],128}})
end

function TabLayout:mouseReleased(x, y, b)
	local tabX
	if self.totalTabWidth < self.contentWidth then
		tabX = (self.contentWidth/2)-(self.totalTabWidth/2)
	else
		tabX = -self.tabScroll
	end
	
	for i=1, #self.tabs do
		local tab = self.tabs[i]
		local text = tab.name
		local textwidth = tab.textwidth
		local tabwidth = tab.width
		local textheight = tab.textheight
		local tabheight = self.tabHeight
		
		if x > tabX and x < tabX+tabwidth and y < tabheight then
			self.selectedTab = i
			
			self.tween = tween.new(0.3, self, {
				tabHover={self.tabHover[1],self.tabHover[2],self.tabHover[3],64},
				indicator = {x=tab.x, x2=tab.width+tab.x},
				tabPosition = i-1
			}, "inOutSine")
			
			break
		end
		
		tabX = tabX+tabwidth
	end
end
