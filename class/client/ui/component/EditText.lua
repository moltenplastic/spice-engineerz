local EditText, super = class.new("client.ui.component.EditText", "client.ui.component.Base")

local tween = require "tween"
local utf8 = require "utf8"

function EditText:init(attrs)
	if type(attrs) == "string" then
		attrs = {text = attrs}
	end
	
	super.init(self, attrs)
	
	self.text = attrs.text or ""
	self.cursor = utf8.len(self.text)
	self.font = attrs.font or Fonts.Regular
	self.state = "normal" --"hover", "clicked"
	self.underlineOverlayWidth = 0
	self.underlineOverlayColor = Colors.primary --material design indigo 500
	self.underlineColor = Colors.primaryLight --material design indigo 900
	self.stateAnimation = nil
	self.states = {
		normal = {
			underlineOverlayWidth = 0,
			underlineColor = Colors.primaryLight
		},
		hover = {
			underlineOverlayWidth = 0,
			underlineColor = {0x3f, 0x51, 0xb5}
		},
		focus = {
			underlineOverlayWidth = 1
		}
	}
	self.gravity = attrs.gravity or "left"
	self.verticalGravity = attrs.verticalGravity or "center"
	self.textColor = attrs.textColor or Colors.text.default
	self.click = attrs.click
end

function EditText:updateSize() --return width and height
	if self.preferredWidth then --enforce preferred width
		self.textwidth = self.preferredWidth-(self.paddingLeft+self.paddingRight)
	else --calculate
		if self.parent and self.widthPercent then
			self.textwidth = self.parent.width*self.widthPercent
		else
			self.textwidth = 100 --meh?
		end
	end
	
	self.wrapheight = self.font:getHeight()
	if self.preferredHeight then --enforce preferred height
		self.textheight = self.preferredHeight-(self.paddingBottom+self.paddingTop)
	else
		self.textheight = self.wrapheight
	end
	
	self.width = self.textwidth+self.paddingLeft+self.paddingRight
	self.height = self.textheight+self.paddingBottom+self.paddingTop
end

function EditText:update(dt)
	if self.stateAnimation then
		self.stateAnimation:update(dt)
	end
end

function EditText:draw()
	if self.state == "normal" then
		local mx, my = love.mouse.getPosition()
		if mx >= self.absoluteX and my >= self.absoluteY and mx <= self.absoluteX+self.width and my <= self.absoluteY+self.height then
			self:changeState("hover")
		end
	elseif self.state == "hover" then
		local mx, my = love.mouse.getPosition()
		if not (mx >= self.absoluteX and my >= self.absoluteY and mx <= self.absoluteX+self.width and my <= self.absoluteY+self.height) then
			self:changeState("normal")
		end
	elseif self.state == "focus" and not self:isFocused() then
		self:changeState("normal")
	end
	
	if self.lastElevation ~= self.elevation then
		--ui.graphics.updateShadow(self, self.elevation)
	end
	
	if self.shadow then
		love.graphics.setColor(255,255,255)
		love.graphics.draw(self.shadow, -self.elevation+self.x, -self.elevation+self.y)
	end
	
	if self.background then
		local t = type(self.background)
		if t == "function" then
			self:background()
		else
			love.graphics.setColor(self.background)
			love.graphics.rectangle("fill",
				self.x,
				self.y,
				self.width,
				self.height, 2)
		end
	end

	love.graphics.setFont(self.font)
	
	local y = 0
	if self.verticalGravity == "top" then
		--TODO: This
		--y = self.textheight/2-self.wrapheight/2
	elseif self.verticalGravity == "bottom" then
		y = self.textheight-self.wrapheight
	end
	
	love.graphics.setColor(self.textColor)
	love.graphics.printf(self.text, self.x+self.paddingLeft, self.y+y+self.paddingTop, self.textwidth, self.gravity)
	
	if self:isFocused() then
		--draw the cursor
		if not (self.cursorX and self.cursorY) then
			--calculate
			self.cursorY = 0
			self.cursorX = self.cursor == 0 and 0 or self.font:getWidth(self.text:sub(1,utf8.offset(self.text, self.cursor)))+2
		end
		
		love.graphics.line(self.cursorX+self.x+self.paddingLeft,
			self.cursorY+self.y+self.paddingTop,
			self.cursorX+self.x+self.paddingLeft,
			self.cursorY+self.y+self.paddingTop+self.font:getHeight())
	end
	
	love.graphics.setColor(self.underlineColor)
	love.graphics.rectangle("fill",
		self.x,
		self.y+self.height-2,
		self.width,
		2)
	
	love.graphics.setColor(self.underlineOverlayColor)
	love.graphics.rectangle("fill",
		self.x,
		self.y+self.height-2,
		self.width*self.underlineOverlayWidth,
		2)
end

function EditText:drawBackground()
	if self.background then
		local t = type(self.background)
		if t == "function" then
			self:background()
		else
			love.graphics.setColor(self.background)
			love.graphics.rectangle("fill", self.x+self.paddingLeft,
				self.y+self.paddingTop,
				self.width-self.paddingRight,
				self.height-self.paddingBottom)
		end
	end
end

function EditText:setCursor(cursor)
	self.cursor = cursor
	self.cursorX = nil
	self.cursorY = nil
end

function EditText:mousePressed(x, y, b)
	self:focus()
	self:changeState("focus")
	
	--try and find the cursor value for our click position
	self.cursor = utf8.len(self.text)
	for i=1, self.cursor do
		if x < self.font:getWidth(self.text:sub(1,utf8.offset(self.text, i))) then
			self.cursor = i-1
			break
		end
	end
	
	self.cursorX = nil
	self.cursorY = nil
	
	return true
end

function EditText:mouseReleased(x, y, b)
	return true
end

function EditText:changeState(state)
	self.state = state
	self.stateAnimation = tween.new(0.3, self, self.states[state])
end

function EditText:focused(focused)
	love.keyboard.setKeyRepeat(focused)
end

--EditText eats all input when focused--
function EditText:keyReleased()
	return true
end

function EditText:textInput(text)
	if self.cursor == 0 then
		self.text = text..self.text
	else
		self.text = self.text:sub(1, utf8.offset(self.text, self.cursor))..text..self.text:sub(utf8.offset(self.text, self.cursor+1))
	end
	self.cursor = self.cursor+1
	self.cursorX = nil
	self.cursorY = nil
	
	return true
end

function EditText:submit() end

function EditText:keyPressed(key)
	if key == "backspace" then
		local len = utf8.len(self.text)
		if len == 1 then
			self.text = ""
			self.cursor = 0
		elseif len > 1 then
			self.text = self.text:sub(1, utf8.offset(self.text, self.cursor-1))..self.text:sub(utf8.offset(self.text, self.cursor+1))
			self.cursor = self.cursor-1
		end
		
		self.cursorX = nil
		self.cursorY = nil
	elseif key == "left" and self.cursor > 0 then
		self.cursor = self.cursor-1
		self.cursorX = nil
		self.cursorY = nil
	elseif key == "right" and self.cursor < utf8.len(self.text) then
		self.cursor = self.cursor+1
		self.cursorX = nil
		self.cursorY = nil
	elseif key == "home" then
		self.cursor = 0
		self.cursorX = nil
		self.cursorY = nil
	elseif key == "end" then
		self.cursor = utf8.len(self.text)
		self.cursorX = nil
		self.cursorY = nil
	elseif key == "return" then
		self:submit()
	elseif key == "v" and love.keyboard.isDown("lctrl", "rctrl") then
		local text = love.system.getClipboardText()
		if self.cursor == 0 then
			self.text = text..self.text
		else
			self.text = self.text:sub(1, utf8.offset(self.text, self.cursor))..text..self.text:sub(utf8.offset(self.text, self.cursor+1))
		end
		self.cursor = self.cursor+utf8.len(text)
		self.cursorX = nil
		self.cursorY = nil
	end
	
	return true
end
