local ScrollableLayout, super = class.new("client.ui.component.ScrollableLayout", "client.ui.component.Layout")

function ScrollableLayout:init(attrs)
	super.init(self, attrs)
	self.scroll = 0 --between 0 and 1
	self.scrollbarGrabbed = false
	self.scrollMouseX = 0
	self.scrollMouseY = 0
	self.lastMouseX = 0
	self.lastMouseY = 0
end

function ScrollableLayout:drawContent(percentOffScreen)
	love.graphics.push()
	love.graphics.translate(0, -(self.scroll*percentOffScreen*self.contentheight))
	for i=1, #self.children do
		self.children[i]:draw()
	end
	love.graphics.pop()
end

function ScrollableLayout:scrollChanged()
	
end

function ScrollableLayout:draw()
	self:drawBackground()
	
	local percentOnScreen = self.realheight/self.contentheight
	local percentOffScreen = 1-percentOnScreen
	
	if percentOnScreen >= 1 then
		if not self.scrollDisabled then
			self.scrollDisabled = true
			self.scroll = 0
			self:scrollChanged()
		end
	else
		self.scrollDisabled = false
	end

	love.graphics.push()
	love.graphics.translate(self.x+self.paddingLeft, self.y+self.paddingTop)
	love.graphics.stencil(function()
		love.graphics.rectangle("fill",0,0,self.width,self.realheight)
	end)
	love.graphics.setStencilTest(true)
	self:drawContent(percentOffScreen)
	love.graphics.setStencilTest(false)
	
	if not self.scrollDisabled then
		love.graphics.setColor(78,78,78)
		love.graphics.rectangle("fill", self.contentwidth, self.scroll*percentOffScreen*self.realheight, 8, percentOnScreen*self.realheight)
	
		if self.scrollbarGrabbed then
			if not love.mouse.isDown(1) then self.scrollbarGrabbed = false end
			
			love.graphics.setColor(255,255,255,32)
			love.graphics.rectangle("fill", self.contentwidth, self.scroll*percentOffScreen*self.realheight, 8, percentOnScreen*self.realheight)
		
			local currentX, currentY = love.mouse.getPosition()
		
			--get y distance from first to last--
			local dist = currentY-self.lastMouseY
		
			--convert the distance to percent of the offscreen bar--
			self.scroll = math.min(math.max(self.scroll+(dist/(self.realheight*percentOffScreen)), 0), 1)
		
			self.lastMouseX, self.lastMouseY = currentX, currentY
			
			self:scrollChanged()
		end
	end
	
	love.graphics.pop()
end

function ScrollableLayout:mousePressed(x, y, b)
	if x > self.paddingLeft and x < self.contentwidth+self.paddingLeft and y > self.paddingTop and y < self.contentheight+self.paddingTop then
		print("IN CONTENT")
		return super.mousePressed(self, x, y, b)
	elseif not self.scrollDisabled and x >= self.contentwidth and x < self.contentwidth+self.paddingLeft+8 and y > self.paddingTop and y < self.realheight+self.paddingTop then
		self.lastMouseX = self.absoluteX+x
		self.lastMouseY = self.absoluteY+y
		self.scrollMouseX = self.lastMouseX
		self.scrollMouseY = self.lastMouseY
		self.scrollbarGrabbed = true
		return true
	end
end

function ScrollableLayout:mouseReleased(x, y, b)
	if x > self.paddingLeft and x < self.width-self.paddingRight and y > self.paddingTop and y < self.height-self.paddingBottom then
		return super.mouseReleased(self, x, y, b)
	end
end
