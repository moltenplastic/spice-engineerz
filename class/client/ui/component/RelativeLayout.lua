local RelativeLayout, super = class.new("client.ui.component.RelativeLayout", "client.ui.component.Layout")

function RelativeLayout:updateSize() --return width and height
	--TODO: Make size from layout, use preferred size or parent size
	
	if self.preferredWidth then
		self.width = self.preferredWidth-(self.paddingLeft+self.paddingRight)
	elseif self.parent then
		self.width = self.parent.width-(self.parent.paddingLeft+self.parent.paddingRight)-(self.paddingLeft+self.paddingRight)
	end
	
	if self.preferredHeight then
		self.height = self.preferredHeight-(self.paddingTop+self.paddingBottom)
	elseif self.parent then
		self.height = self.parent.height-(self.parent.paddingTop+self.parent.paddingBottom)-(self.paddingTop+self.paddingBottom)
	end
	
	self:requestLayout()
	
	local width, height = self.width, self.height
	
	for i=1, #self.children do
		--do the sizing pass
		local child = self.children[i]
		local vwidth = child.x+child.width
		local vheight = child.y+child.height
		
		if vwidth > width then
			print(child.x, child.width, child.id)
			width = vwidth--(self.paddingLeft+self.paddingRight)
		end
		if vheight > height then
			height = vheight--(self.paddingTop+self.paddingBottom)
		end
	end
	
	self.width = width+(self.paddingLeft+self.paddingRight)
	self.height = height+(self.paddingTop+self.paddingBottom)
	
	--request final layout after getting final width and height from content
	self:requestLayout()
end

--[[

Box model:

# - margin
$ - padding
! - content

########################################################
#
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# $!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$
# $!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$
# $!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$
# $!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#
########################################################

What IS Margin?

Margin is the space between two different views, while padding is the reserved space inside a view.
What makes margin different from padding is that padding can be draw inside by the children.

Margin is invisible space that is only present during the layout phase.

Layout Child:

How does Layout Child work?

We look at the tags to figure out the X and Y positions and size constraints.

X:

First pass:
withParentLeft sets X to 0.
rightOf calculates the layout of the referenced child and sets our X value to rightOf.x+rightOf.width+rightOf.marginRight
withLeft calculates the referenced child and sets out X value to withLeft.x-withLeft.marginLeft

Second pass:
If X is already set,
	leftOf calculates the referenced child and sets the view width to leftOf.x-X-leftOf.marginLeft-marginRight
	withParentRight sets the view with to the distance between the parent width and X - marginRight
Else
	leftOf sets X to leftOf.x-leftOf.marginLeft-width-marginRight
	withParentRight sets X to parent width-width-marginRight
	
Y:

Y is similar to X, just with different names
]]

local function layoutChild(self, index)
	local child = self.children[index]
	child:updateSize()
	local width, height = child.width, child.height
	
	child.x = child.marginLeft
	child.y = child.marginTop
	
	--first, check tags to find the child's x and y position
	
	--TODO: rename with to withLeft, add withTop
	
	self.laidOut[child] = true
	
	local startingX
	
	if child.attrs.withParentLeft then
		startingX = child.marginLeft+self.paddingLeft
	elseif child.attrs.rightOf then
		local rightOf = self.ids[child.attrs.rightOf]
		if not self.laidOut[rightOf] then
			layoutChild(self, self.idToIndex[child.attrs.rightOf])
		end
		
		startingX = child.marginLeft+rightOf.x+rightOf.width+rightOf.marginRight
	elseif child.attrs.withLeft then
		local withLeft = self.ids[child.attrs.withLeft]
		if not self.laidOut[withLeft] then
			layoutChild(self, self.idToIndex[child.attrs.withLeft])
		end
		
		startingX = child.marginLeft+withLeft.x-withLeft.marginLeft
	end
	
	if startingX then
		child.x = startingX
		if child.attrs.leftOf then
			local leftOf = self.ids[child.attrs.leftOf]
			if not self.laidOut[leftOf] then
				layoutChild(self, self.idToIndex[child.attrs.leftOf])
			end
			
			child:setPreferredWidth(leftOf.x-leftOf.marginLeft-startingX-child.marginRight)
			child:updateSize()
			width, height = child.width, child.height
		elseif child.attrs.withParentRight then
			child:setPreferredWidth(self.width-self.paddingRight-startingX-child.marginRight)
			child:updateSize()
			width, height = child.width, child.height
		end
	else
		if child.attrs.leftOf then
			local leftOf = self.ids[child.attrs.leftOf]
			if not self.laidOut[leftOf] then
				layoutChild(self, self.idToIndex[child.attrs.leftOf])
			end
			
			child.x = leftOf.x-leftOf.marginLeft-width-child.marginRight
		elseif child.attrs.withParentRight then
			child.x = self.width-self.paddingRight-width-child.marginRight
		end
	end
	
	local startingY
	
	if child.attrs.withParentTop then
		startingY = child.marginTop+self.paddingTop
	elseif child.attrs.below then
		local below = self.ids[child.attrs.below]
		if not self.laidOut[below] then
			layoutChild(self, self.idToIndex[child.attrs.below])
		end
		startingY = child.marginTop+below.y+below.height+below.marginBottom
	end
	
	if startingY then
		child.y = startingY
		if child.attrs.above then
			local above = self.ids[child.attrs.above]
			if not self.laidOut[above] then
				layoutChild(self, self.idToIndex[child.attrs.above])
			end
			
			child:setPreferredHeight(above.y-above.marginTop-startingY-child.marginBottom)
			child:updateSize()
			width, height = child.width, child.height
		elseif child.attrs.withParentBottom then
			child:setPreferredHeight(self.height-self.paddingBottom-startingY-child.marginBottom)
			child:updateSize()
			width, height = child.width, child.height
		end
	else
		if child.attrs.above then
			local above = self.ids[child.attrs.above]
			if not self.laidOut[above] then
				layoutChild(self, self.idToIndex[child.attrs.above])
			end
			child.y = above.y-above.marginTop-(height+child.marginBottom)
		elseif child.attrs.withParentBottom then
			child.y = self.height-self.paddingBottom-(height+child.marginBottom)
		end
	end
end

function RelativeLayout:requestLayout()
	--attempt to start laying out the view--
	self.laidOut = {}
	
	self.ids = {}
	self.idToIndex = {}
	for i, v in ipairs(self.children) do
		if v.id then
			self.ids[v.id] = v
			self.idToIndex[v.id] = i
		end
	end
	
	for i=1, #self.children do
		if not self.laidOut[i] then
			layoutChild(self, i)
		end
	end
end
