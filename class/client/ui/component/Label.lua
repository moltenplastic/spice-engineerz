local Label, super = class.new("client.ui.component.Label", "client.ui.component.Base")

function Label:init(attrs)
	if type(attrs) == "string" then
		attrs = {text = attrs}
	end
	
	super.init(self, attrs)
	
	self.text = attrs.text
	self.font = attrs.font or Fonts.Regular
	self.gravity = attrs.gravity or "left"
	self.verticalGravity = attrs.verticalGravity or "top"
	self.textColor = attrs.textColor or Colors.text.default
	self.background = attrs.background
end

function Label:updateSize() --return width and height
	--print("label",self.id,self.preferredWidth,self.preferredHeight)
	self.textwidth = (self.preferredWidth or self.font:getWidth(self.text))+self.paddingLeft+self.paddingRight
	
	self.wrapheight = (#select(2,self.font:getWrap(self.text, self.textwidth)))*self.font:getHeight()
	if self.preferredHeight then
		self.textheight = self.preferredHeight
	else
		self.textheight = self.wrapheight+self.paddingBottom+self.paddingTop
	end
	
	self.width, self.height = self.textwidth, self.textheight
end

function Label:draw()
	if self.background then
		local t = type(self.background)
		if t == "function" then
			self:background()
		else
			love.graphics.setColor(self.background)
			love.graphics.rectangle("fill", self.x+self.paddingLeft, self.y+self.paddingTop, self.width, self.height, 2)
		end
	end

	love.graphics.setFont(self.font)
	
	local y = 0
	if self.verticalGravity == "center" then
		y = self.textheight/2-self.wrapheight/2
	elseif self.verticalGravity == "bottom" then
		y = self.textheight-self.wrapheight
	end
	
	love.graphics.setColor(self.textColor)
	love.graphics.printf(self.text, self.x+self.paddingLeft, self.y+y+self.paddingTop, self.textwidth, self.gravity)
end
