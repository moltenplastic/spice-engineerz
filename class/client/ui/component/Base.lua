local Base, super = class.new "client.ui.component.Base"

FOCUS = nil

function Base.builder(cls, attrs)
	--a function that returns functions that will eventually return an object
	return cls:new(attrs)
end

function Base:init(attrs)
	self.attrs = attrs --store attributes so that parent can get layout parameters
	self.id = attrs.id
	
	self.x = 0
	self.y = 0
	self.absoluteX = 0
	self.absoluteY = 0
	self.width = 0
	self.height = 0
	self.paddingBottom = attrs.paddingBottom or attrs.padding or 0
	self.paddingTop = attrs.paddingTop or attrs.padding or 0
	self.paddingLeft = attrs.paddingLeft or attrs.padding or 0
	self.paddingRight = attrs.paddingRight or attrs.padding or 0
	self.marginBottom = attrs.marginBottom or attrs.margin or 0
	self.marginTop = attrs.marginTop or attrs.margin or 0
	self.marginLeft = attrs.marginLeft or attrs.margin or 0
	self.marginRight = attrs.marginRight or attrs.margin or 0
	self.elevation = attrs.elevation or 0
	self.borderRadius = attrs.borderRadius or 0
	self.background = attrs.background
	self.widthPercent = attrs.widthPercent or 1
	self.heightPercent = attrs.heightPercent or 1
	self.lastElevation = 0
	
	self.dirty = true
end

function Base:markDirty()
	self.dirty = true
	if self.parent then
		self.parent:markDirty()
	end
end

function Base:clean()
	self:updateSize()
	
	if self.parent then
		self.absoluteX = self.parent.absoluteX+self.parent.paddingLeft+self.x
		self.absoluteY = self.parent.absoluteY+self.parent.paddingTop+self.y
	else
		self.absoluteX = self.x
		self.absoluteY = self.y
	end
	
	self:cleanContent()
	
	self.dirty = false
end

function Base:setParent(parent)
	self.parent = parent
	self:markDirty()
end

function Base:focus()
	FOCUS = self
	self:focused(true)
end

function Base:unfocus()
	if FOCUS == self then
		FOCUS = nil
		self:focused(false)
	end
	
	self:unfocusContent()
end

function Base:isFocused()
	return FOCUS == self
end

function Base:updateSize() self.width, self.height = 0, 0 end
function Base:setPreferredWidth(w)
	self.preferredWidth = math.max(w, 0)*self.widthPercent
	self:markDirty()
end
function Base:setPreferredHeight(h)
	self.preferredHeight = math.max(h, 0)*self.heightPercent
	self:markDirty()
end
function Base:setPreferredSize(w, h)
	self.preferredWidth, self.preferredHeight = math.max(w, 0)*self.widthPercent, math.max(h, 0)*self.heightPercent
	self:markDirty()
end

function Base:update(dt) end
function Base:draw() end
function Base:drawBackground()
	if self.background then
		local t = type(self.background)
		if t == "function" then
			self:background()
		else
			love.graphics.setColor(self.background)
			love.graphics.rectangle("fill",
				self.x,
				self.y,
				self.width,
				self.height, self.borderRadius)
		end
	end
end
function Base:mousePressed(x, y, b) end
function Base:mouseReleased(x, y, b) end
function Base:keyPressed(key) end
function Base:keyReleased(key) end
function Base:textInput(input) end
function Base:focused(focused) end

function Base:cleanContent() end
function Base:unfocusContent() end
function Base:findById() return nil end
