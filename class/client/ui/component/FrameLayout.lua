local FrameLayout, super = class.new("client.ui.component.FrameLayout", "client.ui.component.Layout")

function FrameLayout:updateSize() --return width and height
	local width, height
	
	if self.preferredWidth then
		width = self.preferredWidth-(self.paddingLeft+self.paddingRight)
	else
		width = 0
	end
	
	if self.preferredHeight then
		height = self.preferredHeight-(self.paddingTop+self.paddingBottom)
	else
		height = 0
	end
	
	local cwidth, cheight = 0, 0
	
	for _, child in pairs(self.children) do
		if self.preferredWidth then
			child:setPreferredWidth(self.preferredWidth-(self.paddingLeft+self.paddingRight))
		end
	
		if self.preferredHeight then
			child:setPreferredHeight(self.preferredHeight-(self.paddingTop+self.paddingBottom))
		end
	
		child:updateSize()
		
		cwidth = math.max(child.width+child.x+child.marginLeft+child.marginRight, cwidth)
		cheight = math.max(child.height+child.y+child.marginTop+child.marginBottom, cheight)
	end
	
	self.contentwidth, self.contentheight = cwidth, cheight
	
	width = math.max(width, cwidth)
	height = math.max(height, cheight)
	
	if self.attrs.centerChildren then
		local coffx, coffy = (width/2)-(cwidth/2), (height/2)-(cheight/2)
		
		for _, child in pairs(self.children) do
			child.x = child.x+coffx
			child.y = child.y+coffy
		end
	end
	
	self.width, self.height = width+self.paddingLeft+self.paddingRight, height+self.paddingBottom+self.paddingTop
end
