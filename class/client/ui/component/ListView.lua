local ListView, super = class.new("client.ui.component.ListView", "client.ui.component.ScrollableLayout")

function ListView:init(attrs)
	super.init(self, attrs)
	self.adapter = attrs.adapter or class.import "client.ui.util.Adapter":new()
	self.lastAdapter = nil
	self.available = {}
end

--adapter:
--[[
	adapter:getItemCount() --returns number of items
	adapter:getItemType(position) --optional, 1 if no type given
	adapter:getTypeCount() --returns the number of types (1-<returned number>)
	adapter:createView(type) --returns a view
	adapter:bindView(view, position, type) --binds the data from position into view
]]

function ListView:allocateView(type)
	local avail = self.available[type]
	if avail and #avail > 0 then
		return table.remove(avail, 1)
	else
		local v = self.adapter:createView(type)
		v:setPreferredWidth(self.width)
		v:updateSize()
		v.listview_type = type
		return v
	end
end

function ListView:freeView(view)
	local type = view.listview_type
	local avail = self.available[type] or {}
	self.available[type] = avail
	avail[#avail+1] = view
end

function ListView:updateAdapter()
	local height = 0
	for type=1, self.adapter:getTypeCount() do
		local v = self:allocateView(type)
		height = math.max(height, v.height)
		self:freeView(v)
	end
	
	self.itemheight = height
	self.contentheight = self.itemheight*self.adapter:getItemCount()
end

function ListView:updateSize()
	if self.preferredWidth then
		self.width = self.preferredWidth
	elseif self.parent then
		self.width = self.parent.width or self.parent.preferredWidth
	end
	
	self.width = self.width+(self.paddingLeft+self.paddingRight)
	
	if self.preferredHeight then
		self.height = self.preferredHeight
	elseif self.parent then
		self.height = self.parent.height or self.parent.preferredHeight
	end
	
	self.height = self.height+(self.paddingTop+self.paddingBottom)
	self.realheight = self.height-(self.paddingBottom+self.paddingTop)
	
	self:updateAdapter()
	self.contentwidth = self.width-8
end

function ListView:update()
	if self.adapter ~= self.lastAdapter then
		self.lastAdapter = self.adapter
		self.available = {}
		self:updateAdapter()
		self:scrollChanged()
	end
end

function ListView:scrollChanged()
	if not self.realheight then self:updateSize() end
	
	local percentOnScreen = self.realheight/self.contentheight
	local percentOffScreen = 1-percentOnScreen
	
	local item
	
	if percentOnScreen >= 1 then
		self.scrollDisabled = true
		self.scroll = 0
		item = 0
	else
		item = math.floor((self.scroll*percentOffScreen*self.contentheight)/self.itemheight)
	end
	
	item = item+1
	
	for i=1, #self.children do
		self:freeView(self.children[i])
	end
	self:removeAll()
	
	for i=item, math.ceil(self.contentheight/self.itemheight) do
		local v = self:allocateView(self.adapter:getItemType(i))
		self.adapter:bindView(v, i, v.listview_type)
		v.x = 0
		v.y = (i-1)*self.itemheight
		self:add(v)
	end
end

function ListView.builder(cls, attrs)
	--a function that returns functions that will eventually return an object
	return cls:new(attrs)
end
