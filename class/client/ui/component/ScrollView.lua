local ScrollView, super = class.new("client.ui.component.ScrollView", "client.ui.component.ScrollableLayout")

function ScrollView:updateSize() --return width and height
	local width, height = self.preferredWidth or 0, self.preferredHeight or 0
	for _, child in pairs(self.children) do
		if self.preferredWidth then
			child:setPreferredWidth(self.preferredWidth)
		end
	
		if self.preferredHeight then
			child:setPreferredHeight(self.preferredHeight)
		end
	
		child:updateSize()
		
		width = math.max(child.width+child.x+child.marginLeft+child.marginRight, width)
		height = math.max(child.height+child.y+child.marginTop+child.marginBottom, height)
	end
	
	width = width-8
	
	self.contentwidth, self.contentheight = width, height
	
	self.realheight = (self.preferredHeight or height)-self.paddingBottom-self.paddingTop
	
	self.width = math.max((self.preferredWidth or 0)+self.paddingRight+self.paddingLeft, width+8+self.paddingRight+self.paddingLeft)
	self.height = (self.preferredHeight or height)+self.paddingBottom+self.paddingTop
end
