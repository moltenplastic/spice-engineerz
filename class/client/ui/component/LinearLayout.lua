local LinearLayout, super = class.new("client.ui.component.LinearLayout", "client.ui.component.Layout")

function LinearLayout:init(attrs)
	super.init(self, attrs)
	
	self.orientation = attrs.orientation or "vertical"
end

function LinearLayout:updateSize() --return width and height
	local x, y = 0, 0
	local width, height = self.preferredWidth or 0, self.preferredHeight or 0
	print("suggested size: ",width,height)
	
	local totalWeight = 0
	for i, v in ipairs(self.children) do
		if v.attrs.weight then
			totalWeight = totalWeight+v.attrs.weight
		--[[elseif totalWeight > 0 then
			error("Not all contents in LinearLayout are weighted!")]]
		end
	end
	
	for i, v in ipairs(self.children) do
		if self.preferredWidth then
			v:setPreferredWidth(self.preferredWidth)
		end
		
		if totalWeight > 0 and (self.orientation == "vertical" and self.preferredHeight or self.preferredWidth)
			and v.attrs.weight then
			
			if self.orientation == "vertical" then
				v:setPreferredHeight(self.preferredHeight*(v.attrs.weight/totalWeight))
			else
				v:setPreferredWidth(self.preferredWidth*(v.attrs.weight/totalWeight))
			end
		end
		
		v:updateSize()
	
		local vwidth, vheight = v.width, v.height
		
		v.x = x
		v.y = y
		
		if self.orientation == "vertical" then
			width = math.max(width, vwidth)
			height = y+vheight
			y = height
		else
			height = math.max(height, vheight)
			width = x+vwidth
			x = width
		end
	end
	
	self.childrenwidth, self.childrenheight = width, height
	
	self.width, self.height = width+self.paddingLeft+self.paddingRight, height+self.paddingBottom+self.paddingTop
end
