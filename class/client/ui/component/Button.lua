local Button, super = class.new("client.ui.component.Button", "client.ui.component.Base")

local tween = require "tween"

function Button:init(attrs)
	if type(attrs) == "string" then
		attrs = {text = attrs}
	end
	
	super.init(self, attrs)
	
	self.text = attrs.text
	self.font = attrs.font or Fonts.Regular
	self.borderRadius = attrs.borderRadius or 4
	self.state = "normal" --"hover", "clicked"
	self.stateOverlayColor = {255,255,255,0}
	self.stateAnimation = nil
	self.states = {normal = {255,255,255,0}, hover = {225,225,225,64}, clicked = {255,255,255,192}}
	self.gravity = attrs.gravity or "center"
	self.verticalGravity = attrs.verticalGravity or "center"
	self.textColor = attrs.textColor or Colors.text.default
	self.click = attrs.click
end

function Button:updateSize() --return width and height
	if self.preferredWidth then --enforce preferred width
		self.textwidth = self.preferredWidth-(self.paddingLeft+self.paddingRight)
	else --calculate width from data
		self.textwidth = self.font:getWidth(self.text)
	end
	
	self.wrapheight = (#select(2,self.font:getWrap(self.text, self.textwidth)))*self.font:getHeight()
	if self.preferredHeight then
		self.textheight = self.preferredHeight-(self.paddingBottom+self.paddingTop)
	else
		self.textheight = self.wrapheight
	end
	
	self.width = self.textwidth+self.paddingLeft+self.paddingRight
	self.height = self.textheight+self.paddingBottom+self.paddingTop
end

function Button:update(dt)
	if self.stateAnimation then
		self.stateAnimation:update(dt)
	end
end

function Button:draw()
	if self.state == "normal" then
		local mx, my = love.mouse.getPosition()
		if mx >= self.absoluteX and my >= self.absoluteY and mx <= self.absoluteX+self.width and my <= self.absoluteY+self.height then
			self:changeState("hover")
		end
	elseif self.state == "hover" then
		local mx, my = love.mouse.getPosition()
		if not (mx >= self.absoluteX and my >= self.absoluteY and mx <= self.absoluteX+self.width and my <= self.absoluteY+self.height) then
			self:changeState("normal")
		end
	end
	
	if self.lastElevation ~= self.elevation then
		--ui.graphics.updateShadow(self, self.elevation)
	end
	
	if self.shadow then
		love.graphics.setColor(255,255,255)
		love.graphics.draw(self.shadow, -self.elevation+self.x, -self.elevation+self.y)
	end
	
	if self.background then
		local t = type(self.background)
		if t == "function" then
			self:background()
		else
			love.graphics.setColor(self.background)
			love.graphics.rectangle("fill",
				self.x,
				self.y,
				self.width,
				self.height, self.borderRadius)
		end
	end

	love.graphics.setFont(self.font)
	
	local y = 0
	if self.verticalGravity == "top" then
		--TODO: This
		--y = self.textheight/2-self.wrapheight/2
	elseif self.verticalGravity == "bottom" then
		y = self.textheight-self.wrapheight
	end
	
	love.graphics.setColor(self.textColor)
	love.graphics.printf(self.text, self.x+self.paddingLeft, self.y+y+self.paddingTop, self.textwidth, self.gravity)
	
	love.graphics.setColor(self.stateOverlayColor)
	love.graphics.rectangle("fill",
		self.x,
		self.y,
		self.width,
		self.height, self.borderRadius)
end

function Button:drawBackground()
	if self.background then
		local t = type(self.background)
		if t == "function" then
			self:background()
		else
			love.graphics.setColor(self.background)
			love.graphics.rectangle("fill", self.x+self.paddingLeft,
				self.y+self.paddingTop,
				self.width-self.paddingRight,
				self.height-self.paddingBottom)
		end
	end
end

function Button:click(x,y,b) end

function Button:mousePressed(x, y, b)
	self:focus()
	self:changeState("clicked")
	return true
end

function Button:mouseReleased(x, y, b)
	self:click(x, y, b)
	self:changeState("normal")
	return true
end

function Button:changeState(state)
	self.state = state
	self.stateAnimation = tween.new(0.3, self.stateOverlayColor, self.states[state])
end
