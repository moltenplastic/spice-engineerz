local Layout, super = class.new("client.ui.component.Layout", "client.ui.component.Base")

function Layout:init(attrs)
	super.init(self, attrs)
	
	self.children = {}
end

function Layout:cleanContent()
	for i=1, #self.children do
		self.children[i]:clean()
	end
end

function Layout:unfocusContent()
	for i=1, #self.children do
		self.children[i]:unfocus()
	end
end

function Layout:update(dt)
	for i=1, #self.children do
		self.children[i]:update(dt)
	end
end

function Layout:draw()
	self:drawBackground()
	
	love.graphics.push()
	love.graphics.translate(self.x+self.paddingLeft, self.y+self.paddingRight)
	for i=1, #self.children do
		self.children[i]:draw()
	end
	love.graphics.pop()
end

function Layout:add(v)
	local children = self.children
	children[#children+1] = v
	v:setParent(self)
	self:markDirty()
	return #children
end

function Layout:remove(v)
	local children = self.children
	for i=1, #children do
		if children[i] == v then
			v:setParent(nil)
			table.remove(children, i)
			self:markDirty()
			return i --FFS. TODO: Fix
		end
	end
end

function Layout:removeAll()
	while self.children[1] do
		self:remove(self.children[1])
	end
end

function Layout:findById(id)
	if self.id == id then return self end
	
	local children = self.children
	if children then
		for _, child in ipairs(children) do
			if child.id == id then return child end
			
			local v = child:findById(id)
			if v then return v end
		end
	end
	
	return nil
end

function Layout:mousePressed(x, y, b)
	x = x-self.paddingLeft
	y = y-self.paddingTop
	
	for i, v in ipairs(self.children) do
		if x >= v.x and y >= v.y and x <= v.x+v.width and y <= v.y+v.height then
			if v:mousePressed(x-v.x, y-v.y, b) then
				return true
			end
		end
	end
end

function Layout:mouseReleased(x, y, b)
	x = x-self.paddingLeft
	y = y-self.paddingTop
	
	for i, v in ipairs(self.children) do
		if x >= v.x and y >= v.y and x <= v.x+v.width and y <= v.y+v.height then
			if v:mouseReleased(x-v.x, y-v.y, b) then
				return true
			end
		end
	end
end

function Layout.builder(cls, attrs)
	--a function that returns functions that will eventually return an object
	local self = cls:new(attrs)
	return function(children)
		for i, v in ipairs(children) do
			self:add(v)
		end
		
		return self
	end
end
