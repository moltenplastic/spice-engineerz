local Inflater = class.new "client.ui.Inflater"

function Inflater.inflate(src)
	local globals = setmetatable({},{__index=function(_,i)
		local s, cls = pcall(class.import, "client.ui.component."..i)
		
		if s then
			return function(...)
				return cls:builder(...)
			end
		else
			--print(cls)
		end
		
		return _G[i]
	end})
	return setfenv(loadstring(src), globals)()
end
