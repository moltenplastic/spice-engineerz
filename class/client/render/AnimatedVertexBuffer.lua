local AnimatedVertexBuffer, super = class.new "client.render.AnimatedVertexBuffer"

--Test Code:

--[[
init:
	self.b = mdf.toVertexBuffers(resource.get("global:model/drill.mdf"))[6]
	self.a = mdf.toVertexBuffers(resource.get("global:model/saw.mdf"))[6]
	self.c = class.import "client.render.AnimatedVertexBuffer":new(self.a, self.b)
	self.time = 0
update:
	self.time = self.time+dt
	self.c.time = math.cos(self.time)
draw:
	love.graphics.push()
	love.graphics.scale(256,256)
	love.graphics.translate(1,0)
	love.graphics.setColor(255,255,255)
	self.c:draw()
	love.graphics.pop()]]

--This is just testing the morph shader--
--[[
init:
	self.shader = love.graphics.newShader("resources/shaders/morph.vsh")
	self.testMesh = love.graphics.newMesh({
		{"VertexPosition", "float", 2},
		{"VertexTargetPosition", "float", 2},
		{"VertexColor", "byte", 4},
		{"VertexTargetColor", "byte", 4},
	}, {
		{0, 0, 32, 0, 255, 0, 0, 255, 0, 0, 255, 255},
		{64, 0, 64, 64, 0, 255, 0, 255, 0, 255, 0, 255},
		{0, 64, 0, 64, 0, 0, 255, 255, 255, 0, 0, 255},
	})
update:
	self.shader:send("time", self.time)
draw:
	love.graphics.setShader(self.shader)
	love.graphics.setColor(255,255,255)
	love.graphics.draw(self.testMesh)
	love.graphics.setShader()]]


--TODO: fix bug with this source that sometimes makes it not work and crash because MORPH is nil
--TODO: Fix code.

local MORPH

resource.getAsync("global:shader/morph.sh", function(src)
	MORPH = love.graphics.newShader(src)
end)

function AnimatedVertexBuffer:init(a, b)
	self.time = 0
	self.a = a
	self.b = b
	
	self:morph(a, b)
end

function AnimatedVertexBuffer:morph(a, b)
	--TODO: Morphing on things with textures .-.
	local atris = a.meshData[a.NO_IMAGE]
	local btris = b.meshData[b.NO_IMAGE]
	
	--first, we figure out which triangles are the best going from a to b
	local bestLinked = {}
	
	for i=1, #atris, 3 do
		local best = {nil, math.huge, i}
		
		--we compute the area and the center of the triangle here
		local a1, a2, a3 = atris[i], atris[i+1], atris[i+2]
		
		local a1x, a1y = a1[1], a1[2]
		local a2x, a2y = a2[1], a2[2]
		local a3x, a3y = a3[1], a3[2]
		
		print(i)
		
		--[[local aarea = 0.5 * math.abs(
			((a1x-a3x)*(a2y-a1y))-((a1x-a2x)*(a3y-a1y))
		)
		
		local acx, acy = (a1x+a2x+a3x)/3, (a1y+a2y+a3y)/3]]
		
		for j=1, #btris, 3 do
			local b1, b2, b3 = btris[j], btris[j+1], btris[j+2]
		
			local b1x, b1y = b1[1], b1[2]
			local b2x, b2y = b2[1], b2[2]
			local b3x, b3y = b3[1], b3[2]
			
			if a1x == b1x and a1y == b1y and a2x == b2x and a2y == b2y and a3x == b3x and a3y == b3y then
				--basically, if we are completely equal, we fail early
				best[1] = j
				best[2] = 0
				break
			end
			
			--[[local barea = 0.5 * math.abs(
				((b1x-b3x)*(b2y-b1y))-((b1x-b2x)*(b3y-b1y))
			)
			
			local bcx, bcy = (b1x+b2x+b3x)/3, (b1y+b2y+b3y)/3]]
			
			local ab1 = ((a1x-b1x)^2+(a1y-b1y)^2)^0.5
			local ab2 = ((a2x-b2x)^2+(a2y-b2y)^2)^0.5
			local ab3 = ((a3x-b3x)^2+(a3y-b3y)^2)^0.5
			
			local score = ab1+ab2+ab3
			
			if score < best[2] then
				best[1] = j
				best[2] = score
			end
		end
		
		bestLinked[#bestLinked+1] = best
		
		print("Best", best[1], best[2])
	end
	
	--run through best and cull out ones that are taken with a better score--
	local better = {}
	local cullCount = 0
	for i=1, #bestLinked do
		local best = bestLinked[i]
		
		if not better[best[1]] then
			better[best[1]] = best
		else
			if best[2] < better[best[1]][2] then
				better[best[1]][1] = nil
				better[best[1]] = best
				cullCount = cullCount+1
			else
				best[1] = nil
				cullCount = cullCount+1
			end
		end
	end
	
	print(cullCount, "culled")
	
	local accounted = 0
	local used = {}
	for i=1, #bestLinked do
		local best = bestLinked[i]
		if best[1] then
			accounted = accounted+1
			used[best[1]] = true
		end
	end
	
	for i=1, #btris, 3 do
		if not used[i] then
			--find a triangle and make it use this one--
			for j=1, #bestLinked do
				local best = bestLinked[j]
				if not best[1] then
					best[1] = i
					break
				end
			end
		end
	end
	
	print(accounted, "out of", #btris, "accounted for")
	
	--those who have been culled will use their centers as the origin point
	
	local verts = {}
	for i=1, #bestLinked do
		local best = bestLinked[i]
		local atri = best[3]
		local btri = best[1]
		
		print(btri)
		
		local bdef
		
		if not btri then
			btri = -3
			local b1x, b1y = atris[atri][1], atris[atri][2]
			local b2x, b2y = atris[atri+1][1], atris[atri+1][2]
			local b3x, b3y = atris[atri+2][1], atris[atri+2][2]
			
			bdef = {
			(b1x+b2x+b3x)/3, (b1y+b2y+b3y)/3,
			0,0,
			0,0,0,0}
		end
		
		--Vertex Format: VertexPosition, VertexTargetPosition, VertexColor, VertexTargetColor
		local atv = atris[atri]
		local btv = btris[btri] or bdef
		verts[#verts+1] = {atv[1], atv[2], btv[1], btv[2], atv[5], atv[6], atv[7], atv[8], btv[5], btv[6], btv[7], btv[8]}
		
		atv = atris[atri+1]
		btv = btris[btri+1] or bdef
		verts[#verts+1] = {atv[1], atv[2], btv[1], btv[2], atv[5], atv[6], atv[7], atv[8], btv[5], btv[6], btv[7], btv[8]}
		
		atv = atris[atri+2]
		btv = btris[btri+2] or bdef
		verts[#verts+1] = {atv[1], atv[2], btv[1], btv[2], atv[5], atv[6], atv[7], atv[8], btv[5], btv[6], btv[7], btv[8]}
	end
	
	self.mesh = love.graphics.newMesh({
		{"VertexPosition", "float", 2},
		{"VertexTargetPosition", "float", 2},
		{"VertexColor", "byte", 4},
		{"VertexTargetColor", "byte", 4},
	}, verts,"triangles")
end

function AnimatedVertexBuffer:draw()
	MORPH:send("time", self.time)
	love.graphics.setShader(MORPH)
	love.graphics.draw(self.mesh)
	love.graphics.setShader()
end
