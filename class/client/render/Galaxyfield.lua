local love_graphics = love.graphics

local Galaxyfield, super = class.new "client.render.Galaxyfield"

local loadres = {
	["background"] = "global:galaxyfield/background.png",
	["galaxy_00"] = "global:galaxyfield/galaxy_00.png",
	["galaxy_01"] = "global:galaxyfield/galaxy_01.png",
	["galaxy_02"] = "global:galaxyfield/galaxy_02.png",
	["galaxy_03"] = "global:galaxyfield/galaxy_03.png",
	["galaxy_04"] = "global:galaxyfield/galaxy_04.png",
}

function Galaxyfield:init(conf)
	self.dist = conf.dist
	self.res = {}
	for k, v in pairs(loadres) do
		resource.getAsync(v, function(image)
			self.res[k] = image
		end)
	end
	self.blocks = {}
	self.seed = conf.seed or love.math.random(0xFFFFFFF)
end

local function rng(x)
	for l1 = 1, 5 do
		x = bit.bxor(x, bit.lshift(x, 21))
		x = bit.bxor(x, bit.ror(x, 7))
		x = bit.bxor(x, bit.lshift(x, 4))
	end
	return x
end

local ngalaxytypes = 5
local maxgalaxies = 5
local blocksize = 512
local blockpadding = 64

function Galaxyfield:generateBlock(x, y)
	local block = {}
	local seed = rng(bit.bxor(rng(bit.bxor(self.seed, x)), y))
	for i = 1, (seed % maxgalaxies) + 1 do
		seed = bit.bxor(y, rng(bit.bxor(seed, x)))
		local color = rng(seed)
		block[i] = { 
			seed % ngalaxytypes,
			math.pi * bit.band(seed, 0xFF) / 0x77,
			0.05 + (bit.band(bit.rshift(seed,16), 0xFF)) / 0x277,
			(bit.band(seed, 0xFFFF) / 0xFFFF) * blocksize,
			(bit.band(bit.rshift(seed,16), 0xFFFF) / 0xFFFF) * blocksize,
			bit.band(color,0xFF),
			bit.band(bit.rshift(color,8),0xFF),
			bit.band(bit.rshift(color,16),0xFF),
		}
	end
	self.blocks[x]=self.blocks[x] or {}
	self.blocks[x][y]=block
end

function Galaxyfield:update(x, y)
	if self.lastx ~= x or self.lasty ~= y then
		local w, h = love_graphics.getDimensions()
		local mwh = math.max(w, h)
		local galaxyw = math.ceil(1.5 * mwh / blocksize)
		--See message in Starfield for more information.
		local startx = math.floor(((x / self.dist) - (1.5 * mwh / 2)) / blocksize)-1
		local starty = math.floor(((y / self.dist) - (1.5 * mwh / 2)) / blocksize)-1
		for k, v in tpairs(self.blocks) do
			if k < startx or k >= startx + galaxyw then
				self.blocks[k] = nil
			else
				for n, l in tpairs(v) do
					if n < starty or n >= starty + galaxyw then
						v[n] = nil
					end
				end
			end
		end
		for cx = startx, startx + galaxyw - 1 do
			for cy = starty, starty + galaxyw - 1 do
				if not (self.blocks[cx] or {})[cy] then
					self:generateBlock(cx, cy)
				end
			end
		end
		self.lastx = x
		self.lasty = y
	end
end

function Galaxyfield:draw(x, y, angle, zoom)
	local w, h = love_graphics.getDimensions()
	local mx = math.max(w, h)
	if self.res.background then
		local bgw, bgh = self.res.background:getDimensions()
		love_graphics.setColor(255,255,255)
		love_graphics.draw(self.res.background, w / 2, h / 2, -angle, mx / ((bgw / 2) * 1.41421356237), mx / ((bgh / 2) * 1.41421356237), bgw / 2, bgh / 2)
	end
	love_graphics.setColor(255,255,255)
	love_graphics.push()
	love_graphics.translate(w/2, h/2)
	love_graphics.rotate(-angle)
	for k, v in pairs(self.blocks) do
		for n, l in pairs(v) do
			local px = (w / 2) + (k * blocksize) + ((x * (1 - (1 / self.dist))) - x)
			local py = (h / 2) + (n * blocksize) + ((y * (1 - (1 / self.dist))) - y)
			for i = 1, #l do
				local c = l[i]
				local res = self.res["galaxy_"..string.format("%02d", c[1])]
				if res then
					love_graphics.setColor(127 + (c[6] / 2), 128, 127 + (c[8] / 2))
					love_graphics.draw(res, px + c[4], py + c[5], c[2], c[3], c[3], 128, 128) -- assumes galaxy images are 256x256
				end
			end
		end
	end
	love_graphics.setColor(255,255,255)
	love_graphics.pop()
end
