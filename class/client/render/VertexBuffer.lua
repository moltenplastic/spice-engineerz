local VertexBuffer, super = class.new "client.render.VertexBuffer"

--TODO: Mesh id support:
--[[
	Basically, when we rebatch, we create new underlying Mesh objects.
	We also recompute a lot of un needed triangles.
	If we added id to the system, we could use an :update to remove all triangles associated with that id and re insert them
	in the same position.
	This way, we save time and memory when doing a lot of rebatching all at the same time, since we aren't busy redoing what we
	just did.
]]

--Constructs a VertexBuffer for composing triangle geometry
function VertexBuffer:init()
	
end

local NO_IMAGE = {}
VertexBuffer.NO_IMAGE = NO_IMAGE

--Starts the VertexBuffer batching process
function VertexBuffer:start()
	self.s = love.timer.getTime()
	self.tris = 0
	self.meshes = {}
	self.meshData = {}
	self.matrix = matrix.new()
	self.matrixStack = {}
	--Mesh data gets combined depending on the image used
	--This means it's better to use sprite sets
	--Meshes always render induvisual triangles, not fans, strips or points
end

function VertexBuffer:push()
	self.matrixStack[#self.matrixStack+1] = self.matrix
	self.matrix = matrix.copy(self.matrix)
end

function VertexBuffer:pop()
	self.matrix = table.remove(self.matrixStack)
end

--Translates future points by x, y
function VertexBuffer:translate(x, y)
	self.matrix = matrix.translate(self.matrix, x, y)
end

function VertexBuffer:rotate(theta)
	self.matrix = matrix.rotateZ(self.matrix, theta)
end

function VertexBuffer:scale(x,y)
	self.matrix = matrix.scale(self.matrix, x, y or x)
end

--Adds a triangle to the vertex buffer, with an optional texture
function VertexBuffer:add(p1, p2, p3, image)
	image = image or NO_IMAGE
	local data = self.meshData[image]
	local mat = self.matrix
	
	if not data then
		data = {}
		self.meshData[image] = data
	end
	
	local nx, ny = matrix.apply(mat, p1[1], p1[2])
	data[#data+1] = {nx, ny, p1[3] or 0, p1[4] or 0, p1[5], p1[6], p1[7], p1[8]}
	
	nx, ny = matrix.apply(mat, p2[1], p2[2])
	data[#data+1] = {nx, ny, p2[3] or 0, p2[4] or 0, p2[5], p2[6], p2[7], p2[8]}
	
	nx, ny = matrix.apply(mat, p3[1], p3[2])
	data[#data+1] = {nx, ny, p3[3] or 0, p3[4] or 0, p3[5], p3[6], p3[7], p3[8]}
	
	self.tris = self.tris+1
end

--Finishes the VertexBuffer, constructs the Love2D mesh objects for each group
function VertexBuffer:finish()
	for image, verts in pairs(self.meshData) do
		local mesh = self.meshes[image]
		
		if not mesh then
			mesh = love.graphics.newMesh(verts,"triangles")
			
			if image ~= NO_IMAGE then
				mesh:setTexture(image)
			end
			
			self.meshes[image] = mesh
		else
			mesh:setVertices(verts)
		end
	end
	
	print("Vertex Buffer took "..((love.timer.getTime()-self.s)*1000).."ms to compile "..self.tris.." triangles")
end

--Draws a vertex buffer to the screen, if it's batched already
local love_graphics_draw = love.graphics.draw
function VertexBuffer:draw()
	if self.meshes then
		for _, mesh in pairs(self.meshes) do
			love_graphics_draw(mesh)
		end
	end
end
