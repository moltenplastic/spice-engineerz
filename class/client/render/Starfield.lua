local Starfield, super = class.new "client.render.Starfield"

local MIX_SHADER, STAR_LUT

resource.getAsync("global:shader/starfield.sh", function(s)
	MIX_SHADER = love.graphics.newShader(s)
	if STAR_LUT then
		MIX_SHADER:send("lutTexture", STAR_LUT)
	end
end)

resource.getAsync("global:texture/star_color_lut.png", function(i)
	STAR_LUT = i
	if MIX_SHADER then
		MIX_SHADER:send("lutTexture", STAR_LUT)
	end
end)

local blocksize = 8
local blockpadding = 8

local function starfieldThread(ch, chr, dist, alpha, maxalpha, freq, jitter, rate, seed)
	local blocksize = 8
	local blockpadding = 8
	local function rng(x)
		for l1 = 1, 5 do
			x = bit.bxor(x, bit.lshift(x, 21))
			x = bit.bxor(x, bit.ror(x, 7))
			x = bit.bxor(x, bit.lshift(x, 4))
		end
		return x
	end
	-- TODO: Make this a global utility function since it handles adding entries to tables during iteration
	-- without undefined behavior
	local function tpairs(tbl)
		local s = {}
		local c = 1
		for k, v in pairs(tbl) do
			s[c] = k
			c = c + 1
		end
		c = 0
		return function()
			c = c + 1
			return s[c], tbl[s[c]]
		end
	end
	local blocks={}
	function generateBlock(x, y)
		local ablockscale = blocksize * freq
		local points = {size=ablockscale}
		for sx = 0, blocksize - 1 do
			for sy = 0, blocksize - 1 do
				local br = rng(bit.bxor(rng((sx + x * blocksize) * 3434 + seed), (sy + y * blocksize) * 6969))
				if rng(br) < rate * 0xFFFFFFFF then
					local clr = rng(bit.bxor(br, sx * 0x4242))
					local ax = (sx * freq) + blockpadding + (freq * (bit.band(br, 0xFF) / 0xFF))
					local ay = (sy * freq) + blockpadding + (freq * (bit.band(bit.rshift(br, 8), 0xFF) / 0xFF))
					points[#points+1] = ax
					points[#points+1] = ay
				end
			end
		end
		blocks[x] = blocks[x] or {}
		blocks[x][y] = true
		return points
	end
	local lastbx,lastby
	while true do
		local conf = ch:demand()
		local mwh = math.max(conf.w, conf.h)
		local starsw = math.ceil(1.5 * mwh / freq)
		local starsx = math.floor(((conf.x / dist) - (1.5 * mwh / 2)) / freq)
		local starsy = math.floor(((conf.y / dist) - (1.5 * mwh / 2)) / freq)
		
		--ds84182: also, comment your code, it's sad that a message to you is the first comment in your code
		local bx = math.floor(starsx / blocksize)-1
		local by = math.floor(starsy / blocksize)-1
		local ex = math.ceil((starsx + starsw) / blocksize)+1
		local ey = math.ceil((starsy + starsw) / blocksize)+1
		local out = {bx, ex, by, ey}
		if lastbx == bx and lastby == by then
			chr:push({})
		else
			for k, v in tpairs(blocks) do
				if k < bx or k > ex then
					blocks[k] = nil
				else
					for n, l in tpairs(v) do
						if n < by or n > ey then
							v[n] = nil
						end
					end
				end
			end
			for cx = bx, ex do
				for cy = by, ey do
					if not blocks[cx] or not blocks[cx][cy] then
						out[#out + 1] = generateBlock(cx, cy)
						out[#out + 1] = cx
						out[#out + 1] = cy
					end
				end
			end
			chr:push(out)
			lastbx = bx
			lastby = by
		end
	end
end

function Starfield:init(conf)
	self.dist = conf.dist or 2.5
	self.alpha = conf.alpha or 255
	self.maxalpha = conf.maxalpha or self.alpha
	self.freq = conf.freq or 150
	self.jitter = conf.jitter or 1
	self.rate = conf.rate or 0.1
	self.seed = self.seed or love.math.random(0xFFFFFFF)
	self.blocks = {}
	self.blockQueue = {}
	self.thread = love.thread.newThread(string.dump(starfieldThread))
	self.channel = love.thread.newChannel()
	self.rchannel = love.thread.newChannel()
	self.thread:start(self.channel, self.rchannel, self.dist, self.alpha, self.maxalpha, self.freq, self.jitter, self.rate, self.seed)
end

function Starfield:update(x, y)
	if self.thread:getError() or not self.thread:isRunning() then
		error(self.thread:getError() or "Thread stopped")
	end
	
	if self.rchannel:getCount() > 0 then
		local r = self.rchannel:pop()
		if r[1] then
			for k, v in tpairs(self.blocks) do
				if k < r[1] or k > r[2] then
					self.blocks[k] = nil
				else
					for n, l in tpairs(v) do
						if n < r[3] or n > r[4] then
							v[n] = nil
						end
					end
				end
			end
			for l1 = 5, #r, 3 do
				self.blocks[r[l1 + 1]] = self.blocks[r[l1 + 1]] or {}
				-- Defer canvas rendering so we don't take over 16ms in .update()
				self.blockQueue[#self.blockQueue+1] = {
					self.blocks[r[l1 + 1]],
					r[l1 + 2],
					r[l1]
				}
			end
		end
	end
	
	if self.blockQueue[1] then
		local b = table.remove(self.blockQueue, 1)
		local points = b[3]
		-- Use an r8 canvas to save GPU memory
		local canvas = love.graphics.newCanvas(points.size, points.size, "r8")
		b[1][b[2]] = canvas
		canvas:renderTo(function()
			love.graphics.setColor(255, 255, 255)
			local old = love.graphics.getPointSize()
			love.graphics.setPointSize(2)
			love.graphics.points(points)
			love.graphics.setPointSize(old)
		end)
	end
	
	if self.channel:getCount() == 0 then
		local w, h = love.graphics.getDimensions()
		self.channel:push({
			x = x, y = y,
			w = w, h = h,
		})
	end
end

function Starfield:draw(x, y, angle, zoom)
	if not MIX_SHADER then return end
	
	love.graphics.push()
	local w, h = love.graphics.getDimensions()
	love.graphics.translate(w/2, h/2)
	love.graphics.rotate(-angle)
	love.graphics.setShader(MIX_SHADER)
	love.graphics.setColor(255, 255, 255, self.alpha)
	for k, v in pairs(self.blocks) do
		for n, l in pairs(v) do
			local px = (w / 2) + (k * blocksize * self.freq) + ((x * (1 - (1 / self.dist))) - x) - blockpadding
			local py = (h / 2) + (n * blocksize * self.freq) + ((y * (1 - (1 / self.dist))) - y) - blockpadding
			love.graphics.draw(l, px, py)
		end
	end
	love.graphics.setShader()
	love.graphics.pop()
end

