local Dialog = class.new "client.state.Dialog"

local tween = require "tween"

function Dialog:init()
	self.showPrevious = true
	self.previousSkrimColor = {0,0,0,0}
	self.previousSkrimTransitionColor = {0,0,0,128}
	self.dialogPosition = {0, love.graphics.getHeight()}
	self.dialogPositionTransition = {0,0}
	self.pause = true
	self.pauseDeltaCoefficient = {1}
	self.initialPauseDeltaCoefficient = 1
	self.exiting = false
	
	self:initDialog()
	
	self.skrimTween = tween.new(0.3, self.previousSkrimColor, self.previousSkrimTransitionColor, "outSine")
	self.dialogPositionTween = tween.new(0.6, self.dialogPosition, self.dialogPositionTransition, "outExpo")
	self.pauseDeltaCoefficientTween = tween.new(3, self.pauseDeltaCoefficient, {0}, "outExpo")
end

function Dialog:initDialog()
	self.ui = class.import "client.ui.Inflater".inflate(resource.get("global:ui/settings"))
	self.ui:setPreferredSize(love.graphics.getDimensions())
end

function Dialog:enter(prev)
	self.previous = prev
end

function Dialog:update(dt)
	if self.pause then
		if self.pauseDeltaCoefficient[1] > 0 then
			self.previous:update(dt*self.pauseDeltaCoefficient[1])
		end
	else
		self.previous:update(dt)
	end
	
	if self.ui.dirty then
		self.ui:clean()
	end
	self.ui:update(dt)
	
	if self.skrimTween then
		self.skrimTween:update(dt)
	end
	
	if self.dialogPositionTween then
		self.dialogPositionTween:update(dt)
	end
	
	if self.pauseDeltaCoefficientTween then
		self.pauseDeltaCoefficientTween:update(dt)
	end
	
	if self.exiting and self.skrimTween:finished() and self.dialogPositionTween:finished() and self.pauseDeltaCoefficientTween:finished() then
		gamestate.pop()
	end
end

function Dialog:draw()
	if self.showPrevious then
		self.previous:draw()
		love.graphics.setColor(self.previousSkrimColor)
		love.graphics.rectangle("fill", 0,0, love.graphics.getDimensions())
	end
	
	love.graphics.push()
	love.graphics.translate(self.dialogPosition[1], self.dialogPosition[2])
	self.ui:draw()
	love.graphics.pop()
end

function Dialog:mousepressed(x, y, b)
	self.ui:mousePressed(x, y, b)
end

function Dialog:mousereleased(x, y, b)
	self.ui:mouseReleased(x, y, b)
end

function Dialog:keypressed(k)
	if k == "escape" then
		self.skrimTween = tween.new(0.3, self.previousSkrimColor, {0,0,0,0}, "outSine")
		self.dialogPositionTween = tween.new(0.6, self.dialogPosition, {0, love.graphics.getHeight()}, "outExpo")
		self.pauseDeltaCoefficientTween = tween.new(0.6, self.pauseDeltaCoefficient, {self.initialPauseDeltaCoefficient}, "outExpo")
		self.exiting = true
	end
end

function Dialog:suspend()
	self.ui:unfocus() --unfocus events echo through UI Layouts
end

function Dialog:leave()
	self.ui:unfocus() --unfocus events echo through UI Layouts
end
