local Local, super = class.new("client.state.Local", "client.state.Server")

function Local:initContext(_,__,callback)
	self.server = self.env.class.import "client.LocalServer":new()

	function self.server.onInit(server)
		self.server.onInit = nil
		
		self.context = self.env.class.import "common.ClientContext":new(server.address, server.port, "LocalPlayer")
		callback()
		
		server:execute("log('BACKDOOR', 'Hi from LocalServer backdoor!')")
	
		function self.context.onLogin(context)
			--[==[server:execute [=[
				local baseblock = class.import "common.block.Base":new()
				local teststruct = class.import "common.entity.Structure":new(context, baseblock)
				context:spawn(teststruct)

				teststruct:put(0, 1, class.import "common.block.Base":new())
				teststruct:put(1, 1, class.import "common.block.Base":new())
				teststruct:put(1, 2, class.import "common.block.Base":new())
				teststruct:put(2, 2, class.import "common.block.Base":new())
				teststruct:put(2, 3, class.import "common.block.Base":new())
				teststruct:put(2, 4, class.import "common.block.Base":new())
				teststruct:put(2, 5, class.import "common.block.Base":new())
				teststruct:put(1, 5, class.import "common.block.Base":new())
				teststruct:put(1, 6, class.import "common.block.Base":new())
				teststruct:put(0, 6, class.import "common.block.Base":new())
				teststruct:put(0, 7, class.import "common.block.Base":new())
				
				context:spawn(class.import "common.entity.Missile":new(context))
			]=]]==]
		end
	end
end

function Local:command()
	self.server:execute("THRUSTERS_ARE_A_GO = true")
end

function Local:update(dt)
	super.update(self, dt)
	
	if self.server then
		self.server:update()
	end
end

function Local:keyreleased(key, rep)
	if key == "escape" then
		gamestate.push(class.import "client.state.PauseDialog":new())
	else
		super.keyreleased(self, key, rep)
	end
end

function Local:suspend()
	if not self.server.paused then
		self.server:pause()
	end
end

function Local:resume()
	if self.server.paused then
		self.server:unpause()
	end
end
