local MainMenu = class.new "client.state.MainMenu"

function MainMenu:init()
	log("MAINMENU", "Initializing")
	
	--replace "mainmenu" with "settings" if you want to see the beta settings UI
	self.ui = class.import "client.ui.Inflater".inflate(resource.get("global:ui/mainmenu"))
	self.ui:setPreferredSize(love.graphics.getDimensions())
	
	self.ui:findById("play_alone").click = function(self, x, y, b)
		gamestate.push(class.import "client.state.Local":new())
	end
	
	self.ui:findById("play_together").click = function(self, x, y, b)
		gamestate.push(class.import "client.state.Server":new("boop.party","6969"))
	end
	
	self.ui:findById("settings").click = function(self, x, y, b)
		gamestate.push(class.import "client.state.Dialog":new())
	end
	
	self.background = Colors.background
	
	self.starfieldX = 0
	self.starfieldY = 0
	self.starfieldAngle = 0
	
	self.starfieldVX = 240 --pixels per second
	self.starfieldVY = 0 --pixels per second
	self.starfieldVA = math.rad(8) --radians per second
	
	self.starfields = {
		class.import "client.render.Starfield":new {
			dist = 5,
			alpha = 100,
			freq = 80,
		},
		class.import "client.render.Starfield":new {
			dist = 9,
			alpha = 60,
			freq = 60,
		},
		class.import "client.render.Starfield":new {
			dist = 19.5,
			alpha = 45,
			freq = 50,
		},
	}
end

function MainMenu:update(dt)
	if self.ui.dirty then
		self.ui:clean()
	end
	self.ui:update(dt)
	
	self.starfieldX = self.starfieldX+(self.starfieldVX*dt)
	self.starfieldY = self.starfieldY+(self.starfieldVY*dt)
	self.starfieldAngle = self.starfieldAngle+(self.starfieldVA*dt)
	
	for i = 1, #self.starfields do
		self.starfields[i]:update(self.starfieldX, self.starfieldY, self.zoom)
	end
end

function MainMenu:draw()
	love.graphics.setColor(self.background)
	love.graphics.rectangle("fill", 0,0, love.graphics.getWidth(), love.graphics.getHeight())
	
	love.graphics.setColor(255,255,255)
	for i=1,#self.starfields do
		self.starfields[i]:draw(self.starfieldX, self.starfieldY, self.starfieldAngle, self.zoom)
	end
	
	self.ui:draw()
end

function MainMenu:mousepressed(x, y, b)
	self.ui:mousePressed(x, y, b)
end

function MainMenu:mousereleased(x, y, b)
	self.ui:mouseReleased(x, y, b)
end

function MainMenu:suspend()
	self.ui:unfocus() --unfocus events echo through UI Layouts
end

function MainMenu:leave()
	self.ui:unfocus() --unfocus events echo through UI Layouts
end

local gc = require "luatraverse"
function MainMenu:resume()
	--collectgarbage()
	--print(gc.countreferences(prev))
end
