local Server = class.new "client.state.Server"

function Server:init(address, port)
	log("STATE", "Initializing")
	
	self.env = sandbox.init({
		--Put libraries here that maintain a global state or expose metatables--
		--Reinclude them later--
		[class] = true,
		[Colors] = true,
		[network] = true,
		[resource] = true,
	})
	
	setfenv(function()
		--state reinit--
		package.loaded.class = nil
		package.loaded.Colors = nil
		package.loaded.network = nil
		package.loaded.resource = nil

		--TODO: Get require working inside this environment .-.
		class = setfenv(love.filesystem.load "class.lua", _G)();
		setfenv(love.filesystem.load "colors.lua", _G)();
		network = setfenv(love.filesystem.load "network.lua", _G)();
		resource = setfenv(love.filesystem.load "resource.lua", _G)();

		log("STATE", "Inside new environment!")
		
		self.loading = false
		self.leakReporter = newproxy(true)
		getmetatable(self.leakReporter).__gc = function()
			print("Leak reporter collected. No memory leaks here!")
		end
		
		--This list should only contain global resources. Mods can add their own resources in initMods()
		--Mods will also be able to hook into loadingCallbacks
		self.loadlist = {
			"global:ui/toolbelt/side.png",
			"global:ui/toolbelt/section.png",
			"global:model/thruster.mdf",
			"global:model/thruster_active.mdf",
			"global:model/missile.mdf",
			"global:model/missile_active.mdf",
			"global:model/saw.mdf",
			"global:model/drill.mdf",
			"global:sfx/thruster.ogg"
		}
		--TODO: initMods(), which will add more to the load list
		self.loadTotal = #self.loadlist
		
		self.loadingCallbacks = {
			function(self)
				self.zoom = 8
				self.setzoom = 2
				self.context.lod = self.zoom
				self.toolbelt = class.import "client.game.ui.Hotbar":new(self.context)
				
				self.starfields = {
					--[[class.import "client.render.Starfield":new {
						dist = 5,
						alpha = 100,
						freq = 80,
					},
					class.import "client.render.Starfield":new {
						dist = 9,
						alpha = 60,
						freq = 60,
					},
					class.import "client.render.Starfield":new {
						dist = 19.5,
						alpha = 45,
						freq = 50,
					},]]
				}
				self.galaxyfield = class.import "client.render.Galaxyfield":new {
					dist = 100,
				}
			end
		}

		self.zooming = 0
		
		self:initContext(address, port, function()
			context = self.context
			
			function self.context.onConnect()
				self.context.onConnect = nil --LuaJIT bug: Cyclic Upvalues can keep an unreferenced loop in memory
						--forever
				
				self.loading = true
				
				for i, v in ipairs(self.loadlist) do
					resource.getAsync(v, function()
						for i=1, #self.loadlist do
							if self.loadlist[i] == v then
								table.remove(self.loadlist, i)
								return
							end
						end
					end)
				end
			end
		end)
	end, self.env)()
end

function Server:initContext(address, port, callback)
	self.context = self.env.class.import "common.ClientContext":new(address, port)
	callback()
end

function Server:addLoadingResource(...)
	for _, res in ipairs({...}) do
		self.loadlist[#self.loadlist+1] = res
	end
end

function Server:addLoadingCallback(callback)
	self.loadingCallbacks[#self.loadingCallbacks+1] = callback
end

function Server:update(dt)
	if self.env.resource then
		if DEBUGGING then
			if self.env.resource.update() then --SLOW LOAD :P -- FUCK YOU
				love.timer.sleep(1/20)
			end
		else
			while self.env.resource.update() do end
		end
	end
	
	if self.loading then
		self.loading = #self.loadlist ~= 0
		
		if not self.loading then
			log("STATE", "Loading finished")
			for i, v in ipairs(self.loadingCallbacks) do
				v(self)
			end
		end
	elseif self.context then
		if self.context.connected then
			self.setzoom = math.min(math.max(self.setzoom + (self.setzoom * self.zooming * dt), 3 / 4), 8)
			self.zoom = self.zoom + ((self.setzoom - self.zoom) * dt * 4)
			
			local player = self.context.localPlayer
			if player then
				for i = 1, #self.starfields do
					self.starfields[i]:update(player.body:getX(), player.body:getY(), self.zoom)
				end
				self.galaxyfield:update(player.body:getX(), player.body:getY(), self.zoom)
			end
			self.toolbelt:update(dt)
		elseif (love.timer.getTime()-self.context.connectionStarted) > 30 then
			error("Connection timeout")
		end
		
		self.context:update(dt)
	end
end

function Server:quit()
	if self.context then
		self.context:quit()
	end
end

function Server:leave()
	if self.context then
		self.context:quit()
	end
	self.server = nil
	self.context = nil
	self.env.class = nil
	self.env.resource = nil
	self.env = nil
end

local loadingColor = Colors.primary

function Server:draw()
	love.graphics.setColor(255,255,255)
	local width, height = love.graphics.getDimensions()
	
	if self.loading then
		love.graphics.push()
		love.graphics.setFont(Fonts.Display3)
		love.graphics.setColor(255,255,255)
		love.graphics.print("Loading...", 0, 0)
		love.graphics.setColor(loadingColor)
		love.graphics.rectangle("line", 16, love.graphics.getHeight()-64, love.graphics.getWidth()-32, 4)
		love.graphics.rectangle("fill", 16, love.graphics.getHeight()-64, (love.graphics.getWidth()-32)*(1-((#self.loadlist)/self.loadTotal)), 4)
		love.graphics.pop()
	elseif self.context and self.context.connected then
		love.graphics.push()
		
		local player = self.context.localPlayer
		if player then
			self.galaxyfield:draw(player.body:getX(), player.body:getY(), player.body:getAngle(), self.zoom)
			for i=1,#self.starfields do
				self.starfields[i]:draw(player.body:getX(), player.body:getY(), player.body:getAngle(), self.zoom)
			end
			
			love.graphics.scale(self.zoom)
			love.graphics.translate(love.graphics.getWidth()/2/self.zoom, love.graphics.getHeight()/2/self.zoom)
			love.graphics.rotate(-player.body:getAngle())
			love.graphics.translate(-player.body:getX(), -player.body:getY())
		else
			love.graphics.scale(self.zoom)
		end
		
		self.context:draw(self.zoom)
		
		love.graphics.pop()
		
		self.toolbelt:draw()
	else
		love.graphics.setFont(Fonts.Display3)
		love.graphics.print("Connecting", 0, 0)
	end
end

function Server:wheelmoved(x, y)
	self.setzoom = math.min(math.max(self.setzoom+(self.zoom*y*0.3),3/4),8)
end

function Server:keypressed(key)
	if key == "r" then
		self.zooming = 1.5
	elseif key == "f" then
		self.zooming = -1.5
	end
end

function Server:keyreleased(key)
	if key == "r" or key == "f" then
		self.zooming = 0
	end
end

function Server:mousereleased(x, y, b)
	if self.context then
		if b == 1 then
			if self.toolbelt:click(x, y) then return end
			
			local player = self.context.localPlayer
			--transform coordinates by player viewport
			if player then
				x = x/self.zoom
				y = y/self.zoom
				local tx = x-love.graphics.getWidth()/2/self.zoom
				local ty = y-love.graphics.getHeight()/2/self.zoom
				local angle = player.body:getAngle()
				x = (math.cos(angle)*tx)-(math.sin(angle)*ty)
				y = (math.cos(angle)*ty)+(math.sin(angle)*tx)
				x = x+player.body:getX()
				y = y+player.body:getY()
			
				player:useItem(x, y)
			end
		elseif b == 2 then
			local player = self.context.localPlayer
			--transform coordinates by player viewport
			if player then
				x = x/self.zoom
				y = y/self.zoom
				local tx = x-love.graphics.getWidth()/2/self.zoom
				local ty = y-love.graphics.getHeight()/2/self.zoom
				local angle = player.body:getAngle()
				x = (math.cos(angle)*tx)-(math.sin(angle)*ty)
				y = (math.cos(angle)*ty)+(math.sin(angle)*tx)
				x = x+player.body:getX()
				y = y+player.body:getY()
			
				player:configure(x, y)
			end
	
			--find the object that was clicked--
			--first, compare bounding boxes--
			--[[for i, v in pairs(self.context.entities) do
				local x1,y1,x2,y2 = v:getBoundingBox()
		
				if x >= x1 and y >= y1 and x <= x2 and y <= y2 then
					log("TEST", "Potential click target: "..v.classname..": "..v.id)
					if v:testPoint(x, y) then
						log("TEST", "Click succeeded!")
						return
					else
						log("TEST", "Click failed.")
					end
				end
			end
	
			--else, if the click failed, try to see if we are close to a structure object--
			--see if the click position local to the structure object thats near us borders another block (indicating a place)--
			for i, v in pairs(self.context.entities) do
				if v:isA("common.entity.Structure") then
					local lx, ly = v.body:getLocalPoint(x, y)
					lx = math.floor(lx/16)
					ly = math.floor(ly/16)
			
					if not v:get(lx, ly) then
						if v.map:hasNeighbors(lx, ly) then
							v:put(lx, ly, class.import('common.block.Base'):new())
							return
						end
					end
				end
			end
	
			--else, request spawn--
			local buffer = love.string.newWriteBuffer()
			buffer:writeByte(network.SERVERBOUND.DEBUG_SPAWN_ENTITY)
			buffer:writeString("common.entity.Structure")
			buffer:writeDouble(x)
			buffer:writeDouble(y)
			buffer:writeDouble(player.body:getAngle())
			self.context:send(buffer:getString())]]
		end
	end
end
