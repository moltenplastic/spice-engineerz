local PauseDialog, super = class.new("client.state.PauseDialog", "client.state.Dialog")

function PauseDialog:initDialog()
	self.pauseDeltaCoefficient[1] = 0 --sudden pause so we don't get desyncd
	self.initialPauseDeltaCoefficient = 0
	
	self.ui = class.import "client.ui.Inflater".inflate(resource.get("global:ui/pause_menu"))
	self.ui:setPreferredSize(love.graphics.getDimensions())
	
	self.ui:findById("quit").click = function(buttonSelf, x, y, b)
		self.previous.server:unpause()
		self.previous.server:save(function(s, e)
			print(e)
			gamestate.pop()
			gamestate.pop()
		end)
	end
end

function PauseDialog:update(dt)
	super.update(self, dt)
	self.previous.server:update()
end
