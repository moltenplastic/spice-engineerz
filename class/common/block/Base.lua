local Base, super = class.new "common.block.Base"

--Mesh table format:
--Organized by batch layer then LOD, 1 is max zoom in, 10 is max zoom out
--This will automatically use the next available setting.
--For instance, say the LOD is at 10 (max zoom out), but we only have LOD 1 available
--The system will use LOD 1 instead
local MESHES = {
	default = {
		-- LOD 0
		[0] = {
			--Mesh 1
			{
				--Vertex Data (Triangles)
				{
					{0,0},
					{1,0},
					{1,1},
				},
				{
					{1,1},
					{0,1},
					{0,0},
				},
				image = nil
			}
		},
	}
}

function Base:init()
	self.dynamic = false --If the object is dynamic then update will get called every frame
	self.meshes = MESHES --If this is defined, then these meshes are used instead of calling :draw every frame
	self.batchDraw = true --If this is true and meshes is nil, :render will get called when the object needs to be rebatched
	self.layers = {default=true} --default layers: background, backgroundColored, default, colored (taken from self.color prop), foreground, foregroundColored
	self.structure = nil --Structure that has this block
	self.opaque = true --This really is just an option for rendering
	self.rotation = 0 -- 0: <-, 1: ^, 2: ->, 3: v
	self.color = DEBUGGING and {255,0,0} or {255,255,255}
end

local MIN = 0
local MID = 0.5
local MAX = 1

--Returns an optimized polygon, if any
--TODO: Mark triangles with flags (like "deadly")
function Base:getPolygon()
	if (self.left and self.left.opaque) and (self.right and self.right.opaque) and (self.up and self.up.opaque) and (self.down and self.down.opaque) then
		return
	end

	local tris = {}

	if not (self.up and self.up.opaque) then
		tris[#tris+1] = {
			MIN, MIN,
			MAX, MIN,
			MID, MID
		}
	end

	if not (self.right and self.right.opaque) then
		tris[#tris+1] = {
			MAX, MIN,
			MAX, MAX,
			MID, MID
		}
	end

	if not (self.down and self.down.opaque) then
		tris[#tris+1] = {
			MAX, MAX,
			MIN, MAX,
			MID, MID
		}
	end

	if not (self.left and self.left.opaque) then
		tris[#tris+1] = {
			MIN, MAX,
			MIN, MIN,
			MID, MID
		}
	end

	return tris
end

--Returns the base love.physics shape that goes along with the collision
--This is intended to let the server batch collision geometry in the background
function Base:getShape()
	return love.physics.newRectangleShape(0.5,0.5, 1,1)
end

--Called when this block is added to a structure
function Base:onStructureAdded()

end

--Called if self.dynamic is true
function Base:update(dt)

end

--Called if self.meshes is nil and self.batchDraw is true
function Base:render(lod,vb,layer)

end

--Called if self.meshes is nil and self.batchDraw is false
function Base:draw(lod,layer)

end

--Called when the player right clicks this block--
--x and y range from 0-1--
function Base:configure(player, x, y)

end

--Serialize the block for network transfer or disk saving
function Base:serialize(buffer, toSave)
	buffer:writeByte(self.rotation)
	buffer:writeByte(self.color[1])
	buffer:writeByte(self.color[2])
	buffer:writeByte(self.color[3])
end

--Unserialize the block from a network transfer or from disk
function Base:unserialize(buffer, fromSave)
	self.rotation = buffer:readByte()
	self.color[1] = buffer:readByte()
	self.color[2] = buffer:readByte()
	self.color[3] = buffer:readByte()
end
