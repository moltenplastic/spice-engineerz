local Seat, super = class.new("common.block.Seat", "common.block.Component")

local Event = class.import "common.block.Computer.Event"

function Seat:init()
	super.init(self)
	
	self.sitting = nil -- entity id
	self.weakConnectionList = setmetatable({}, {__mode="k"})
	
	self.keys = {
		left = false,
		right = false,
		up = false,
		down = false,
		rleft = false,
		rright = false
	}
	
	self.componentType = "seat"
end

function Seat:unseatEntity()
	self.joint:destroy()
	self.joint = nil
	local sent = self.structure.context:findById(self.sitting)
	if sent then
		sent.extra.seat = nil
	end
	self.sitting = nil
	
	self:updateKeys(false, false, false, false, false, false)
end

function Seat:doSeatEntity(entity)
	if self.joint then
		self:unseatEntity()
	end
	
	local wx, wy = self.structure.body:getWorldPoint((self.x*16)+8, ((self.y-1)*16)+8)
	entity.body:setPosition(wx, wy-entity.height/2)
	self.joint = love.physics.newWeldJoint(entity.body, self.structure.body, wx, wy, false)
	self.sitting = entity.id
	
	entity.extra.seat = {
		structure = self.structure.id,
		seat = self.componentName,
		joint = self.joint
	}
end

local SEAT_CONNECT = 0
local SEAT_DISCONNECT = 1
local SEAT_QUERY = 2
local SEAT_EJECT = 3

local SEAT_KEY_UP = 1
local SEAT_KEY_DOWN = 2
local SEAT_KEY_LEFT = 4
local SEAT_KEY_RIGHT = 8
local SEAT_KEY_ROTATE_LEFT = 16
local SEAT_KEY_ROTATE_RIGHT = 32

function Seat:invoke(computer, operation, operandA, operandB)
	if operation == SEAT_CONNECT then
		print("seat.connect()")
		self.weakConnectionList[computer] = true
		return 1
	elseif operation == SEAT_DISCONNECT then
		print("seat.disconnect()")
		self.weakConnectionList[computer] = nil
		return 1
	elseif operation == SEAT_QUERY then
		print("seat.query() -> ", self.sitting ~= nil)
		return self.sitting and 1 or 0
	elseif operation == SEAT_EJECT then
		if self.sitting and self.joint then
			print("seat.eject() -> true")
			self:unseatEntity()
			self.structure:requestSync(self)
			return 1
		else
			print("seat.eject() -> false")
			return 0
		end
	else
		return -1
	end
end

function Seat:updateKeys(left, right, up, down, rleft, rright)
	local sendUpdate = left ~= self.keys.left or
						right ~= self.keys.right or
						up ~= self.keys.up or
						down ~= self.keys.down or
						rleft ~= self.keys.rleft or
						rright ~= self.keys.rright
	
	if sendUpdate then
		local newlyDown, newlyUp = 0, 0
		
		if left ~= self.keys.left then
			if left then
				newlyDown = newlyDown+SEAT_KEY_LEFT
			else
				newlyUp = newlyUp+SEAT_KEY_LEFT
			end
		end
		
		if right ~= self.keys.right then
			if right then
				newlyDown = newlyDown+SEAT_KEY_RIGHT
			else
				newlyUp = newlyUp+SEAT_KEY_RIGHT
			end
		end
		
		if up ~= self.keys.up then
			if up then
				newlyDown = newlyDown+SEAT_KEY_UP
			else
				newlyUp = newlyUp+SEAT_KEY_UP
			end
		end
		
		if down ~= self.keys.down then
			if down then
				newlyDown = newlyDown+SEAT_KEY_DOWN
			else
				newlyUp = newlyUp+SEAT_KEY_DOWN
			end
		end
		
		if rleft ~= self.keys.rleft then
			if rleft then
				newlyDown = newlyDown+SEAT_KEY_ROTATE_LEFT
			else
				newlyUp = newlyUp+SEAT_KEY_ROTATE_LEFT
			end
		end
		
		if rright ~= self.keys.rright then
			if rright then
				newlyDown = newlyDown+SEAT_KEY_ROTATE_RIGHT
			else
				newlyUp = newlyUp+SEAT_KEY_ROTATE_RIGHT
			end
		end
		
		print(newlyDown, newlyUp)
		
		local ev = Event:new("seat_key_update")
		ev:setString(0, self.componentName)
		ev:setInteger(1, newlyDown)
		ev:setInteger(2, newlyUp)
		for computer in pairs(self.weakConnectionList) do
			computer:pushEvent(ev)
		end
	end
	
	self.keys.left = left
	self.keys.right = right
	self.keys.up = up
	self.keys.down = down
	self.keys.rleft = rleft
	self.keys.rright = rright
end

function Seat:configure(player)
	if player.id == self.sitting and self.joint then
		self:unseatEntity()
	else
		self:doSeatEntity(player)
	end
	
	self.structure:requestSync(self)
end

--Serialize the block for network transfer or disk saving
function Seat:serialize(buffer)
	super.serialize(self, buffer)
	
	if self.sitting then
		buffer:writeByte(1)
		buffer:writeString(self.sitting)
	else
		buffer:writeByte(0)
	end
end

--Unserialize the block from a network transfer or from disk
function Seat:unserialize(buffer, fromSave)
	super.unserialize(self, buffer)
	
	local isSitting = buffer:readByte() > 0
	
	if isSitting then
		self.sitting = buffer:readString()
		
		local ent = self.structure.context:findById(self.sitting)
		if not ent then
			if fromSave then
				self.structure:runLater(function()
					local ent = self.structure.context:findById(self.sitting)
					if not ent then
						self.sitting = nil --entity really not found now...
					else
						self:doSeatEntity(ent)
					end
				end)
			else
				self.sitting = nil
			end
		else
			self:doSeatEntity(ent)
		end
	else
		if self.sitting and self.joint then
			self:unseatEntity()
		end
		
		self.sitting = nil
	end
	
	--[[if CLIENT then
		self.meshes = self.on and MESHES_ACTIVE or MESHES
		self.structure.rerender = true
	end]]
end
