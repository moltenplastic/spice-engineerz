local Thruster, super = class.new("common.block.Thruster", "common.block.Component")

local MESHES = {}
local MESHES_ACTIVE = {}

if CLIENT then
	resource.getAsync("global:model/thruster.mdf",function(lods)
		mdf.toBlockMesh(lods, MESHES)
	end)
	
	resource.getAsync("global:model/thruster_active.mdf",function(lods)
		mdf.toBlockMesh(lods, MESHES_ACTIVE)
	end)
end

function Thruster:init()
	super.init(self)
	
	self.meshes = MESHES
	self.dynamic = true
	self.on = false
	self.componentType = "thruster"
end

function Thruster:configure(player, x, y)
	self.rotation = (self.rotation+1) % 4
	self.structure:requestSync(self)
end

local THRUSTER_SET = 0
local THRUSTER_GET = 1

function Thruster:invoke(computer, operation, operandA, operandB)
	if operation == THRUSTER_SET then
		self:set(operandA ~= 0)
		return 1
	elseif operation == THRUSTER_GET then
		return self.on and 1 or 0
	else
		return -1
	end
end

function Thruster:set(on, noSync)
	local pon = self.on
	self.on = on
	
	if SERVER and pon ~= on and not noSync then
		self.structure:requestSync(self)
	end
	
	if CLIENT and pon ~= on then
		self.meshes = self.on and MESHES_ACTIVE or MESHES
		self.structure.rerender = true
	end
end

function Thruster:update(dt)
	if self.on then
		local angle = self.structure.body:getAngle()+(self.rotation*(math.pi/2))
		local forceX, forceY = math.cos(angle)*20, math.sin(angle)*20
		local applyX, applyY = self.structure.body:getWorldPoint((self.x*16)+8, (self.y*16)+8)
		
		--print(forceX, forceY, applyX, applyY)
		
		self.structure.body:applyForce(forceX, forceY, applyX, applyY)
	end
end

--Serialize the block for network transfer or disk saving
function Thruster:serialize(buffer)
	super.serialize(self, buffer)
	buffer:writeByte(self.on and 1 or 0)
end

--Unserialize the block from a network transfer or from disk
function Thruster:unserialize(buffer)
	super.unserialize(self, buffer)
	self.on = buffer:readByte() ~= 0
	
	if CLIENT then
		self.meshes = self.on and MESHES_ACTIVE or MESHES
		self.structure.rerender = true
	end
end
