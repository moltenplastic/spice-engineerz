local Computer, super = class.new("common.block.Computer", "common.block.Component")

local bit = require "bit"

local MESHES = {}

if CLIENT then
	resource.getAsync("global:model/computer.mdf",function(lods)
		mdf.toBlockMesh(lods, MESHES)
	end)
end

local Event = class.new("common.block.Computer.Event")

do
	function Event:init(typ)
		self.type = typ
		self.arguments = {}
	end
	
	function Event:setInteger(ind, i)
		self.arguments[ind+1] = {"integer", i}
	end
	
	function Event:setString(ind, s)
		self.arguments[ind+1] = {"string", s}
	end
	
	function Event:setFloat(ind, f)
		self.arguments[ind+1] = {"float", f}
	end
end

local TEST_BIN = resource.get("global:lvx/seat_control.bin")

function Computer:init()
	super.init(self)
	
	self.meshes = MESHES
	self.dynamic = true
	self.on = false
	self.componentType = "computer"
	self.sleep = {on=false, start=0, length=0, interrupts=false}
	self.eventBuffer = {}
	self.event = nil
end

local STRING_LIMIT = 1024

local function readZString(I, addr)
	if addr == 0 then return end
	
	local buffer = {}
	while true do
		local int = I.memory:read(addr)
			
		local d, c, b, a = bit.band(int,0xFF),
							bit.band(bit.rshift(int, 8),0xFF),
							bit.band(bit.rshift(int,16),0xFF),
							bit.band(bit.rshift(int,24),0xFF)
		
		if a == 0 then
			break
		else
			buffer[#buffer+1] = string.char(a)
		end
		
		if b == 0 then
			break
		else
			buffer[#buffer+1] = string.char(b)
		end
		
		if c == 0 then
			break
		else
			buffer[#buffer+1] = string.char(c)
		end
		
		if d == 0 then
			break
		else
			buffer[#buffer+1] = string.char(d)
		end
		
		addr = addr+4
		
		if #buffer >= STRING_LIMIT then
			break
		end
	end
	return table.concat(buffer)
end

local function writeString(I, dest, s)
	local name = s..("\0"):rep(4-(#s%4))
	for i=0, #name-1, 4 do
		local d, c, b, a = name:byte(i+1,i+5)
		local int = bit.lshift(d, 24)+bit.lshift(c, 16)+bit.lshift(b, 8)+a
		I.memory:write(dest+i, int)
	end
	return #name
end

local function writeBytes(I, dest, s)
	local i = 0
	while i < #s do
		local left = #s-i
		
		if left >= 4 then
			local d, c, b, a = s:byte(i+1,i+5)
			local int = bit.lshift(d, 24)+bit.lshift(c, 16)+bit.lshift(b, 8)+a
			I.memory:write(dest+i, int)
			i = i+4
		elseif left >= 2 then
			local b, a = s:byte(i+1,i+3)
			local short = bit.lshift(b, 8)+a
			I.memory:writeShort(dest+i, short)
			i = i+2
		elseif left == 1 then
			I.memory:writeChar(dest+i, s:byte(i+1))
			i = i+1
		end
	end
	return #s
end

function Computer:HWI(hwi)
	print("HWI", hwi)
	
	if hwi == 0 then --void shutdown(int restart);
		--TODO: Restart support
		self.on = false
		self.lvx = nil
		self.structure:requestSync(self)
	elseif hwi == 1 and DEBUGGING then
		print(self.lvx:getGPR(0))
	elseif hwi == 2 and DEBUGGING then
		print(self.lvx:getFPR(0))
	elseif hwi == 3 and DEBUGGING then
		print(self.lvx:getGPR(0))
		print(readZString(self.lvx, self.lvx:getGPR(0)))
	elseif hwi == 4 and DEBUGGING then
		io.write(string.char(self.lvx:getGPR(0)))
	elseif hwi == 5 and DEBUGGING then
		print("GPRs:")
		for i=0, 15 do
			print(string.format("R%2.01d: %08X", i, I:getGPR(i)))
		end
	elseif hwi == 6 then --int componentCount(char *type);
		local typ = readZString(self.lvx, self.lvx:getGPR(0))
		
		print("componentCount", typ or "\"all\"")
		local cnt = 0
		local cmp = typ and self.structure.components.byType[typ] or self.structure.components.byName
		if cmp then
			for _, _ in pairs(cmp) do cnt = cnt+1 end
		end
		self.lvx:setGPR(0, cnt)
	elseif hwi == 7 then --int componentList(char *dest, int max, char *type);
		local dest = self.lvx:getGPR(0)
		local max = self.lvx:getGPR(1)
		local typ = readZString(self.lvx, self.lvx:getGPR(2))
		
		print("componentList", dest, max, typ or "\"all\"")
		
		local cnt = 0
		local cmp = typ and self.structure.components.byType[typ] or self.structure.components.byName
		if cmp then
			for _, v in pairs(cmp) do
				cnt = cnt+1
				
				if cnt > max then cnt = cnt-1 break end
				
				print(v.componentName, v.componentType, dest)
				
				local name = v.componentName:sub(1, 16).."\0\0\0\0"
				for i=0, #name-1, 4 do
					local d, c, b, a = name:byte(i+1,i+5)
					local int = bit.lshift(d, 24)+bit.lshift(c, 16)+bit.lshift(b, 8)+a
					self.lvx.memory:write(dest+i, int)
				end
				dest = dest+#name
			end
		end
		self.lvx:setGPR(0, cnt)
	elseif hwi == 8 then --int componentInvoke(char *name, int operation, int operandA, int operandB);
		local name = readZString(self.lvx, self.lvx:getGPR(0))
		local operation = self.lvx:getGPR(1)
		local operandA = self.lvx:getGPR(2)
		local operandB = self.lvx:getGPR(3)
		
		print("componentInvoke", name, self.lvx:getGPR(0), operation, operandA, operandB)
		
		local cmp = self.structure.components.byName[name]
		
		if cmp then
			local int = cmp:invoke(self, operation, operandA, operandB)
			self.lvx:setGPR(0, int or 0)
		else
			self.lvx:setGPR(0, -1)
		end
	elseif hwi == 9 then --int sleep(int msec, int noInterrupt);
		self.sleep.on = true
		self.sleep.start = love.timer.getTime()
		self.sleep.length = self.lvx:getGPR(0)/1000
		self.sleep.interrupt = self.lvx:getGPR(1) == 0
		
		print("sleep", self.sleep.length*1000, not self.sleep.interrupt)
	elseif hwi == 10 then --int componentNext(char *dest, char *prev, char *type);
		--basically, we run next() on the component table that was chosen
		--The stuff gets written into dest
		local dest = self.lvx:getGPR(0)
		local prev = readZString(self.lvx, self.lvx:getGPR(1))
		local typ = readZString(self.lvx, self.lvx:getGPR(2))
		
		print("componentNext", dest, prev, typ)
		
		local cmp = typ and self.structure.components.byType[typ] or self.structure.components.byName
		if cmp then
			local n = next(cmp, prev)
			
			if not n then
				self.lvx:setGPR(0, 0)
				return
			end
			
			local name = n:sub(1, 16).."\0\0\0\0"
			for i=0, #name-1, 4 do
				local d, c, b, a = name:byte(i+1,i+5)
				local int = bit.lshift(d, 24)+bit.lshift(c, 16)+bit.lshift(b, 8)+a
				self.lvx.memory:write(dest+i, int)
			end
			self.lvx:setGPR(0, #name)
		else
			self.lvx:setGPR(0, 0)
		end
	elseif hwi == 11 then --int componentType(char *dest, char *name);
		--Returns the number of bytes written to name
		local dest = self.lvx:getGPR(0)
		local name = readZString(self.lvx, self.lvx:getGPR(1))
		
		print("componentType", dest, name)
		
		local cmp = self.structure.components.byName[name]
		
		if cmp then
			self.lvx:setGPR(0, writeString(self.lvx, dest, cmp.componentType))
		else
			self.lvx:setGPR(0, 0)
		end
	elseif hwi == 12 then --int eventAvailable();
		--Returns the number of events waiting (1 or more means an event is currently in the buffer)
		if self.event then
			self.lvx:setGPR(0, 1+#self.eventBuffer)
		else
			self.lvx:setGPR(0, 0)
		end
	elseif hwi == 13 then --int eventType(char *dest);
		--Returns the number of bytes written to dest, or 0 if the event doesn't exist
		--The type is culled to 60 bytes (the last 4 is used as zero termination), so you can allocate a buffer of 64
			--bytes for this
		if self.event then
			self.lvx:setGPR(0, writeString(self.lvx, self.lvx:getGPR(0), self.event.type))
		else
			self.lvx:setGPR(0, 0)
		end
	elseif hwi == 14 then --int eventArgumentCount();
		--Returns the number of arguments in the event or 0
		if self.event then
			self.lvx:setGPR(0, #self.event.arguments)
		else
			self.lvx:setGPR(0, 0)
		end
	elseif hwi == 15 then --int eventArgumentType(int index);
		--Returns the type (0=int, 1=string, 2=float) of the argument
		if self.event then
			local arg = self.event.arguments[self.lvx:getGPR(0)+1]
			if arg then
				local t = 0
				if arg[1] == "string" then
					t = 1
				elseif arg[1] == "float" then
					t = 2
				end
				
				self.lvx:setGPR(0, t)
			else
				self.lvx:setGPR(0, -1)
			end
		else
			self.lvx:setGPR(0, -1)
		end
	elseif hwi == 16 then --int eventArgumentGetInt(int index);
		--Returns the integer argument
		if self.event then
			local arg = self.event.arguments[self.lvx:getGPR(0)+1]
			if arg and arg[1] == "integer" then
				self.lvx:setGPR(0, arg[2])
			else
				self.lvx:setGPR(0, -1)
			end
		else
			self.lvx:setGPR(0, -1)
		end
	elseif hwi == 17 then --int eventArgumentGetString(int index, char *dest, int bufferLength);
		--Writes the string into dest, returns the length of the string
		--If bufferLength is 0 then the entire string is written, else, up to bufferLength is written
		if self.event then
			local arg = self.event.arguments[self.lvx:getGPR(0)+1]
			if arg and arg[1] == "string" then
				local s = arg[2]
				local l = self.lvx:getGPR(2)
				if l > 0 and #s > l then
					s = s:sub(1,l)
				end
				
				self.lvx:setGPR(0, writeBytes(self.lvx, self.lvx:getGPR(1), s))
			else
				self.lvx:setGPR(0, 0)
			end
		else
			self.lvx:setGPR(0, 0)
		end
	elseif hwi == 18 then --int eventArgumentGetStringLength(int index);
		--Returns the length of the argument string
		if self.event then
			local arg = self.event.arguments[self.lvx:getGPR(0)+1]
			if arg and arg[1] == "string" then
				self.lvx:setGPR(0, #arg[2])
			else
				self.lvx:setGPR(0, 0)
			end
		else
			self.lvx:setGPR(0, 0)
		end
	elseif hwi == 19 then --float eventArgumentGetFloat(int index);
		--Returns the float argument
		if self.event then
			local arg = self.event.arguments[self.lvx:getGPR(0)+1]
			if arg and arg[1] == "float" then
				self.lvx:setFPR(0, arg[2])
			else
				self.lvx:setFPR(0, 0)
			end
		else
			self.lvx:setFPR(0, 0)
		end
	elseif hwi == 20 then --int eventDismiss();
		--Dismisses the latest event, returns 1 if a new event is available
		self.event = nil
		if self.eventBuffer[1] then
			self.event = table.remove(self.eventBuffer, 1)
		end
		self.lvx:setGPR(0, self.event and 1 or 0)
	end
	--Example:
	--[[
	if (eventAvailable()) {
		char name[64];
		eventType(&name);
		
		if (strcmp(name, "component_added")) {
			puts("Component Added: ");
			char component[20];
			int length = eventArgumentGetString(0, &component, 19); //Length is off by one for zero termination
			component[length] = 0; //zero termination
			puts(component);
			puts("\n");
		} else if (strcmp(name, "component_removed")) {
			puts("Component Removed: ");
			char component[20];
			int length = eventArgumentGetString(0, &component, 19); //Length is off by one for zero termination
			component[length] = 0; //zero termination
			puts(component);
			puts("\n");
		} else {
			puts("Unhandled Event: ");
			puts(name);
			puts("\n");
		}
	}
	]]
end

function Computer:configure(player, x, y)
	self.on = not self.on
	print("Computer on: ", self.on)
	
	self.structure:requestSync(self)
	
	if self.on then
		self.lvx = nersis.lvx.Interpreter()
		
		self.lvx:setHWICallback(function(hwi)
			self:HWI(hwi)
		end)
		
		local mem = self.lvx.memory
		print(mem)
	
		local page = nersis.lvx.MemoryPage(0, 0x10000)
		self.mainPage = page --Need to keep reference to page so it doesn't get gc'd
	
		for i=0, #TEST_BIN-1, 4 do
			local d, c, b, a = TEST_BIN:byte(i+1,i+5)
			local int = bit.lshift(d, 24)+bit.lshift(c, 16)+bit.lshift(b, 8)+a
			page:write(mem, i, int)
		end
	
		mem:addPage(page)
	
		self.lvx:setGPR(15, 0)
	else
		self.lvx = nil
		--[[self.on = true
		print("JK lol")
		local ev = Event:new("test_event")
		ev:setString(0, "Hello, World!")
		self:pushEvent(ev)]]
	end
end

local EVENT_BUFFER_LIMIT = 32 -- Maximum 32 events in the buffer, if 32 or more are in the buffer, new events are dropped

function Computer:pushEvent(event)
	if self.sleep.on and self.sleep.interrupt then
		self.sleep.on = false --interrupt sleep even if the buffer is full
	end
	
	if #self.eventBuffer > EVENT_BUFFER_LIMIT then
		return false
	end
	
	if self.event then
		self.eventBuffer[#self.eventBuffer+1] = event
	else
		self.event = event
	end
	
	return true
end

function Computer:update(dt)
	if self.on then
		if CLIENT then
			--[[if self.meshes ~= MESHES_ACTIVE then
				self.meshes = MESHES_ACTIVE
				self.structure.rerender = true
			end]]
		else
			if self.sleep.on then
				local t = love.timer.getTime()
				if self.sleep.start+self.sleep.length <= t then
					local l = t-self.sleep.start
					self.lvx:setGPR(0, math.floor(l*1000))
					print(l*1000)
					self.sleep.on = false
				end
			else
				for i=1, 128 do
					self.lvx:step()
					if self.sleep.on then break end
				end
			end
		end
	end
end

--Serialize the block for network transfer or disk saving
function Computer:serialize(buffer)
	super.serialize(self, buffer)
	buffer:writeByte(self.on and 1 or 0)
end

--Unserialize the block from a network transfer or from disk
function Computer:unserialize(buffer)
	super.unserialize(self, buffer)
	self.on = buffer:readByte() ~= 0
end
