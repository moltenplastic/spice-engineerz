local Controller, super = class.new("common.block.Controller", "common.block.Component")

function Controller:init()
	super.init(self)
	
	self.thrusters = {}
	self.centerOfMass = {x = 0, y = 0}
	self.targetVX = 0
	self.targetVY = 0
	self.targetAV = 0
	self.active = true
	
	self.dynamic = true
	self.componentType = "controller"
end

function Controller:onStructureAdded()
	self.thrusters = {}
	local thrusters = self.structure.components.byType.thruster
	if thrusters then
		for name, thruster in pairs(thrusters) do
			self.thrusters[#self.thrusters+1] = thruster
		end
	end
end

function Controller:onComponentsAdded(added)
	for i=1, #added do
		local component = added[i]
		if component.componentType == "thruster" then
			self.thrusters[#self.thrusters+1] = component
		end
	end
end

function Controller:onComponentsRemoved(removed)
	for i=1, #removed do
		local component = removed[i]
		if component.componentType == "thruster" then
			for i=1, #self.thrusters do
				if self.thrusters[i] == component then
					table.remove(self.thrusters, i)
					break
				end
			end
		end
	end
end

local CONTROLLER_SET_TARGET_VELOCITY = 0
local CONTROLLER_GET_TARGET_VELOCITY_X = 1
local CONTROLLER_GET_TARGET_VELOCITY_Y = 2
local CONTROLLER_SET_TARGET_ANGULAR_VELOCITY = 3
local CONTROLLER_GET_TARGET_ANGULAR_VELOCITY =  4
local CONTROLLER_SET_ACTIVE = 5
local CONTROLLER_GET_ACTIVE = 6

function Controller:invoke(computer, operation, operandA, operandB)
	if operation == CONTROLLER_SET_TARGET_VELOCITY then
		self.targetVX = nersis.lvx.toFloat(operandA)
		self.targetVY = nersis.lvx.toFloat(operandB)
		print("controller.setTargetVelocity("..self.targetVX..", "..self.targetVY..")")
		return 1
	elseif operation == CONTROLLER_GET_TARGET_VELOCITY_X then
		print("controller.getTargetVelocityX() -> "..self.targetVX)
		return nersis.lvx.fromFloat(self.targetVX)
	elseif operation == CONTROLLER_GET_TARGET_VELOCITY_Y then
		print("controller.getTargetVelocityY() -> "..self.targetVY)
		return nersis.lvx.fromFloat(self.targetVY)
	elseif operation == CONTROLLER_SET_TARGET_ANGULAR_VELOCITY then
		self.targetAV = nersis.lvx.toFloat(operandA)
		print("controller.setTargetAngularVelocity("..self.targetAV..")")
		return 1
	elseif operation == CONTROLLER_GET_TARGET_ANGULAR_VELOCITY then
		print("controller.getTargetAngularVelocity() -> "..self.targetAV)
		return nersis.lvx.fromFloat(self.targetAV)
	elseif operation == CONTROLLER_SET_ACTIVE then
		self.active = operandA ~= 0
		print("controller.setActive("..self.active..")")
		return 1
	elseif operation == CONTROLLER_GET_ACTIVE then
		print("controller.getActive() -> "..self.active)
		return self.active and 1 or 0
	else
		return -1
	end
end

local AV_THRES = 0.0075

--TODO: Find better resting values (Use thruster outputs to find the smallest possible change we can apply)

function Controller:go(dt, targetVX, targetVY, targetAV, unboundedLinearVelocity)
	local centerX, centerY, mass, inertia = self.structure.body:getMassData()
	local angularVelocity = self.structure.body:getAngularVelocity()
	local linearVelX, linearVelY = self.structure.body:getLinearVelocityFromLocalPoint(centerX, centerY)
	local linearAccelX, linearAccelY = linearVelX/dt, linearVelY/dt
	
	local worldCenterX, worldCenterY = self.structure.body:getWorldPoint(centerX, centerY)
	
	--TODO: Test the validity of my math, I don't know how much of this is correct.
	
	--[[
	Time to get out the Physics Book.
	
	Knows:
	v0 = 5 m/s
	v1 = 0 m/s
	t = 1/60 s (timestep)
	m = 1 kg
	F(thruster) = 2N
	
	Wants:
	a = ? m/s^2
	
	v1 = v0+a*t
	  -v0      -v0
	v1-v0 = at
	  /t      /t
	(v1-v0)/t = a
	
	(0-5)/(1/60) = -0.08333 m/s^2 = a
	EF = ma = 1*-0.08333 = -0.08333 N
	
	However, thrusters apply 2N of force, so it would begin going in the opposite direction at 1.91667N
	This would get solved in another timestep...
	]]
	
	--[[
	Rotations and Forces:
	
	After deciding what initial thrusters to use that will make us go in the right target velocity, we must adjust
	and select thrusters depending on potential rotational velocity+current rotational velocity vs the target
	rotational velocity.
	
	To find the amount of rotational velocity added from an offbalanced force:
	
	angularAcceleration = (inertia^-1) * distance.cross(force)
	
	Example:
	
	Knows:
	inertia = 1
	distance = [1, 0]
	force = [0, 80]
	
	angularAcceleration = (inertia^-1) * distance.cross(force)
						= distance.cross(force)
						= (distance.x*force.y)-(distance.y*force.x)
						= 80-0
						= 80
	]]
	
	local computedAX, computedAY = (targetVX)/dt, (targetVY)/dt
	local deltaAX, deltaAY = computedAX-linearAccelX, computedAY-linearAccelY
	local forceX, forceY = mass*deltaAX, mass*deltaAY
	
	if unboundedLinearVelocity then
		if math.abs(linearAccelX) > math.abs(computedAX) and ((linearAccelX < 0 and computedAX < 0) or (linearAccelX > 0 and computedAX > 0)) then
			forceX = 0
		end
		
		if math.abs(linearAccelY) > math.abs(computedAY) and ((linearAccelY < 0 and computedAY < 0) or (linearAccelY > 0 and computedAY > 0)) then
			forceY = 0
		end
	end
	
	local forceMagnitude = ((forceX^2)+(forceY^2))^0.5
	
	--print("Target force: ", forceX, forceY, forceMagnitude, mass*linearAccelX, mass*linearAccelY)
	--self.structure.body:applyForce(forceX, forceY)
	
	angularVelocity = targetAV-angularVelocity
	local m_I = inertia - (mass * ((centerX*centerX)+(centerY*centerY))) -- exact reverse of what box2d does to 
																		 -- set angular velocity from torque
	local torque = ((angularVelocity)*(m_I))/dt
	
	--print("Target torque: ", torque, angularVelocity)
	
	--print("CI", forceX, forceY, forceMagnitude, torque)
	
	--give every thruster a score on how well it would help reach the goal
	local torqueFromThrusters = 0
	if forceMagnitude >= 20 or math.abs(torque) > 3000 then
		local angle = self.structure.body:getAngle()
		local continue = true
		
		local scored = {}
		while continue do
			local scores = {}
			
			local meanForce = 0
			local meanTorque = 0
			
			for i, thruster in ipairs(self.thrusters) do
				if not scored[thruster] then
					local score = {thruster=thruster}
					scores[#scores+1] = score
					
					local direction = thruster.rotation*(math.pi/2)
					local thrusterAngle = direction+angle
					local mfx = math.cos(thrusterAngle)*20
					local mfy = math.sin(thrusterAngle)*20
					
					local forceDiffX = forceX-mfx
					local forceDiffY = forceY-mfy
					
					local applyX, applyY = self.structure.body:getWorldPoint((thruster.x*16)+8, (thruster.y*16)+8)
					local tax, tay = applyX-worldCenterX, applyY-worldCenterY
					
					local mt = ((tax*mfy)-(tay*mfx))
					local torqueDiff = torque-mt
					
					score.mfx = mfx
					score.mfy = mfy
					score.mt = mt
					
					score.force = forceMagnitude-(((forceDiffX^2)+(forceDiffY^2))^0.5)
					score.torque = math.abs(torque)-math.abs(torque-mt)
					score.total = (score.force+score.torque)/2
					
					meanForce = meanForce+score.force
					meanTorque = meanTorque+score.torque
					
					--print("Force Diff for thruster", thruster.componentName, score.force, score.torque, score.total)
				end
			end
			
			meanForce = meanForce/#scores
			meanTorque = meanTorque/#scores
			
			table.sort(scores, function(a, b)
				return a.total > b.total
			end)
			
			continue = false
			
			for i=1, #scores do
				local score = scores[i]
				
				local on
				
				if forceMagnitude >= 20 then
					on = score.force > 0 and score.force > meanForce
				elseif math.abs(torque) > 3000 then
					on = score.torque > 0 and score.torque > meanTorque
				end
				
				--print(i, score.thruster.componentName, score.force, score.torque, on)
				
				score.thruster:set(on, true)
				if on then
					scored[score.thruster] = true
					
					forceX = forceX-score.mfx
					forceY = forceY-score.mfy
					
					torque = torque-score.mt
					
					--[[if unboundedLinearVelocity then
						local caxdiff = -score.mfx*mass
						computedAX = computedAX+caxdiff
						if math.abs(linearAccelX) > math.abs(computedAX) and ((linearAccelX < 0 and computedAX < 0) or (linearAccelX > 0 and computedAX > 0)) then
							forceX = 0
						end
						
						local caydiff = -score.mfy*mass
						computedAY = computedAY+caydiff
						if math.abs(linearAccelY) > math.abs(computedAY) and ((linearAccelY < 0 and computedAY < 0) or (linearAccelY > 0 and computedAY > 0)) then
							forceY = 0
						end
					end]]
					
					forceMagnitude = ((forceX^2)+(forceY^2))^0.5
					
					continue = forceMagnitude >= 20 or math.abs(torque) > 3000
					
					if continue then
						--print(forceX, forceY, forceMagnitude, torque, score.mt, angularVelocity)
					end
					
					break
				end
			end
		end
	else
		for _, thruster in ipairs(self.thrusters) do
			thruster:set(false, true)
		end
	end
end

function Controller:update(dt)
	if self.active then
		self:go(dt, self.targetVX, self.targetVY, self.targetAV, true)
	end
end
