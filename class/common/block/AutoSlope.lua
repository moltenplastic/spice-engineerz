local AutoSlope, super = class.new("common.block.AutoSlope", "common.block.Base")

local MIN, MID, MAX = 0, 0.5, 1

function AutoSlope:init()
	super.init(self)
	self.meshes = nil -- So render will get called
end

-- TODO: Override AutoSlope:getPolygon
function AutoSlope:getPolygon()
	local function check(x)
		return x and x.opaque
	end

	local left, right, up, down = check(self.left), check(self.right), check(self.up), check(self.down)

	if left and right and up and down then
		return
	end

	if (left and right) or (up and down) then
		return super.getPolygon(self)
	end

	if up then
		if left then
			return {
				{0,0,
				1,0,
				0,1}
			}
		elseif right then
			return {
				{0,0,
				1,0,
				1,1}
			}
		end
	elseif down then
		if left then
			return {
				{0,0,
				1,1,
				0,1}
			}
		elseif right then
			return {
				{0,1,
				1,0,
				1,1}
			}
		end
	end

	return super.getPolygon(self)
end

function AutoSlope:render(lod,vb,layer)
	if layer == "default" then
		local function check(x)
			return x and x.opaque
		end

		local left, right, up, down = check(self.left), check(self.right), check(self.up), check(self.down)

		if (left and right) or (up and down) then
			-- render full block and return
			vb:add({0,0},{1,0},{1,1})
			vb:add({1,1},{0,1},{0,0})
			return
		end

		if up then
			if left then
				vb:add({0,0},{1,0},{0,1})
				return
			elseif right then
				vb:add({0,0},{1,0},{1,1})
				return
			end
		elseif down then
			if left then
				vb:add({0,0},{1,1},{0,1})
				return
			elseif right then
				vb:add({0,1},{1,0},{1,1})
				return
			end
		end

		-- Render default
		vb:add({0,0},{1,0},{1,1})
		vb:add({1,1},{0,1},{0,0})
	end
end
