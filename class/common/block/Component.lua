local Component, super = class.new("common.block.Component", "common.block.Base")

function Component:init()
	super.init(self)
	
	self.componentType = "generic"
	self.componentName = uuid.generate()
	self.componentGlobal = true --if true, it shows up in the structure wide
end

function Component:invoke(computer, operation, operandA, operandB)
	print(computer.componentName, operation, operandA, operandB)
end

function Component:onComponentsAdded(components)
	
end

function Component:onComponentsRemoved(components)
	
end

--Serialize the block for network transfer or disk saving
function Component:serialize(buffer)
	super.serialize(self, buffer)
	buffer:writeString(self.componentName)
end

--Unserialize the block from a network transfer or from disk
function Component:unserialize(buffer)
	super.unserialize(self, buffer)
	self.componentName = buffer:readString()
end
