local love_graphics = love.graphics
local love_physics = love.physics

local Missile, super = class.new("common.entity.Missile", "common.entity.Gravity")

local MODEL_NORMAL, MODEL_ACTIVE

if CLIENT then
	resource.getAsync("global:model/missile.mdf", function(lods)
		MODEL_NORMAL = mdf.toVertexBuffers(lods, function(lod, vb)
			vb:translate(-0.5, -0.5)
			vb:scale(16,16)
		end)
	end)
	
	resource.getAsync("global:model/missile_active.mdf", function(lods)
		MODEL_ACTIVE = mdf.toVertexBuffers(lods, function(lod, vb)
			vb:translate(-0.5, -0.5)
			vb:scale(16,16)
		end)
	end)
end

function Missile:initPhysics(base)
	self.body:setType("dynamic")
	self.body:setLinearDamping(0.5)
	self.body:setAngularDamping(2.5)
	--self.body:setBullet(true)
	self:addShape(love.physics.newRectangleShape(16,2.56))
	self.fuel = 5
	self.ignition = 0.5
end

local sensitivity=400

function Missile:onCollision(a)
	if not self.explosion then
		local vx,vy=a.body:getLinearVelocity()
		local cx,cy=self.body:getLinearVelocity()
		if math.abs(cx-vx)+math.abs(cx-vy)>sensitivity then
			local ang=math.atan2(a.body:getX()-self.body:getX(),a.body:getY()-self.body:getY())+(math.pi/2)
			a.body:applyLinearImpulse(math.sin(ang)*100,math.cos(ang)*100)
			self.explosion=0
		end
	end
end

function Missile:update(dt)
	if self.explosion then
		self.explosion=self.explosion+dt
	end
	if self.ignition > 0 then
		self.ignition = self.ignition - dt
	elseif self.fuel > 0 and not self.explosion then
		self.fuel = self.fuel - dt
		local angle = self.body:getAngle()
		self.body:applyLinearImpulse(math.cos(angle)*1,math.sin(angle)*1)
	elseif ((self.explosion and self.explosion>1) or self.fuel <= 0) and SERVER then
		self.context:despawn(self)
	end
end

function Missile:draw(lod)
	love_graphics.push()
	love_graphics.translate(self.body:getX(), self.body:getY())
	love_graphics.rotate(self.body:getAngle())
	
	if self.explosion then
		love_graphics.setColor(255,128,0,math.max(0,255-(self.explosion*512)))
		love_graphics.circle("fill",0,0,(self.explosion*50)+20)
		love_graphics.setColor(255,self.explosion*128,0,math.max(0,255-(self.explosion*255)))
		love_graphics.circle("fill",0,0,self.explosion*100)
	else
		love_graphics.setColor(255,255,255)
		local lodm = mdf.selectLOD((self.fuel > 0 and self.ignition < 0) and MODEL_ACTIVE or MODEL_NORMAL, lod)
		if lodm then lodm:draw() end
	end

	love_graphics.pop()
end


