local Gravity, super = class.new("common.entity.Gravity", "common.entity.Base")

function Gravity:init(context, ...)
	super.init(self, context, ...)
	
	self.gravityX = 0
	self.gravityY = 0
	self.x1,self.y1,self.x2,self.y2,self.hx,self.hy = 0,0,0,0,0,0
	self.normalInvalid = true
end

function Gravity:update(dt)
	super.update(self, dt)
	
	--Left over code from another thing I did... Can't use this code, this code does Super Mario Galaxy esque gravity
	
	--[[if self.normalInvalid then
		local centerdist, planet = self.context:getNearest("common.entity.Planet", self.body:getX(), self.body:getY())
		local fix,ourfix,dist,x1,y1,x2,y2 = self:closestFixture(planet)
		local dir = (dist == 0 and math.pi or math.pi*2)-math.atan2(x2-x1,y2-y1)
		x1 = x1+math.sin(dir)*8
		y1 = y1-math.cos(dir)*8
		x2 = x2-math.sin(dir)*8
		y2 = y2+math.cos(dir)*8
		local frac = math.huge
		self.context.world:rayCast(x2, y2, x1, y1, function(fixture, x, y, xnn, ynn, fraction)
			if planet:ownsFixture(fixture) and fraction < frac then
				frac = fraction
				self.gravityX = -xnn
				self.gravityY = -ynn
				self.hx = x
				self.hy = y
			end
			return -1
		end)
		self.x1,self.y1,self.x2,self.y2 = x1,y1,x2,y2
		self.normalInvalid = false
		self.planet = planet
	else
		--raycast in the direction of the normal--
		local x1, y1 = self.body:getPosition()
		local x2, y2 = x1+self.gravityX*self.max*1.5, y1+self.gravityY*self.max*1.5
		local hit = false
		local frac = math.huge
		local newxn, newyn
		
		if x1 == x2 and y1 == y2 then return end
		self.context.world:rayCast(x1, y1, x2, y2, function(fixture, x, y, xnn, ynn, fraction)
			if self.planet:ownsFixture(fixture) and fraction < frac then
				frac = fraction
				newxn = -xnn
				newyn = -ynn
				self.hx = x
				self.hy = y
				hit = true
			end
			return -1
		end)
		
		if not hit then
			self.normalInvalid = true
		else
			self.gravityX = (self.gravityX*2+newxn)/3
			self.gravityY = (self.gravityY*2+newyn)/3
		end
		self.x1,self.y1,self.x2,self.y2 = x1,y1,x2,y2
	end
	
	local dist = (self.planet.max*3)-(((self.body:getX()-self.planet.body:getX())^2+(self.body:getY()-self.planet.body:getY())^2)^0.5)
	if dist > 0 then
		dist = 1/(dist/(self.planet.max*3))
		self.body:applyForce(self.gravityX*1024*dist,self.gravityY*1024*dist)
	end]]
end

function Gravity:debugDraw()
	love.graphics.setColor(255,0,0)
	love.graphics.circle("fill",self.x1,self.y1,4)
	love.graphics.setColor(0,255,0)
	love.graphics.circle("fill",self.x2,self.y2,4)
	love.graphics.setColor(0,255,255)
	love.graphics.circle("fill",self.hx,self.hy,4)
	love.graphics.setColor(0,0,255)
	love.graphics.line(self.x1,self.y1,self.x2,self.y2)
	love.graphics.setColor(255,0,255)
	love.graphics.line(self.x1,self.y1,self.x1+(self.gravityX*16),self.y1+(self.gravityY*16))
end
