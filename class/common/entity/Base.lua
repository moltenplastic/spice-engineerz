local Base = class.new "common.entity.Base"

function Base:init(context, ...)
	self.context = context
	
	self.body = love.physics.newBody(self.context.world, 0, 0)
	self.body:setUserData(self)
	self.shapes = {}
	self.fixtures = {}
	
	self.runlist = {}
	
	--TODO: Hook this up sometime?
	self.syncFrequency = 1/30 --30fps
	self.ttNextSync = 0
	self.needsSync = true
	
	self.id = uuid.generate()
	self.valid = true
	self.extra = {} --extra nonserializable data (example: when sitting in a seat put a table here with seating data)
	
	self:initPhysics(...)
end

--This is called to initialize the physics of the objects
--Extra arguments are passed to this function from :init()
function Base:initPhysics()
	
end

function Base:destroy()
	self:removeAllShapes()
	self.body:destroy()
	self.body = nil
	self.valid = false
end

--Adds a shape and a fixture to the current Box2D body
function Base:addShape(shape)
	local fixture = love.physics.newFixture(self.body, shape)
	self.fixtures[#self.fixtures+1] = fixture
	self.fixtures[fixture] = true
	self.shapes[#self.shapes+1] = shape
	return fixture
end

--Removes all shapes from the Box2D body
function Base:removeAllShapes()
	for i, v in pairs(self.fixtures) do
		if type(v) == "userdata" then
			self.fixtures[v] = nil
			self.fixtures[i] = nil
			v:destroy()
		end
	end
	
	for i, v in pairs(self.shapes) do
		self.shapes[i] = nil
	end
	
	self.width = nil --invalidate bounding box
end

--Finds the closest fixture to another fixture
function Base:closestFixture(other)
	local fix,dist,x1,y1,x2,y2 = nil, math.huge
	local ourfix
	for _, myfix in ipairs(self.fixtures) do
		for _, theirfix in ipairs(other.fixtures) do
			local _dist,_x1,_y1,_x2,_y2 = love.physics.getDistance(theirfix,myfix)
			if _dist < dist then
				dist,x1,y1,x2,y2 = _dist,_x1,_y1,_x2,_y2
				fix = theirfix
				ourfix = myfix
			end
		end
	end
	
	return fix,ourfix,dist,x1,y1,x2,y2
end

--Returns whether the passed fixture is owned by this base or not
function Base:ownsFixture(fixture)
	return self.fixtures[fixture]
end

--Adds a function to the runlist that is executed in :update(dt)
function Base:runLater(func, ...)
	self.runlist[#self.runlist+1] = {func, ...}
end

--Updates the object, it computes the bounding box and checks if a sync is needed
function Base:update(dt)
	if not self.width then
		local x1,y1,x2,y2 = math.huge,math.huge,-math.huge,-math.huge
		
		for _, shape in ipairs(self.shapes) do
			local bbx1,bby1,bbx2,bby2 = shape:computeAABB(0,0,math.pi/2)
			
			x1 = math.min(x1,bbx1)
			y1 = math.min(y1,bby1)
			x2 = math.max(x2,bbx2)
			y2 = math.max(y2,bby2)
		end
		
		self.width = x2-x1
		self.height = y2-y1
		self.max = math.max(self.width,self.height)
		self.bbx1 = x1
		self.bby1 = y1
		self.bbx2 = x2
		self.bby2 = y2
		self.cx = (x1+x2)/2
		self.cy = (y1+y2)/2
	end
	
	if self.runlist[1] then
		local entry = self.runlist[1]
		entry[1](unpack(entry,2))
		table.remove(entry, 1)
	end
	
	self.ttNextSync = self.ttNextSync+dt
	
	if self.ttNextSync > self.syncFrequency then
		self.needsSync = true
	end
end

function Base:onCollision(other, contact, myFixture, otherFixture)
	
end

if CLIENT then
	--Draws the object
	--CLIENT SIDE ONLY
	function Base:draw()
	
	end
end

--Gets the general bounding box of the entity's current rotation
function Base:getBoundingBox()
	local x1,y1,x2,y2 = math.huge,math.huge,-math.huge,-math.huge
		
	for _, fixture in ipairs(self.fixtures) do
		local bbx1,bby1,bbx2,bby2 = fixture:getBoundingBox()
		
		x1 = math.min(x1,bbx1)
		y1 = math.min(y1,bby1)
		x2 = math.max(x2,bbx2)
		y2 = math.max(y2,bby2)
	end
	
	return x1,y1,x2,y2
end

--Objects that output optimized geometry (Structure) should override this.
function Base:testPoint(x, y)
	for _, fixture in ipairs(self.fixtures) do
		if fixture:testPoint(x,y) then
			return true
		end
	end
end

--Processes a packet from the Client or Server, developers should use the CLIENT environmental variable to figure out where
function Base:processPacket(source, buffer)
	--On the client, source will be nil, but on the server, source will be an enet peer object
end

--Writes sync data to be serialized into a sync packet
function Base:writeSyncData(buffer)
end

--Writes to the saved data file buffer
function Base:writeSaveData(buffer)
	
end

--Creates a sync packet. Please do not override.
function Base:createSyncPacket(buffer, toSave)
	buffer:writeDouble(self.body:getAngle())
	buffer:writeDouble(self.body:getAngularDamping())
	buffer:writeDouble(self.body:getAngularVelocity())
	buffer:writeDouble(self.body:getLinearDamping())
	
	local linvelx, linvely = self.body:getLinearVelocity()
	buffer:writeDouble(linvelx)
	buffer:writeDouble(linvely)
	
	local cx, cy, mass, inertia = self.body:getMassData()
	buffer:writeDouble(cx)
	buffer:writeDouble(cy)
	buffer:writeDouble(mass)
	buffer:writeDouble(inertia)
	
	local x, y = self.body:getPosition()
	buffer:writeDouble(x)
	buffer:writeDouble(y)
	
	buffer:writeString(self.body:getType())
	buffer:writeByte(self.body:isActive() and 1 or 0)
	buffer:writeByte(self.body:isAwake() and 1 or 0)
	buffer:writeByte(self.body:isBullet() and 1 or 0)
	
	buffer:startSection()
	if not toSave then
		self:writeSyncData(buffer)
	else
		self:writeSaveData(buffer)
	end
	buffer:endSection()
end

--Reads sync data serialized in a sync packet
function Base:readSyncData(buffer)

end

--Reads the saved data from the save file buffer
function Base:readSaveData(buffer)
	
end

--Reads a sync packet
function Base:readSyncPacket(buffer, fromSave)
	self.body:setAngle(buffer:readDouble())
	self.body:setAngularDamping(buffer:readDouble())
	self.body:setAngularVelocity(buffer:readDouble())
	self.body:setLinearDamping(buffer:readDouble())
	self.body:setLinearVelocity(buffer:readDouble(),buffer:readDouble())
	self.body:setMassData(buffer:readDouble(),buffer:readDouble(),buffer:readDouble(),buffer:readDouble())
	self.body:setPosition(buffer:readDouble(),buffer:readDouble())
	self.body:setType(buffer:readString())
	self.body:setActive(buffer:readByte() > 0)
	self.body:setAwake(buffer:readByte() > 0)
	self.body:setBullet(buffer:readByte() > 0)

	buffer:startSection()
	if not fromSave then
		self:readSyncData(buffer)
	else
		self:readSaveData(buffer)
	end
	buffer:endSection()
end
