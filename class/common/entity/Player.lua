local Player, super = class.new("common.entity.Player", "common.entity.Gravity")

function Player:initPhysics(username, player)
	self.username = username
	self.player = player

	if self.username then
		self.id = uuid.generateFrom(self.username)
	end

	self.noSave = true --don't save the player entities, they get spawned in their ship (TODO: This)
	self.body:setType "dynamic"
	self.body:setPosition(80,80)
	self.body:setAngularDamping(10)
	self.body:setLinearDamping(2)
	self:addShape(love.physics.newRectangleShape(16, 32))

	self.up = false
	self.down = false
	self.left = false
	self.right = false
	self.rleft = false
	self.rright = false

	self.inventory = {
		class.import "common.item.Block":new(self.context),
		class.import "common.item.Block":new(self.context, "common.block.Thruster"),
		class.import "common.item.Block":new(self.context, "common.block.Computer"),
		class.import "common.item.Block":new(self.context, "common.block.Controller"),
		class.import "common.item.Block":new(self.context, "common.block.Seat"),
		class.import "common.item.Drill":new(self.context),
		class.import "common.item.Missile":new(self.context),
		class.import "common.item.Block":new(self.context, "common.block.AutoSlope"),
	} --TODO: Inventory object
	self.selected = 2

	self.thrusters = {
		{x = -8, y = 0, size = 4, power = 0, setpower = 0, angle = math.pi / 2},
		{x = 0, y = -16, size = 4, power = 0, setpower = 0, angle = math.pi},
		{x = 8, y = 0, size = 4, power = 0, setpower = 0, angle = math.pi * 1.5},
		{x = 0, y = 16, size = 4, power = 0, setpower = 0, angle = 0},

		{x = -8, y = -8, size = 3, power = 0, setpower = 0, angle = math.pi / 2},
		{x = -8, y = 8, size = 3, power = 0, setpower = 0, angle = math.pi / 2},
		{x = 8, y = -8, size = 3, power = 0, setpower = 0, angle = math.pi * 1.5},
		{x = 8, y = 8, size = 3, power = 0, setpower = 0, angle = math.pi * 1.5},
	}

	if CLIENT then
		resource.getAsync("global:sfx/thruster.ogg", function(sound)
			sound:setLooping(true)
			self.thrustersound = sound:clone()
		end, "static")
	end
end

local TYPES = network.constants {
	"SYNC_KEYS",
	"USE_ITEM",
	"SELECT_ITEM",
	"CONFIGURE",
}

function Player:update(dt)
	super.update(self, dt)

	if CLIENT and self.localplayer then
		self.pup,self.pdown = self.up, self.down
		self.pleft,self.pright = self.left, self.right
		self.prleft,self.prright = self.rleft, self.rright

		if love.keyboard.isDown("a") then
			self.left = true
			self.right = false
		elseif love.keyboard.isDown("d") then
			self.left = false
			self.right = true
		else
			self.left = false
			self.right = false
		end

		if love.keyboard.isDown("w") then
			self.up = true
			self.down = false
		elseif love.keyboard.isDown("s") then
			self.up = false
			self.down = true
		else
			self.up = false
			self.down = false
		end

		if love.keyboard.isDown("q") then
			self.rleft = true
			self.rright = false
		elseif love.keyboard.isDown("e") then
			self.rleft = false
			self.rright = true
		else
			self.rleft = false
			self.rright = false
		end

		if self.pup ~= self.up or self.pdown ~= self.down or self.pleft ~= self.left or self.pright ~= self.right or self.prleft ~= self.rleft or self.prright ~= self.rright then
			local buffer = love.string.newWriteBuffer()
			buffer:writeByte(TYPES.SYNC_KEYS)
			buffer:writeByte(self.up and 1 or 0)
			buffer:writeByte(self.down and 1 or 0)
			buffer:writeByte(self.left and 1 or 0)
			buffer:writeByte(self.right and 1 or 0)
			buffer:writeByte(self.rleft and 1 or 0)
			buffer:writeByte(self.rright and 1 or 0)
			self.context:send(network.entityPacket(self, buffer:getString()))
		end
	end

	local angle = self.body:getAngle()
	local x = math.cos(angle)*8
	local y = -math.sin(angle)*8

	for i = 1, #self.thrusters do
		self.thrusters[i].setpower = 0
	end

	if not self.extra.seat then
		if self.left then
			self.body:applyLinearImpulse(-x,y)
			self.thrusters[3].setpower = 10
		elseif self.right then
			self.body:applyLinearImpulse(x,-y)
			self.thrusters[1].setpower = 10
		end

		if self.up then
			self.body:applyLinearImpulse(-y,-x)
			self.thrusters[4].setpower = 10
		elseif self.down then
			self.body:applyLinearImpulse(y,x)
			self.thrusters[2].setpower = 10
		end

		if self.rleft then
			self.body:applyAngularImpulse(-40)
			self.thrusters[6].setpower = 6
			self.thrusters[7].setpower = 6
		elseif self.rright then
			self.body:applyAngularImpulse(40)
			self.thrusters[5].setpower = 6
			self.thrusters[8].setpower = 6
		end
	elseif SERVER then
		local structure = self.context:findById(self.extra.seat.structure)

		if structure then
			local component = structure.components.byName[self.extra.seat.seat]

			if component then
				component:updateKeys(self.left, self.right, self.up, self.down, self.rleft, self.rright)
			else
				print("Sitting in a nil seat?!")
			end
		else
			print("Sitting in a nil structure?!")
		end
	end

	local thrusterIntensity = 0
	local thrusterBase = 0
	for i = 1, #self.thrusters do
		local thruster = self.thrusters[i]
		thruster.phase = (thruster.phase or (math.random() * math.pi * 2)) + (dt * 30)
		thruster.power = thruster.power + ((thruster.setpower - thruster.power) * dt * 10)
		if thruster.setpower == 0 and thruster.power < 0.5 then
			thruster.power = 0
		end
		thrusterBase = thrusterBase + thruster.setpower
		thrusterIntensity = thrusterIntensity + thruster.power
	end

	if CLIENT and self.thrustersound then
		if thrusterIntensity > 0.5 then
			self.thrustersound:setVolume(0.5 - (0.5 / (thrusterIntensity * 2)))
			self.thrustersound:setPitch(0.7 - (0.7 / (1.8 + math.max(0, thrusterBase-thrusterIntensity))))
			if not self.thrustersound:isPlaying() then
				self.thrustersound:play()
			end
		else
			self.thrustersound:stop()
		end
	end
end

function Player:draw()
	love.graphics.setColor(255,255,255)
	love.graphics.push()
	love.graphics.translate(self.body:getX(),self.body:getY())
	love.graphics.rotate(self.body:getAngle())
	love.graphics.rectangle("line",-8,-16,16,32)
	for i = 1, #self.thrusters do
		local thruster = self.thrusters[i]
		if thruster.power > 0 then
			love.graphics.push()
			love.graphics.translate(thruster.x, thruster.y)
			love.graphics.rotate(thruster.angle)
			local power=math.max(0, thruster.power + math.sin(thruster.phase) * 0.5)
			love.graphics.setColor(64, 128, 255, 255 / (power / 2))
			love.graphics.polygon("fill", -thruster.size, 0, 0, power, thruster.size, 0)
			love.graphics.setColor(255, 32, 32, 255 / (power / 2))
			love.graphics.polygon("fill", -thruster.size/1.5, 0, 0, power / 3, thruster.size/1.5, 0)
			love.graphics.setColor(64, 128, 255, 255 - (255 / (power / 2)))
			love.graphics.setLineWidth(1)
			love.graphics.line(-thruster.size, 0, 0, power, thruster.size, 0)
			love.graphics.pop()
		end
	end
	love.graphics.pop()

	--self:debugDraw()
end

function Player:useItem(x, y)
	if CLIENT then
		local buffer = love.string.newWriteBuffer()
		buffer:writeByte(TYPES.USE_ITEM)
		buffer:writeDouble(x)
		buffer:writeDouble(y)
		self.context:send(network.entityPacket(self, buffer:getString()))
	else
		local item = self.inventory[self.selected]
		if item then
			item:use(self, x, y)
		end
	end
end

function Player:configure(x, y)
	if CLIENT then
		local buffer = love.string.newWriteBuffer()
		buffer:writeByte(TYPES.CONFIGURE)
		buffer:writeDouble(x)
		buffer:writeDouble(y)
		self.context:send(network.entityPacket(self, buffer:getString()))
	else
		local item = self.inventory[self.selected]
		if item and item:configure(self, x, y, "pre") then
			return
		end

		for i, v in pairs(self.context.entities) do
			if v:isA("common.entity.Structure") then
				local x1,y1,x2,y2 = v:getBoundingBox()

				if x >= x1 and y >= y1 and x <= x2 and y <= y2 then
					if v:testPoint(x, y) then
						local lx, ly = v.body:getLocalPoint(x, y)
						lx = math.floor(lx/16)
						ly = math.floor(ly/16)

						local slx, sly = v.body:getLocalPoint(x, y)

						v:get(lx, ly):configure(self, slx/16, sly/16)

						return
					end
				end
			end
		end

		local item = self.inventory[self.selected]
		if item and item:configure(self, x, y, "post") then
			return
		end
	end
end

function Player:selectItem(i)
	if CLIENT then
		local buffer = love.string.newWriteBuffer()
		buffer:writeByte(TYPES.SELECT_ITEM)
		buffer:writeByte(i)
		self.context:send(network.entityPacket(self, buffer:getString()))
	else
		self.selected = i
	end
end

function Player:processPacket(player, buffer)
	local type = buffer:readByte()

	if type == TYPES.SYNC_KEYS then
		local up = buffer:readByte() > 0
		local down = buffer:readByte() > 0
		local left = buffer:readByte() > 0
		local right = buffer:readByte() > 0
		local rleft = buffer:readByte() > 0
		local rright = buffer:readByte() > 0

		if self.username == player.username then
			self.up = up
			self.down = down
			self.left = left
			self.right = right
			self.rleft = rleft
			self.rright = rright
		end
	elseif type == TYPES.USE_ITEM then
		local x, y = buffer:readDouble(), buffer:readDouble()

		if self.username == player.username then
			self:useItem(x, y)
			--TODO: Sync items with our client
		end
	elseif type == TYPES.SELECT_ITEM then
		local i = buffer:readByte()

		if self.username == player.username then
			self:selectItem(i)
		end
	elseif type == TYPES.CONFIGURE then
		local x, y = buffer:readDouble(), buffer:readDouble()

		if self.username == player.username then
			self:configure(x, y)
		end
	end
end

function Player:readSyncData(buffer)
	local ogun = self.username

	self.username = buffer:readString()
	self.up = buffer:readByte() > 0
	self.down = buffer:readByte() > 0
	self.left = buffer:readByte() > 0
	self.right = buffer:readByte() > 0
	self.rleft = buffer:readByte() > 0
	self.rright = buffer:readByte() > 0
	self.selected = buffer:readByte()

	if self.username == self.context.username then
		self.localplayer = true
	end

	if not ogun then
		self.id = uuid.generateFrom(self.username)
	end
end

function Player:writeSyncData(buffer)
	buffer:writeString(self.username)
	buffer:writeByte(self.up and 1 or 0)
	buffer:writeByte(self.down and 1 or 0)
	buffer:writeByte(self.left and 1 or 0)
	buffer:writeByte(self.right and 1 or 0)
	buffer:writeByte(self.rleft and 1 or 0)
	buffer:writeByte(self.rright and 1 or 0)
	buffer:writeByte(self.selected)
end
