local Structure, super = class.new("common.entity.Structure", "common.entity.Gravity")

local LinkedMap = class.import "util.LinkedMap"

local VertexBuffer, BufferLayer, BUFFER_LAYERS

if CLIENT then
	VertexBuffer = class.import "client.render.VertexBuffer"
	
	BufferLayer = class.new("common.entity.Structure.BufferLayer")
	
	function BufferLayer:init(name)
		self.name = name
		self.vertexBuffer = VertexBuffer:new()
	end
	
	function BufferLayer:start()
		self.vertexBuffer:start()
	end
	
	function BufferLayer:finish()
		self.vertexBuffer:finish()
	end
	
	function BufferLayer:onBlock(block)
		--Default behavior: do nothing
	end
	
	--TODO: Colored versions + VertexBuffer color support
	BUFFER_LAYERS = {
		{"background", BufferLayer},
		{"default", BufferLayer},
		{"foreground", BufferLayer}
	}
	
	Structure.BUFFER_LAYERS = BUFFER_LAYERS
end

local polygons = require "polygons"

local TYPES = network.constants {
	"BLOCK_REQUEST",
	"PLACE_BLOCK",
	"REMOVE_BLOCK",
	"BLOCKS",
	"SYNC_BLOCK_DATA",
}

--[[

So the blocks inside structures are going to use a linked map, basically.
	Literally just a left right up down field and stuff.
	Of course, frame of reference is held by the Box2D body objects.

The blocks will also be contained inside an internal list,
	we calculate whether blocks are still connected every time the structure is marked as dirty.
	We can then create new structures and internally detach blocks that physically detached.

Blocks will have a polygon representation and a Box2D representation.
	The Box2D representation is used to add a temporary shape to the fixture while calculating the unioned triangulated polygon
	in another thread.

Also, when the base part of the structure is destroyed, the game will select another random part to become the base part,
	usually in the center.

]]

function Structure:initPhysics(base)
	self.body:setType "dynamic"
	base = base or (class.import "common.block.Base":new())
	base.structure = self
	self.map = LinkedMap:new(base)
	
	if CLIENT then
		--WHEN I GET BACK FROM DOWNSTAIRS: Finish batchlayers, make them modular using inner classes, and fix the loading of inner
			--classes when the outer class isn't loaded (by attempting to find the first file in the class name that is not a 
				--package)
		self.bufferLayers = {}
		
		for i, v in ipairs(BUFFER_LAYERS) do
			local bl = v[2]:new(v[1])
			self.bufferLayers[i] = bl
			self.bufferLayers[v[1]] = bl
		end
		
		self.renderlod = 0
		self.dynamicRenderBlocks = {}
		self.drawReady = false
		self.rerender = false
		self.requestBlocks = true
	end
	
	self.dirty = true
	self.dynamicBlocks = {}
	self.syncBlocks = {}
	self.syncing = {}
	self.components = {byName = {}, byType = {}}
	
	base:onStructureAdded()
end

function Structure:update(dt)
	super.update(self, dt)
	
	if self.dirty then
		self.dirty = false
		
		if self.map:length() == 0 then
			self.context:despawn(self)
			return
		end
		
		self:splitIslands()
		self:batchPhysics()
		
		if CLIENT then
			self:renderVertexBuffer()
		end
	end
	
	for _, obj in ipairs(self.dynamicBlocks) do
		obj:update(dt)
	end
	
	if self.syncBlocks[1] then
		local ct = 0
		local buffer = love.string.newWriteBuffer()
		buffer:writeByte(TYPES.SYNC_BLOCK_DATA)
		while self.syncBlocks[1] and ct < 64 do
			local block = table.remove(self.syncBlocks, 1)
			self.syncing[block] = nil
		
			if self.map:find(block) then
				buffer:writeByte(1)
				buffer:writeShort(block.x)
				buffer:writeShort(block.y)
				
				buffer:startSection()
				block:serialize(buffer)
				buffer:endSection()
			end
		
			ct = ct+1
		end
		buffer:writeByte(0)
		
		self.context:broadcast(
			network.entityPacket(self, buffer:getString())
		)
	end
end

function Structure:requestSync(block)
	if not self.syncing[block] then
		self.syncBlocks[#self.syncBlocks+1] = block
		self.syncing[block] = true
	end
end

function Structure:testPoint(x, y)
	local lx, ly = self.body:getLocalPoint(x, y)
	lx = math.floor(lx/16)
	ly = math.floor(ly/16)
	
	if self.map:get(lx, ly) then
		return true
	end
end

--New batch shit
--We have specific sides, we say what side is opaque
--when we get the polygon for a side, we also pass the width of the side before it (so code can ajust properly for smaller walls and whatever)

function Structure:splitIslands()
	local sep = self.map:removeIslands()
	
	if SERVER and #sep > 0 then
		log("STRUCTURE", "Splitting structure", self)
		while #sep > 0 do
			log("STRUCTURE", "Number of disconnected "..(#sep))
			--create a new structure for remaining islands
			local base = table.remove(sep,1)
			local bx, by = base.x, base.y
			local struct = Structure:new(self.context, base)
			log("STRUCTURE", "Creating structure", struct)
			
			repeat
				local diter = 0
				local i = 1
				while sep[i] do
					local obj = sep[i]
				
					if struct.map:put(obj.x-bx, obj.y-by, obj) then
						table.remove(sep,i)
						obj.structure = struct
						obj:onStructureAdded()
						diter = diter+1
					else
						i = i+1
					end
				end
			until diter == 0
			
			local px, py = self.body:getWorldPoint(bx*16, by*16)
			struct.body:setPosition(px, py)
			struct.body:setAngle(self.body:getAngle())
			struct.body:setLinearVelocity(self.body:getLinearVelocityFromLocalPoint(bx*16, by*16))
			struct.body:setAngularVelocity(self.body:getAngularVelocity())
			self.context:spawn(struct)
		end
	end
end

function Structure:batchPhysics()
	self:removeAllShapes()
	
	--TODO: This doesn't have to be triangles, just anything with less than 8 verts
	--Optimize block.Base to regurgitate completely optimized geometry
	
	self.triangles = {}
	self.dynamicBlocks = {}
	
	local oldComponentsByName = self.components.byName
	local newComponents = {}
	self.components = {byName = {}, byType = {}}
	
	for pos, obj in self.map() do
		local tris = obj:getPolygon()
		if tris then
			for _, points in pairs(tris) do
				self.triangles[#self.triangles+1] = points
				for i=1, #points, 2 do
					points[i] = (points[i]+pos[1])*16
					points[i+1] = (points[i+1]+pos[2])*16
				end
			end
		end
		
		if obj.dynamic then
			self.dynamicBlocks[#self.dynamicBlocks+1] = obj
		end
		
		if obj:isA("common.block.Component") and obj.componentGlobal then
			self.components.byName[obj.componentName] = obj
			local t = self.components.byType[obj.componentType]
			if not t then
				t = {}
				self.components.byType[obj.componentType] = t
			end
			t[obj.componentName] = obj
			
			if not oldComponentsByName[obj.componentName] then
				newComponents[#newComponents+1] = obj
			end
		end
	end
	
	self.polygonPoints = self.triangles
	
	for _, triangle in pairs(self.polygonPoints) do
		self:addShape(love.physics.newPolygonShape(unpack(triangle)))
	end
	
	local removedComponents = {}
	for name, obj in pairs(oldComponentsByName) do
		if not self.components.byName[name] then
			removedComponents[#removedComponents+1] = obj
		end
	end
	
	if #newComponents > 0 or #removedComponents > 0 then
		for pos, obj in self.map() do
			if obj:isA("common.block.Component") then
				obj:onComponentsAdded(newComponents)
				obj:onComponentsRemoved(removedComponents)
			end
		end
	end
end

function Structure:renderVertexBuffer()
	local renderlod = math.floor(self.context.lod)
	self.renderlod = renderlod
	
	self.dynamicRenderBlocks = {}
	
	for i=1, #self.bufferLayers do
		local bl = self.bufferLayers[i]
		local name = bl.name
		local vb = bl.vertexBuffer
		
		local drb = {}
		self.dynamicRenderBlocks[name] = drb
		
		bl:start()
		vb:scale(16,16)
		for pos, obj in self.map() do
			if obj.layers[name] then
				vb:push()
				if obj.rotation ~= 0 then
					vb:translate(-8,-8)
					vb:rotate(obj.rotation*(math.pi/2))
					vb:translate(pos[1]*16+8, pos[2]*16+8)
				else
					vb:translate(pos[1]*16, pos[2]*16)
				end
		
				if obj.meshes then
					if obj.meshes[name] then
						local meshes = mdf.selectLOD(obj.meshes[name], renderlod)
						for j=1, #meshes do
							local mesh = meshes[j]
							for i=1, #mesh do
								vb:add(mesh[i][1], mesh[i][2], mesh[i][3], mesh.image)
							end
						end
					end
				else
					if obj.batchDraw then
						obj:render(renderlod, vb, name)
					else
						drb[#drb+1] = obj
					end
				end
		
				vb:pop()
			end
		end
		bl:finish()
	end
	
	self.drawReady = true
	self.rerender = false
end

function Structure:draw(lod)
	local loddiff = math.abs(math.floor(lod)-self.renderlod)
	if loddiff >= 1 then
		--TODO: Test to see if there are different meshes for this new lod before doing a re render
		self:renderVertexBuffer()
	end
	
	if self.rerender then
		self:renderVertexBuffer()
	end

	love.graphics.push()
	love.graphics.translate(self.body:getX(),self.body:getY())
	love.graphics.rotate(self.body:getAngle())
	
	if self.drawReady then
		for i=1, #self.bufferLayers do
			local bl = self.bufferLayers[i]
			local name = bl.name
			local vb = bl.vertexBuffer
		
			love.graphics.setColor(255,255,255)
			vb:draw()
		
			for _, obj in ipairs(self.dynamicRenderBlocks[name]) do
				obj:draw(self.renderlod)
			end
		end
	end
	
	--[[if DEBUGGING and self.polygonPoints then
		love.graphics.setColor(0,0,0)
		for i, v in pairs(self.polygonPoints) do
			love.graphics.polygon("line", v)
		end
		
		love.graphics.setColor(255,0,255,128)
		love.graphics.rectangle("fill", self.map.base.x*16, self.map.base.y*16, 16, 16)
	end]]
	love.graphics.pop()
end

function Structure:put(x, y, o)
	local buffer = love.string.newWriteBuffer()
	buffer:writeByte(o and TYPES.PLACE_BLOCK or TYPES.REMOVE_BLOCK)
	buffer:writeShort(x)
	buffer:writeShort(y)
	
	if o then
		buffer:writeString(o.classname)
	end
	
	if SERVER then
		self.map:put(x, y, o)
		
		if o then
			o.structure = self
			o:onStructureAdded()
		end
		
		self.dirty = true
		
		self.context:broadcast(
			network.entityPacket(self, buffer:getString())
		)
	else
		self.context:send(
			network.entityPacket(self, buffer:getString())
		)
	end
end

function Structure:get(x, y)
	return self.map:get(x, y)
end

function Structure:processPacket(player, buffer)
	local typ = buffer:readByte()
	if CLIENT then
		--Client side code
		if typ == TYPES.PLACE_BLOCK then
			local x, y, classname = buffer:readShort(), buffer:readShort(), buffer:readString()
			
			local blk = class.importSafe(classname, "common.block.Base"):new()
			self.map:put(x, y, blk)
			
			blk.structure = self
			blk:onStructureAdded()
			
			self.dirty = true
		elseif typ == TYPES.REMOVE_BLOCK then
			local x, y = buffer:readShort(), buffer:readShort()
			
			self.map:put(x, y)
			self.dirty = true
		elseif typ == TYPES.BLOCKS then
			local len = buffer:readInt()
			local bx, by = buffer:readShort(), buffer:readShort()
			log("STRUCT", "GOT BLOCK RESPONSE "..len)
			for i=1, len do
				local x, y, classname = buffer:readShort(), buffer:readShort(), buffer:readString()
				
				local blk = class.importSafe(classname, "common.block.Base"):new()
				self.map:put(x, y, blk)
				blk.structure = self
				blk:onStructureAdded()
				
				buffer:startSection()
				blk:unserialize(buffer)
				buffer:endSection()
			end
			self.dirty = true
			log("STRUCT", "n blocks: "..self.map:length())
			
			self.map.base = self.map:get(bx, by)
		elseif typ == TYPES.SYNC_BLOCK_DATA then
			while buffer:readByte() ~= 0 do
				local x, y = buffer:readShort(), buffer:readShort()
				local block = self.map:get(x, y)
				
				buffer:startSection()
				if block then
					block:unserialize(buffer)
				end
				buffer:endSection()
			end
		end
	else
		if typ == TYPES.BLOCK_REQUEST then
			--send back the blocks to the client--
			local response = love.string.newWriteBuffer()
			response:writeByte(TYPES.BLOCKS)
			response:writeInt(self.map:length())
			response:writeShort(self.map.base.x)
			response:writeShort(self.map.base.y)
			
			for pos, obj in self.map() do
				response:writeShort(pos[1])
				response:writeShort(pos[2])
				response:writeString(obj.classname)
				
				response:startSection()
				obj:serialize(response)
				response:endSection()
			end
			
			self.context:sendTo(player, network.entityPacket(self, response:getString()))
		elseif typ == TYPES.PLACE_BLOCK then
			local x, y, classname = buffer:readShort(), buffer:readShort(), buffer:readString()
			
			local blk = class.importSafe(classname, "common.block.Base"):new()
			self:put(x, y, blk)
		elseif typ == TYPES.REMOVE_BLOCK then
			local x, y = buffer:readShort(), buffer:readShort()
			
			self:put(x, y)
		end
	end
end

--Reads sync data serialized in a sync packet
--CLIENT SIDE ONLY
function Structure:readSyncData(buffer)
	--simply compares the number of blocks in the client vs the server--
	--if they do not match, the client will request the blocks from the server--
	if buffer:readInt() ~= self.map:length() or self.requestBlocks then
		log("STRUCT", "Send block request")
		self.context:send(network.entityPacket(self, string.char(TYPES.BLOCK_REQUEST)))
		self.requestBlocks = false
	end
end

--Writes sync data to be serialized into a sync packet
--SERVER SIDE ONLY
function Structure:writeSyncData(buffer)
	buffer:writeInt(self.map:length())
end

function Structure:writeSaveData(buffer)
	buffer:writeInt(self.map:length())
	buffer:writeShort(self.map.base.x)
	buffer:writeShort(self.map.base.y)
	
	for pos, obj in self.map() do
		buffer:writeShort(pos[1])
		buffer:writeShort(pos[2])
		buffer:writeString(obj.classname)
		
		buffer:startSection()
		obj:serialize(buffer, true)
		buffer:endSection()
	end
end

function Structure:readSaveData(buffer)
	local len = buffer:readInt()
	local bx, by = buffer:readShort(), buffer:readShort()
	log("STRUCT", "LOAD "..len.." BLOCKS FROM DISK")
	for i=1, len do
		local x, y, classname = buffer:readShort(), buffer:readShort(), buffer:readString()
		
		local blk = class.importSafe(classname, "common.block.Base"):new()
		self.map:put(x, y, blk, true)
		blk.structure = self
		blk:onStructureAdded()
		
		buffer:startSection()
		local s, e = pcall(blk.unserialize, blk, buffer)
		if not s then
			log("STRUCT", "Block encountered error while loading:")
			log("STRUCT", "\t("..x..", "..y..") "..classname)
			log("STRUCT", "\t"..e)
		end
		buffer:endSection()
	end
	self.dirty = true
	
	self.map.base = self.map:get(bx, by)
end
