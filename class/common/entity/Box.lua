local Box, super = class.new("common.entity.Box", "common.entity.Gravity")

function Box:initPhysics()
	self.body:setType "dynamic"
	local angle = math.random()*math.pi*2
	self.body:setPosition((math.cos(angle)*400)+400,(math.sin(angle)*400)+400)
	self:addShape(love.physics.newRectangleShape(16,16))
end

function Box:update(dt)
	super.update(self, dt)
end

function Box:draw()
	love.graphics.setColor(149,93,26)
	love.graphics.push()
	love.graphics.translate(self.body:getX(),self.body:getY())
	love.graphics.rotate(self.body:getAngle())
	love.graphics.rectangle("fill",-8,-8,16,16)
	love.graphics.pop()
	
	--self:debugDraw()
end
