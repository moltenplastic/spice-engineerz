local Planet, super = class.new("common.entity.Planet", "common.entity.Base")

local function generatePolygon(s,size)
	local rot = 0
	local poly = {}
	for i=1,s do
		poly[#poly+1] = math.sin(rot)*size+((love.math.random()-0.5)*8)
		poly[#poly+1] = -math.cos(rot)*size+((love.math.random()-0.5)*8)
		rot = rot+math.rad(360/(s+love.math.random()))
	end
	return poly
end

function Planet:initPhysics()
	self.body:setPosition(400,300)
	
	local polygon = generatePolygon(32,128)
	local triangulated = love.math.triangulate(polygon)
	for _, tri in ipairs(triangulated) do
		self:addShape(love.physics.newPolygonShape(tri))
	end
end

function Planet:draw()
	--love.graphics.circle("line",planet.body:getX(),planet.body:getY(),planet.shape:getRadius())
	love.graphics.setColor(255,255,255)
	love.graphics.push()
	love.graphics.translate(self.body:getX(),self.body:getY())
	love.graphics.rotate(self.body:getAngle())
	--love.graphics.circle("line",0,0,128)
	--love.graphics.rectangle("line",-64,-64,128,128)
	for i, v in pairs(self.shapes) do
		love.graphics.polygon("fill",v:getPoints())
	end
	love.graphics.pop()
end
