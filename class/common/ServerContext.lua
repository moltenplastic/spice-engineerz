local ServerContext, super = class.new("common.ServerContext", "common.Context")

--Initializes a ServerContext with an ENET Host object
function ServerContext:init(host)
	super.init(self)
	self.host = host
	self.tag = "SCONTEXT"
	self.removed = {}
end

function ServerContext:despawn(ent)
	super.despawn(self, ent)
	self.removed[#self.removed+1] = ent
end

--Broadcasts a message from an entity in the default network range
function ServerContext:broadcastFrom(ent, data)
	--self.host:broadcast(
end

--Broadcasts a message to all clients connected to the server
--TODO: Only include clients that are connected to the server in the PLAY state
function ServerContext:broadcast(data,channel,flags)
	self.host:broadcast(data,channel,flags)
end

--Sends a message to a player object
function ServerContext:sendTo(player, data)
	player.peer:send(data)
end

--Unused
function ServerContext:shouldSync()
	for _, entity in ipairs(self.entities) do
		if entity.needsSync then return true end
	end
	return false
end

--Unused
function ServerContext:createSyncPacket()
	--go through all entities
	--if an entity needs a sync, add it to this list
	--new entities always need a sync
	
	local sync = {}
	
	for _, entity in ipairs(self.entities) do
		if entity.needsSync then
			local sdata = entity:createSyncPacket()
			sdata.id = entity.id
			sdata.classname = classname
			sync[entity.id] = sdata
			
			entity.needsSync = false
		end
	end
	
	return sync
end

local REGION_SIZE = 16384

function ServerContext:updateRegions()
	self.regions = {}
	for _, entity in ipairs(self.entities) do
		local cmx, cmy = entity.body:getMassData()
		
		local regionx, regiony = math.floor(cmx/REGION_SIZE), math.floor(cmy/REGION_SIZE)
		
		if not self.regions[regionx] then
			self.regions[regionx] = {}
		end
		
		if not self.regions[regionx][regiony] then
			self.regions[regionx][regiony] = {x = regionx, y = regiony}
		end
		
		table.insert(self.regions[regionx][regiony], entity)
	end
end

--TODO: Save directories in this format:
--[[
save_folder/main.sesf: main save file, magic: "SESFMAIN"
save_folder/entity/0_0.sesf: entity save files, split into 16384x16384 regions, magic "SESFENTY"
save_folder/computer/<entityid>.sesf: computer savestate, contains ram and cpu state, magic "SESFLVX\0"
]]

--[[
SESF header:
4 bytes: container magic, "SESF"
4 bytes: data type, "MAIN", "ENRG", "LVX\0"
1 byte: compression format (0 = none, 1 = lz4, 2 = zlib, 3 = gzip)
4 bytes: compressed data size
... bytes: compressed data
]]

--[[
SESF Main:
4 bytes: number of entity regions
For every entity region:
	2 bytes: entity region x/16384
	2 bytes: entity region y/16384
]]

--[[
Entity Regions:
Entities are encoded into regions by their position in the middle of their center of mass
Specific regions can be saved, and entities can request to get saved by setting saveInvalidated to true
Entities are not saved immediately during runtime.
During runtime, entities are saved with LZ4 compression. When doing a "dedicated save", they are saved using ZLIB with maximum
	compresison

SESF Entity Region:
4 bytes: number of entities
For every entity:
	String: classname
	String: id
	Data Section: entity data
]]

local compressionFormats = {
	[1] = "lz4",
	[2] = "zlib",
	[3] = "gzip",
	lz4 = 1,
	zlib = 2,
	gzip = 3
}

local defaultCompressionLevels = {
	lz4 = -1,
	zlib = 9,
	gzip = -1
}

local function decodeSESF(buffer)
	--extracts the data from an SESF save file--
	assert(buffer:readBytes(4) == "SESF", "Invalid SESF magic!")
	
	local dataMagic = buffer:readBytes(4)
	local compression = compressionFormats[buffer:readByte(1)]
	local compressedData = buffer:readString()
	return dataMagic,love.string.newReadBuffer(compression and love.math.decompress(compressedData, compression) or compressedData)
end

local function encodeSESF(magic, data, compression)
	local compressedData = compression and
		(love.math.compress(data, compression, defaultCompressionLevels[compression]):getString()) or (data:getString())
	
	local buffer = love.string.newWriteBuffer()
	buffer:writeBytes("SESF"..magic)
	buffer:writeByte(compressionFormats[compression] or 0)
	buffer:writeString(compressedData) --TODO: writeString take love::Data
	
	return buffer
end

local function copyFile(source, dest)
	local s = love.filesystem.newFile(source, "r")
	local d = love.filesystem.newFile(dest, "w")
	
	while not s:isEOF() do
		d:write(s:read(4096))
	end
	
	d:flush()
	d:close()
	s:close()
end

local function copyDirectory(source, dest)
	love.filesystem.remove(dest)
	love.filesystem.createDirectory(dest)
	for _, file in pairs(love.filesystem.getDirectoryItems(source)) do
		local sourcefile = source.."/"..file
		local destfile = dest.."/"..file
		if love.filesystem.isDirectory(sourcefile) then
			copyDirectory(sourcefile, destfile)
		else
			copyFile(sourcefile, destfile)
		end
	end
end

local function backup(file)
	if love.filesystem.isFile(file) then
		copyFile(file, file..".bak")
	elseif love.filesystem.isDirectory(file) then
		copyDirectory(file, file.."_bak")
	end
end

local function backupAndWrite(file, data)
	backup(file)
	love.filesystem.write(file, data)
end

function ServerContext:save(dir, compression)
	print("Saving ServerContext...")
	love.filesystem.createDirectory(dir)
	self:updateRegions()
	backupAndWrite(dir.."/main.sesf", self:saveMain(compression))
	
	backup(dir.."/entity")
	love.filesystem.remove(dir.."/entity")
	love.filesystem.createDirectory(dir.."/entity")
	for x, row in pairs(self.regions) do
		for y, region in pairs(row) do
			local fn = (dir.."/entity/%d_%d.sesf"):format(x, y)
			backupAndWrite(fn, self:saveRegion(region, compression))
		end
	end
end

function ServerContext:saveMain(compressionLevel)
	local buffer = love.string.newWriteBuffer()
	
	local regionList = {}
	for x, row in pairs(self.regions) do
		for y, region in pairs(row) do
			regionList[#regionList+1] = region
		end
	end
	
	local nregions = #regionList
	print(nregions, "regions")
	buffer:writeInt(nregions)
	
	for _, region in ipairs(regionList) do
		print("Writing region", region.x, region.y)
		buffer:writeShort(region.x)
		buffer:writeShort(region.y)
	end
	
	print("Done, uncompressed save size:", buffer:getSize())
	
	return encodeSESF("MAIN", buffer, compressionLevel)
end

function ServerContext:saveRegion(region, compressionLevel)
	local buffer = love.string.newWriteBuffer()
	
	local savedEntities = {}
	for _, entity in ipairs(region) do
		if not entity.noSave then
			savedEntities[#savedEntities+1] = entity
		end
	end
	
	local nents = #savedEntities
	print(nents, "entities")
	buffer:writeInt(nents)
	
	for _, entity in ipairs(savedEntities) do
		buffer:writeString(entity.classname)
		buffer:writeString(entity.id)
		buffer:startSection()
		entity:createSyncPacket(buffer, true)
		buffer:endSection()
	end
	
	return encodeSESF("ENRG", buffer, compressionLevel)
end

function ServerContext:load(dir)
	self:loadMain(love.string.newReadBuffer(love.filesystem.read(dir.."/main.sesf")))
	
	for x, row in pairs(self.regions) do
		for y, region in pairs(row) do
			local fn = (dir.."/entity/%d_%d.sesf"):format(x, y)
			self:loadRegion(love.string.newReadBuffer(love.filesystem.read(fn)))
		end
	end
end

function ServerContext:loadMain(buffer)
	local magic, buffer = decodeSESF(buffer)
	
	if magic ~= "MAIN" then
		error("Invalid Magic for Main")
	end
	
	local nregions = buffer:readInt()
	print(nregions, "regions")
	
	self.regions = {}
	for i=1, nregions do
		local x, y = buffer:readShort(), buffer:readShort()
		self.regions[x] = self.regions[x] or {}
		self.regions[x][y] = {x=x, y=y}
	end
end

function ServerContext:loadRegion(buffer)
	local magic, buffer = decodeSESF(buffer)
	
	if magic ~= "ENRG" then
		error("Invalid Magic for Entity Region")
	end
	
	local nents = buffer:readInt()
	
	print(nents, "entities")
	for i=1, nents do
		local clsname = buffer:readString()
		local cls = class.importSafe(clsname)
		local id = buffer:readString()
		
		if not cls then
			print("Entity with a class name of "..clsname.." skipped!")
			buffer:skipSection()
		else
			local entity = cls:new(self)
			entity.id = id
			buffer:startSection()
			entity:readSyncPacket(buffer, true)
			buffer:endSection()
			self:spawn(entity)
		end
	end
end

--[[function ServerContext:load(compressed)
	--TODO: Make newReadBuffer take a Data object from Love2D
	local buffer = love.string.newReadBuffer(love.math.decompress(compressed, "zlib"))
	print("Loading save, size:", buffer:getSize())
	
	local magic = buffer:readBytes(4)
	if magic ~= "SESF" then
		error("Save magic invalid!")
	end
	
	local nents = buffer:readInt()
	
	print(nents, "entities")
	for i=1, nents do
		local clsname = buffer:readString()
		local cls = class.importSafe(clsname)
		
		if not cls then
			--TODO:
			--This errors because I haven't set up bounary recording for userdata yet
			--Boundary recording would let me skip part of the data structure if something fails (example: loading a save
				--that contains an entity for a mod that was uninstalled)
			print("Entity with a class name of "..clsname.." skipped!")
			buffer:skipSection()
		else
			local entity = cls:new(self)
			entity.id = buffer:readString()
			buffer:startSection()
			entity:readSyncPacket(buffer, true)
			buffer:endSection()
			self:spawn(entity)
		end
	end
end]]
