local Context = class.new "common.Context"

--Initializes a context, this creates a Box2D world
function Context:init()
	self.world = love.physics.newWorld(0,0)
	
	self.world:setCallbacks(function(f1, f2, contact)
		local f1body = f1:getBody()
		local f2body = f2:getBody()
		
		local f1entity = f1body:getUserData()
		local f2entity = f2body:getUserData()
		
		if f1entity and f2entity then
			f1entity:onCollision(f2entity, contact, f1, f2)
			f2entity:onCollision(f1entity, contact, f2, f1)
		end
	end)
	
	self.entities = {}
	self.entitiesByType = setmetatable({},{__index=function(_,i)
		rawset(_,i,setmetatable({},{__mode="v"}))
		return _[i]
	end})
	self.tag = "CONTEXT"
end

--Updates the Box2D world and all it's entities
function Context:update(dt)
	self.world:update(dt)
	for _, entity in ipairs(self.entities) do
		entity:update(dt)
	end
end

--Spawns an entity in the world
function Context:spawn(entity)
	log(self.tag, "Spawning entity "..tostring(entity))
	
	self.entities[#self.entities+1] = entity
	local class = entity
	while class.classname and class.classname ~= "common.entity.Base" do
		self.entitiesByType[class.classname][#self.entitiesByType[class.classname]+1] = entity
		class = class.meta.super
	end
end

--Despawns an entity from the world
function Context:despawn(entity)
	for i, v in ipairs(self.entities) do
		if v == entity then
			table.remove(self.entities,i)
			entity:destroy()
			return
		end
	end
end

--Finds the nearest entity derived from classname at x, y
function Context:getNearest(classname, x, y)
	local closestDist, closest = math.huge
	for _, ent in pairs(self.entitiesByType[classname]) do
		local dist = (x-ent.body:getX())^2+(y-ent.body:getY())^2
		if dist < closestDist then
			closestDist, closest = dist, ent
		end
	end
	
	return closestDist, closest
end

--Finds an entity by id
function Context:findById(id)
	for i, v in ipairs(self.entities) do
		if v.id == id then
			return v
		end
	end
end
