local Drill, super = class.new("common.item.Drill", "common.item.Base")

local MODEL, MODEL_HEIGHT

if CLIENT then
	resource.getAsync("global:model/saw.mdf", function(lods)
		MODEL = mdf.toVertexBuffers(lods, function(lod, vb)
			local x1,y1,x2,y2 = mdf.computeBounds(lod.objects)
			MODEL_HEIGHT = y2-y1
			vb:translate((-((x1+x2)/2)),(-((y1+y2)/2)))
			vb:scale(16,16)
		end)
	end)
end

function Drill:initItem(type)
	self.name = "Drill"
	self.description = "A Drill"
	self.hasDurability = true
end

function Drill:use(player, x, y)
	--find the object that was clicked--
	--first, compare bounding boxes--
	for i, v in pairs(self.context.entities) do
		if v:isA("common.entity.Structure") then
			local x1,y1,x2,y2 = v:getBoundingBox()
	
			if x >= x1 and y >= y1 and x <= x2 and y <= y2 then
				if v:testPoint(x, y) then
					local lx, ly = v.body:getLocalPoint(x, y)
					lx = math.floor(lx/16)
					ly = math.floor(ly/16)
					
					v:put(lx, ly)
					
					return
				end
			end
		end
	end
end

function Drill:draw()
	love.graphics.setColor(255,255,255)
	love.graphics.push()
	love.graphics.translate(8,8)
	love.graphics.scale(1/MODEL_HEIGHT)
	MODEL[3]:draw()
	love.graphics.pop()
end
