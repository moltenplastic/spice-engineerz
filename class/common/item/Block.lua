local Block, super = class.new("common.item.Block", "common.item.Base")

function Block:initItem(type)
	self.blockClass = class.importSafe(type, "common.block.Base")
	self.name = self.blockClass.name or "Block"
	self.description = self.blockClass.description or "A Block"
	
	self.hasQuantity = true
	self.quantity = 1
end

function Block:serialize(buffer)
	buffer:writeString(self.blockClass.classname)
end

function Block:deserialize(buffer)
	self.blockClass = class.importSafe(buffer:readString(), "common.block.Base")
end

function Block:use(player, x, y)
	--see if the click position local to the structure object thats near us borders another block (indicating a place)--
	for i, v in pairs(self.context.entities) do
		if v:isA("common.entity.Structure") then
			local lx, ly = v.body:getLocalPoint(x, y)
			lx = math.floor(lx/16)
			ly = math.floor(ly/16)
		
			if not v:get(lx, ly) then
				if v.map:hasNeighbors(lx, ly) then
					v:put(lx, ly, self.blockClass:new())
					self.quantity = self.quantity-1
					return
				end
			end
		end
	end
	
	--else, spawn new--
	local struct = class.import "common.entity.Structure":new(self.context, self.blockClass:new())
	self.context:spawn(struct)
	struct.body:setPosition(x, y)
	struct.body:setLinearVelocity(player.body:getLinearVelocity())
	struct.body:setAngularVelocity(player.body:getAngularVelocity())
	struct.body:setAngle(player.body:getAngle())
	self.quantity = self.quantity-1
end
