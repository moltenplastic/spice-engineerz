local Missile, super = class.new("common.item.Missile", "common.item.Base")

function Missile:initItem(type)
	self.missileClass = class.importSafe(type, "common.entity.Missile")
	self.hasDurability = true
end

function Missile:serialize(buffer)
	buffer:writeString(self.missileClass.classname)
end

function Missile:deserialize(buffer)
	self.missileClass = class.importSafe(buffer:readString(), "common.entity.Missile")
end

function Missile:use(player, x, y)
	local struct = self.missileClass:new(self.context)
	self.context:spawn(struct)
	struct.body:setPosition(x, y)
	local vx, vy = player.body:getLinearVelocity()
	local angle = (math.pi / 2) - math.atan2(x - player.body:getX(), y - player.body:getY())
	struct.body:setLinearVelocity(vx + math.cos(angle) * 70, vy + math.sin(angle) * 70)
	struct.body:setAngularVelocity(player.body:getAngularVelocity())
	struct.body:setAngle(angle)
end
