local Base = class.new "common.item.Base"

local ITEM_INSTANCES = setmetatable({},{__mode="v"})

function Base:init(context,...)
	self.context = context
	self.name = "Item Base"
	self.description = "Item Description :)"
	self.dynamic = false --if true, :update() will get called
	self.animated = false --if true, :draw() will not get batched by canvases
	
	self.hasQuantity = false
	self.quantity = 1
	
	self.hasDurability = false
	self.durability = 100
	self.maxDurability = 100
	
	self.setID(uuid.generate())
	
	self:initItem(...)
end

function Base:initItem(...)
	
end

function Base:update(dt, location, locationExtra)
	--location: string, usually "bag", "floating", or "inventory"
		--bag: locationExtra is the player
		--floating: locationExtra is the Item Entity
		--inventory: locationExtra is the Inventory object
end

-- TODO: Make items 1x1 based with scaling apply (but overlays have to have the size passed to it)
function Base:draw()
	love.graphics.setColor(255,255,255)
	love.graphics.rectangle("fill", 0, 0, 16, 16)
end

function Base:drawOverlay()
	if self.hasQuantity then
		love.graphics.setFont(Fonts.Regular)
		love.graphics.setColor(0, 0, 0)
		love.graphics.print(tostring(self.quantity), 16, 17)
		love.graphics.print(tostring(self.quantity), 16, 15)
		love.graphics.print(tostring(self.quantity), 15, 16)
		love.graphics.print(tostring(self.quantity), 17, 16)
		love.graphics.setColor(255, 255, 255)
		love.graphics.print(tostring(self.quantity), 16, 16)
	end
	
	if self.hasDurability then
		love.graphics.setColor(255, 0, 0)
		love.graphics.rectangle("fill", 0, 16-2, 16, 2)
		love.graphics.setColor(0, 255, 0)
		love.graphics.rectangle("fill", 0, 16-2, 16*(self.durability/self.maxDurability), 2)
	end
end

function Base.checkClickInRange(player, x, y, range)
	range = range or 128
	local px, py = player.body:getPosition()
	local dist = ((x-px)^2+(y-py)^2)^0.5
	return dist <= range
end

function Base:use(player, x, y) --world coords are returned
	print(player, x, y, self.checkClickInRange(player, x, y))
end

function Base:configure(player, x, y, when)
	return false
end

function Base:setID(id)
	if self.id ~= id then
		if self.id then
			ITEM_INSTANCES[self.id] = nil
		end
		self.id = id
		ITEM_INSTANCES[self.id] = self
	end
end

function Base:toBuffer(buffer)
	buffer:writeString(self.id)
	
	if self.hasQuantity then
		buffer:writeInt(self.quantity)
	end
	if self.hasDurability then
		buffer:writeInt(self.durability)
	end
	
	self:serialize(buffer)
end

function Base:fromBuffer(buffer)
	self:setID(buffer:readString())
	
	if self.hasQuantity then
		self.quantity = buffer:readInt()
	end
	if self.hasDurability then
		self.durability = buffer:readInt()
	end
	
	self:deserialize(buffer)
end

function Base:serialize(buffer)
	
end

function Base:deserialize(buffer)
	
end
