local ClientContext, super = class.new("common.ClientContext", "common.Context")

local enet = require "enet"

--Initializes a ClientContext with an address and a port
function ClientContext:init(address, port, username)
	super.init(self)
	
	self.host = enet.host_create()
	print(address, port)
	self.server, self.error = self.host:connect(address..":"..port,2)
	assert(self.server, self.error)
	self.state = network.PEERSTATE.LOGIN
	self.username = username or "Player"..love.math.random(0,9999)
	self.tag = "CCONTEXT"
	
	self.deliverQueue = {}
	self.localPlayer = nil
	self.connected = false
	self.connectionStarted = love.timer.getTime()
end

--Sends data to the server
function ClientContext:send(data)
	self.server:send(data)
end

--Quits the server: Sends the quit packet and then disconnects
function ClientContext:quit()
	self:send(string.char(network.SERVERBOUND.QUIT))
	self.host:flush()
end

local gettime = require "socket".gettime

--Updates the underlying context object, then read network communications
function ClientContext:update(dt)
	super.update(self, dt)
	
	if not self.localPlayer then
		for _, ent in pairs(self.entitiesByType["common.entity.Player"]) do
			if ent.username == self.username then
				self.localPlayer = ent
			end
		end
	end
	
	while self.deliverQueue[1] do
		local data = self.deliverQueue[1]
		
		local ent = self:findById(data[1])
		
		if ent then
			ent:processPacket(nil, data[2])
			table.remove(self.deliverQueue, 1)
		else
			break
		end
	end
	
	while true do
		local event, err = self.host:service(0)
		
		if event then
			local a = love.timer.getTime()
			if event.type == "connect" then
				if self.onConnect then
					self:onConnect()
				end
				
				self.connected = true
				
				local buffer = love.string.newWriteBuffer()
				buffer:writeByte(network.SERVERBOUND.LOGIN)
				buffer:writeString(self.username)
				event.peer:send(buffer:getString())
			elseif event.type == "disconnect" then
				for i, v in pairs(event) do print(i,v) end
				print("Disconnected from server")
			elseif event.type == "receive" then
				local buffer = love.string.newReadBuffer(event.data)
				local packetType = buffer:readByte()
			
				--log("CLCONTEXT", packetType..": "..(#sdata))
			
				if self.state == network.PEERSTATE.LOGIN then
					if packetType == network.CLIENTBOUND.LOGGEDIN then
						log(self.tag, "Logged in")
						self.state = network.PEERSTATE.PLAY
						
						if self.onLogin then
							self:onLogin()
						end
					end
				elseif self.state == network.PEERSTATE.PLAY then
					if packetType == network.CLIENTBOUND.SYNC then
						local sendtime = buffer:readDouble()
						local lag = gettime()-sendtime
						
						local len = buffer:readInt()
						
						for i=1, len do
							local id = buffer:readString()
							local ent = self:findById(id)
							local remove = buffer:readByte() > 0
							if remove then
								if ent then
									self:despawn(ent)
								end
							else
								local classname = buffer:readString()
								if ent then
									ent:readSyncPacket(buffer)
								else
									local ent = class.importSafe(classname, "common.entity.Base"):new(self)
									ent.id = id
									ent:readSyncPacket(buffer)
									self:spawn(ent)
								end
							end
						end
						--extrapolate by lag
						self.world:update(lag)
					elseif packetType == network.CLIENTBOUND.ENTITY then
						local id = buffer:readString()
						local ent = self:findById(id)
					
						if ent then
							--deliver packet--
							ent:processPacket(nil, buffer)
						else
							log(self.tag, "Entity \""..id.."\" doesn't exist yet. Adding to deliver queue...")
							self.deliverQueue[#self.deliverQueue+1] = {id,buffer}
						end
					else
						log(self.tag, "Unhandled PLAY packet: "..tostring(network.CLIENTBOUND[packetType]).." ("..packetType..")")
					end
				end
			end
			local b = (love.timer.getTime()-a)*1000
			if b > 16 then
				print("Packet took too long to process ("..b.."ms)")
				break
			end
		else
			break
		end
	end
end

--Draws the entities to the screen
local CIRCUMSCRIBED = (2^0.5) --math.tan(math.pi/2)
DEBUG_BODIES = true
function ClientContext:draw(lod)
	self.lod = lod
	
	local width, height = love.graphics.getDimensions()
	width = width/lod
	height = height/lod
	
	local sbbx1, sbby1, sbbx2, sbby2
	
	if self.localPlayer then
		local px, py = self.localPlayer.body:getPosition()
		sbbx1 = px-((width/2))
		sbby1 = py-((height/2))
		
		sbbx2 = px+((width/2))
		sbby2 = py+((height/2))
	else
		sbbx1, sbby1, sbbx2, sbby2 = 0
	end
	
	for _, entity in ipairs(self.entities) do
		if entity.width then
			local emax = entity.max*CIRCUMSCRIBED*2
			local emaxh = emax/2
			
			local bx, by = entity.body:getPosition()
			
			if not ((bx-emaxh) > sbbx2 or (bx+emaxh) < sbbx1 or (by-emaxh) > sbby2 or (by+emaxh) < sbby1) then
				entity:draw(lod)
			else
				--print("Culled", entity)
			end
			
			if DEBUGGING and DEBUG_BODIES then
				local emax = entity.max*CIRCUMSCRIBED*2 --Extend the bounds for testing
				local emaxh = emax/2
				love.graphics.setColor(0,255,0)
				love.graphics.rectangle("line", bx-emaxh, by-emaxh, emax, emax)
				love.graphics.circle("line", bx, by, 4)
				love.graphics.setColor(255,255,0)
				local cmx, cmy = entity.body:getMassData()
				local wcmx, wcmy = entity.body:getWorldPoint(cmx, cmy)
				love.graphics.circle("line", wcmx, wcmy, 4)
			end
		else
			entity:draw(lod)
		end
	end
end
