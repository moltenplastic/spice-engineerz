local lfs = require("lfs")
local function list(dir)
	local o={}
	for fn in lfs.dir(dir) do
		if fn~="." and fn~=".." then
			table.insert(o,fn)
		end
	end
	return o
end
local function split(file)
	local t={}
	for dir in file:gmatch("[^/]+") do
		t[#t+1]=dir
	end
	return t
end
local function combine(filea,fileb)
	local o={}
	for k,v in pairs(split(filea)) do
		table.insert(o,v)
	end
	for k,v in pairs(split(fileb)) do
		table.insert(o,v)
	end
	return filea:match("^/?")..table.concat(o,"/")..fileb:match("/?$")
end
local function exists(file)
	return lfs.attributes(file) or false
end
local function isDir(file)
	return lfs.attributes(file).mode == "directory"
end
local crush
function crush(dir)
	local t=0
	for k,v in pairs(list(dir)) do
		local file=combine(dir,v)
		if isDir(file) then
			t=t+crush(file)
		elseif v:match("%.png$") then
			os.execute("pngcrush -brute \""..file.."\" \""..file..".crushed\"")
			if exists(file..".crushed") then
				os.execute("rm "..file)
				os.execute("mv \""..file..".crushed\" \""..file.."\"")
			else
				error()
			end
		end
	end
	return t
end
crush(lfs.currentdir())