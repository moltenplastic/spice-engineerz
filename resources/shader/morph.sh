uniform float time;

varying vec4 targetColor;

#ifdef VERTEX

attribute vec4 VertexTargetPosition;
attribute vec4 VertexTargetColor;

vec4 position( mat4 transform_projection, vec4 vertex_position ) {
	vec4 p = mix(vertex_position, VertexTargetPosition, time);
	targetColor = VertexTargetColor;
	
	return transform_projection * p;
}

#endif

#ifdef PIXEL

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ) {
	vec4 c = mix(color, targetColor, time);
	
	return c;
}

#endif
