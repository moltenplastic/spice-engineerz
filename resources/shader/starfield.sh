uniform sampler2D lutTexture;

vec4 effect( vec4 color, sampler2D texture, vec2 texture_coords, vec2 screen_coords ) {
	vec4 c = texture2D(texture, texture_coords);
	
	return vec4(texture2D(lutTexture, texture_coords).rgb, c.r)*color;
}
