import bpy
import sys
import argparse

sys.path.append('.')
from import_svg import import_svg

print(import_svg)

#usage: blender --background --python convert_svg.py -- <svg file without extension>

argv = sys.argv
argv = argv[argv.index("--") + 1:] # get all args after "--"

parser = argparse.ArgumentParser(description='convert svg files into mdf files')
parser.add_argument('model', help='model to convert')
parser.add_argument('--lod')
parser.set_defaults(lod="all")

args = parser.parse_args(args=argv)
print(args)

lods = {'nintendo_64': 1, 'lowest': 3, 'low': 6, 'normal': 12, 'high': 18, 'highest': 24, 'ultra_super_duper_hd': 36}

selectedlods = ["normal"]

if args.lod == "all":
	selectedlods = ['nintendo_64', 'lowest','low','normal','high','highest', 'ultra_super_duper_hd']
elif args.lod != "none":
	selectedlods = args.lod.split("+")

print(selectedlods)

import_svg.load_svg(args.model+".svg")

name = args.model+".mdf"
file = open(name, "w")

for lod in selectedlods:
	file.write("lod "+lod+"\n")
	for obj in bpy.data.objects:
		if obj.type == "CURVE":
			obj.data.resolution_u = lods[lod]
			obj.data.resolution_v = lods[lod]
			
			mesh = obj.to_mesh(bpy.data.scenes[0],True,'PREVIEW')
			file.write("\tobject "+obj.data.name+"\n")
			for propname in obj.keys():
				file.write("\t\tprop "+propname[8:]+" "+obj[propname]+"\n")
			color = obj.data.materials[0].diffuse_color
			file.write("\t\tcolor %.3f %.3f %.3f\n" % (color.r, color.g, color.b))
			for vert in mesh.vertices:
				file.write("\t\tvert %.3f %.3f\n" % (vert.co.x, 1-vert.co.y))
			for poly in mesh.polygons:
				file.write("\t\ttri %d %d %d\n" % (poly.vertices[0], poly.vertices[1], poly.vertices[2]))
			file.write("\n")

file.close()
