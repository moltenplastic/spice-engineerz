Exporting SVG files to OBJ files using Blender:

SVG Setup:
	1 meter by 1 meter canvas
	Grid Lines:
		Units in cm
		Spacing X: 6.25000
		Spacing Y: 6.25000
		Major grid line: 4

Scale: The width and height in pixels you want
Y Forward
Z Up
Triangulation on

For objects with paths on top of paths, the game's object loader
will do Z sorting to decide what renders first.

In game, objects load in the format described in class/block/Base.lua

