// SE LVX Example Program: Blink Thrusters
// Blink Thrusters finds all the thrusters attached to the system and blinks them on and off at 1 second intervals

main:
.entrypoint
	LDR R0, PC, string_thruster_type-PC
	// HWI 0-5 are the same as in the reference intepreter, but the output is disabled by default
	// HWI 6 is to return the number of components matching the given type in R0
	HWI 6 // int [HWI 6] componentCount(char *type);
	LDR R4, R0 // R4 == number of thrusters attached
	
	// For the first n thrusters, write their component names to thruster_data
	// Component Names are 16 bytes long + 4 zero bytes (20 bytes)
	LDR R0, PC, thruster_data-PC
	LDR R1, R4
	LDR R2, PC, string_thruster_type-PC
	HWI 7 // int [HWI 7] componentList(void *dest, int num, char *type); writes num component names to the destination
	
	LDI R10, 0 // Current thruster state
flick_thrusters:
	// Invert R10
	LDI R0, 0
	CMP R0, R10, R0
	LDI.Z R10, 1
	LDI.NZ R10, 0
	
	LDI R11, 0 // For i = 0, i < R4 (nThrusters) do
	LDR R5, PC, thruster_data-PC
	loop:
		CMP R0, R11, R4
		JMP.GTE loop_end
		
		LDR R0, R5 // Thruster name from thruster_data
		LDI R1, 0 // Thruster operation 0
		LDR R2, R10 // Turn Thruster On or Off
		HWI 8 // int [HWI 8] componentInvoke(char *name, int operation, int operandA, int operandB);
		
		LDR R5, R5, 20 // skip 20 bytes
		LDR R11, R11, 1 // i++
		JMP loop
		
	loop_end:
		LDI R0, 1000 // 1 second
		LDI R1, 1 // No interrupt
		HWI 9 // int [HWI 9] sleep(int msec, bool noInterrupt); (returns the actual amount in milliseconds it slept)
		
		JMP flick_thrusters
	
string_thruster_type:
	.string "thruster\0"
thruster_data:
