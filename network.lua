--stuff for making network packet structures--

local network = {}

local ser = require "ser"

network.serialize = ser
network.unserialize = function(s) return assert(loadstring(s))() end

--Do not change these constants.
--NetCerial is finished, so it's format never changes
--This lets SERVER_INFO maintain cross version compatibility
network.constantSerialize = nc.serialize
network.constantUnserialize = nc.unserialize

local function constants(tab)
	local nt = {}
	local c = 1
	for i=1, #tab do
		local n = tab[i]
		
		if n:sub(1,6) ~= "DEBUG_" or DEBUGGING then
			nt[c] = tab[i]
			nt[tab[i]] = c
			c = c+1
		end
	end
	return nt
end

network.constants = constants

network.DISCON_REASON = constants {
	"NONE",
	"SERVER_EXIT",
}

network.CLIENT_DISCON_REASON = constants {
	"NONE",
	"CRASH",
	"QUIT",
	"VERSION_MISMATCH"
}

network.SERVERBOUND = constants {
	"SERVER_INFO", --DO NOT MOVE ELSEWHERE. THIS NEEDS TO STAY CONSTANT 1 FOR EVERY VERSION. SERVER INFO ALSO SENDS A PREDEFINED NON SERIALIZED TABLE STRUCTURE.
	"LOGIN",
	"QUIT",
	"ENTITY", --Entitybound packet
	"CUSTOM", --Custom packet, for mods
	
	"DEBUG_SPAWN_ENTITY",
}

network.CLIENTBOUND = constants {
	"SERVER_INFO", --Like I said before. DO NOT MOVE.
	"ERROR",
	"LOGGEDIN",
	"ENTITY",
	"SYNC", --Physics sync, can also have other entity data with it
	"CUSTOM" --Custom packet, for mods
}

network.CLIENTBOUND_ERROR = constants {
	"BAD_LOGIN",
	"NAME_IN_USE",
	"NAME_TOO_LONG"
}

network.PEERSTATE = constants {
	"LOGIN",
	"PLAY"
}

function network.entityPacket(ent, data)
	local buffer = love.string.newWriteBuffer()
	
	if CLIENT then
		buffer:writeByte(network.SERVERBOUND.ENTITY)
	else
		buffer:writeByte(network.CLIENTBOUND.ENTITY)
	end
	
	buffer:writeString(ent.id)
	buffer:writeBytes(data)
	
	return buffer:getString()
end

return network
