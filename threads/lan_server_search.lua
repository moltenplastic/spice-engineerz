local searchchannel = love.thread.getChannel("lan_search")
local quitchannel = love.thread.getChannel("lan_search_quit")

print("Starting up LAN Search Thread...")

local sock = require "socket".udp()
sock:setsockname("224.0.2.60", 8231)
sock:setoption("ip-add-membership", {multiaddr = "224.0.2.60", interface = "*"})

searchchannel:clear()
quitchannel:clear()

while true do
	if quitchannel:peek() then
		break
	end
	local data, ipaddress, port = sock:receivefrom()
	print(data, ipaddress, port)
	--data is server address--
	searchchannel:push({data, ipaddress, port})
end

sock:close()
