assert(xpcall(function()
	love.image = require "love.image"
	love.filesystem = require "love.filesystem"
	love.audio = require "love.audio"
	love.sound = require "love.sound"

	local requestChannel = love.thread.getChannel("resource_request_channel")

	while true do
		local request = requestChannel:demand()
		
		local path, dir, constructor, decoderId, responseChannel = table.remove(request,1), table.remove(request,1), table.remove(request,1), table.remove(request,1), table.remove(request,1)
		
		local response = {loadstring(constructor)()(dir, unpack(request))}
		table.insert(response, 1, decoderId)
		table.insert(response, 1, path)
		
		responseChannel:push(response)
	end
end,debug.traceback))