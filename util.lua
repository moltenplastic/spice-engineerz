-- this is the place for all the various utility functions


-- makes it safe to modify a table you are iterating
function tpairs(tbl)
	local s = {}
	local c = 1
	for k, v in pairs(tbl) do
		s[c] = k
		c = c + 1
	end
	c = 0
	return function()
		c = c + 1
		return s[c], tbl[s[c]]
	end
end

-- returns true (and the metatable, if possible) if a table has a metatable, else false--
function hasmetatable(t)
	local s, mt = pcall(getmetatable, t)
	
	if not s then
		return true
	elseif mt then
		return true, mt
	else
		return false
	end
end

-- deep copy of a table
function deepcopy(src, dest, cache)
	cache = cache or {}
	cache[src] = dest
	for i, v in pairs(src) do
		if type(v) == "table" and not hasmetatable(v) then
			if not cache[v] then
				dest[i] = {}
				deepcopy(v,dest[i],cache)
			else
				dest[i] = cache[v]
			end
		else
			dest[i] = v
		end
	end
end
