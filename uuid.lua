local uuid = {}

local valid = {}
for c in ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"):gmatch(".") do
	valid[#valid+1] = c
end

local len = 15

local rng = love.math.newRandomGenerator(os.time(), (os.clock()+love.timer.getTime())*10000)
local gens = 0

function uuid.generate()
	if gens > 8 then
		rng = love.math.newRandomGenerator(os.time(), (os.clock()+love.timer.getTime())*10000)
		gens = 0
	end
	
	gens = gens+1
	
	local t = {}
	for i=1, len do
		t[i] = valid[math.ceil(rng:random()*#valid)]
	end
	
	table.insert(t,8,"-")
	
	return table.concat(t)
end

function uuid.generateFrom(name)
	local msb, lsb = 0, 0
	
	for i=1, #name do
		if i % 2 == 0 then
			msb = msb+(name:byte(i)*i)
		else
			lsb = lsb+(name:byte(i)*i)
		end
	end
	
	local rng = love.math.newRandomGenerator(msb, lsb)
	
	local t = {}
	for i=1, len do
		t[i] = valid[math.ceil(rng:random()*#valid)]
	end
	
	table.insert(t,8,"-")
	
	return table.concat(t)
end

return uuid
