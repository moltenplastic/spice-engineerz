local matrix = {}

function matrix.new()
	return {
  [0] = 1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,0,0,1,
	}
end

function matrix.multiply(b,a)
	local dst = matrix.new()

	dst[0] = a[0]*b[0] + a[1]*b[4] + a[2]*b[8] + a[3]*b[12];
	dst[1] = a[0]*b[1] + a[1]*b[5] + a[2]*b[9] + a[3]*b[13];
	dst[2] = a[0]*b[2] + a[1]*b[6] + a[2]*b[10] + a[3]*b[14];
	dst[3] = a[0]*b[3] + a[1]*b[7] + a[2]*b[11] + a[3]*b[15];
	dst[4] = a[4]*b[0] + a[5]*b[4] + a[6]*b[8] + a[7]*b[12];
	dst[5] = a[4]*b[1] + a[5]*b[5] + a[6]*b[9] + a[7]*b[13];
	dst[6] = a[4]*b[2] + a[5]*b[6] + a[6]*b[10] + a[7]*b[14];
	dst[7] = a[4]*b[3] + a[5]*b[7] + a[6]*b[11] + a[7]*b[15];
	dst[ 8] = a[8]*b[0] + a[9]*b[4] + a[10]*b[8] + a[11]*b[12];
	dst[ 9] = a[8]*b[1] + a[9]*b[5] + a[10]*b[9] + a[11]*b[13];
	dst[10] = a[8]*b[2] + a[9]*b[6] + a[10]*b[10] + a[11]*b[14];
	dst[11] = a[8]*b[3] + a[9]*b[7] + a[10]*b[11] + a[11]*b[15];
	dst[12] = a[12]*b[0] + a[13]*b[4] + a[14]*b[8] + a[15]*b[12];
	dst[13] = a[12]*b[1] + a[13]*b[5] + a[14]*b[9] + a[15]*b[13];
	dst[14] = a[12]*b[2] + a[13]*b[6] + a[14]*b[10] + a[15]*b[14];
	dst[15] = a[12]*b[3] + a[13]*b[7] + a[14]*b[11] + a[15]*b[15];
	return dst
end

function matrix.apply(m,x,y,z)
	z = z or 1
	local nx = m[0]*x + m[1]*y + m[2]*z + m[3]
	local ny = m[4]*x + m[5]*y + m[6]*z + m[7]
	local nz = m[8]*x + m[9]*y + m[10]*z + m[11]
	return nx, ny, nz
end

function matrix.translate(m,x,y,z)
	local tm = matrix.new()
	tm[3] = x
	tm[7] = y
	tm[11] = z or 0
	return matrix.multiply(m,tm)
end

function matrix.scale(m,x,y,z)
	y = y or x
	z = z or y
	local tm = matrix.new()
	tm[0] = x
	tm[5] = y
	tm[10] = z
	return matrix.multiply(m,tm)
end

function matrix.rotateX(m,a)
	local tm = matrix.new()
	tm[5] = math.cos(a)
	tm[6] = -math.sin(a)
	tm[9] = math.sin(a)
	tm[10] = math.cos(a)
	return matrix.multiply(m,tm)
end

function matrix.rotateY(m,a)
	local tm = matrix.new()
	tm[0] = math.cos(a)
	tm[2] = math.sin(a)
	tm[8] = -math.sin(a)
	tm[10] = math.cos(a)
	return matrix.multiply(m,tm)
end

function matrix.rotateZ(m,a)
	local tm = matrix.new()
	tm[0] = math.cos(a)
	tm[1] = -math.sin(a)
	tm[4] = math.sin(a)
	tm[5] = math.cos(a)
	return matrix.multiply(m,tm)
end

function matrix.copy(m)
	local n = {} for i=0, #m do n[i] = m[i] end return n
end

function matrix.toShaderForm(m)
	return {
		{m[0],m[1],m[2],m[3]},
		{m[4],m[5],m[6],m[7]},
		{m[8],m[9],m[10],m[11]},
		{m[12],m[13],m[14],m[15]},
	}
end

function matrix.perspective(fovy, aspect, zNear, zFar)
	local m = matrix.new()
	local sine, cotangent, deltaZ
	local radians = fovy * (1.0 / 2.0 * math.pi / 180.0);

	deltaZ = zFar - zNear;
	sine = math.sin(radians);
	if (deltaZ == 0) or (sine == 0) or (aspect == 0) then return end

	cotangent = math.cos(radians) / sine;

	m[1] = 0; m[2] = 0; m[3] = 0;
	m[4] = 0; m[6] = 0; m[7] = 0;
	m[8] = 0; m[9] = 0;
	m[12] = 0; m[13] = 0;
	m[0] = cotangent / aspect;
	m[5] = cotangent;
	m[10] = -(0 + zNear) / deltaZ;
	m[11] = -1;
	m[14] = - zNear * zFar / deltaZ;
	m[15] = 0;

	return m
end

return matrix
